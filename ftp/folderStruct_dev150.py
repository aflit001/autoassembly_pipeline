#!/usr/bin/python
import os
import sys
import inspect

#currAbsPath     = os.path.dirname(inspect.getfile(inspect.currentframe()))
#currFtpPath     = os.path.abspath(os.path.join(currAbsPath, "ftp"))
#sys.path.append(currFtpPath)

import lib.constants as constants

######################
## PERSONAL VARIABLES
######################
genomeSize              = 950000000
base                    = "/mnt/nexenta/assembly/nobackup/dev_150/" # base of all projects


#folders for RIL sub-project
#www.tomatogenome.net/accessions.html
#./sample_heinz
#./sample_heinz/454
#./sample_heinz/454/08000
#./sample_heinz/454/20000
#./sample_heinz/454/shotgun
#./sample_heinz/illumina
#./sample_heinz/illumina/MP_2000
#./sample_heinz/illumina/MP_3000
#./sample_heinz/illumina/MP_4000
#./sample_heinz/illumina/MP_5000
#./sample_heinz/illumina/PE_250
#./sample_heinz/illumina/PE_300
#./sample_heinz/illumina/PE_500
#
#./sample_pennellii
#./sample_pennellii/454
#./sample_pennellii/454/03000
#./sample_pennellii/454/08000
#./sample_pennellii/454/20000
#./sample_pennellii/illumina
#./sample_pennellii/illumina/MP_3000
#./sample_pennellii/illumina/MP_5000
#./sample_pennellii/illumina/PE_300
#./sample_pennellii/illumina/PE_600

#libraries created to the de novo assembly
denovoLibsHeinz     =  [
                        ["illumina",    [
                                            [ "PE_250",  'PE',  '250' ],
                                            [ "PE_300",  'PE',  '300' ],
                                            [ "PE_500",  'PE',  '500' ],
                                            [ "MP_2000", 'MP', '2000' ],
                                            [ "MP_3000", 'MP', '3000' ],
                                            [ "MP_4000", 'MP', '4000' ],
                                            [ "MP_5000", 'MP', '5000' ],
                                        ]
                        ],
                        ["454",          [
                                            [ "08000"  , 'MP' ,  '8000' ],
                                            [ "20000"  , 'MP' , '20000' ],
                                            [ "shotgun", 'WGS',    None ],
                                        ]
                        ]
                    ]
denovoLibsPenne     =  [
                        ["illumina",    [
                                            [ "PE_300",  'PE',  '300' ],
                                            [ "PE_600",  'PE',  '600' ],
                                            [ "MP_3000", 'MP', '3000' ],
                                            [ "MP_5000", 'MP', '5000' ],
                                        ]
                        ],
                        ["454",          [
                                            [ "03000"  , 'MP' ,  '3000' ],
                                            [ "08000"  , 'MP' ,  '8000' ],
                                            [ "20000"  , 'MP' , '20000' ],
                                        ]
                        ]
                    ]
denovoLibsHeinzTiny =  [
                        ["illumina",    [
                                            [ "PE_500",  'PE',  '500' ],
                                            [ "MP_5000", 'MP', '5000' ],
                                        ]
                        ],
                        ["454",          [
                                            [ "08000"  , 'MP' ,  '8000' ],
                                            [ "shotgun", 'WGS',    None ],
                                        ]
                        ]
                    ]

folders     =   [
                    #root  name                type               subs  libs                 to be cleaned
                    [base, "sample_pennellii" , constants.DENOVO, None, denovoLibsPenne    , True],
                    [base, "sample_heinz"     , constants.DENOVO, None, denovoLibsHeinz    , True],
                    [base, "sample_heinz_tiny", constants.DENOVO, None, denovoLibsHeinzTiny, True],
                    [base, "sample_heinz_nano", constants.DENOVO, None, denovoLibsHeinzTiny, True],
                ]


statusesToClean  = ['CHECKED']
projectsToIgnore = ['sample_pennellii', 'sample_heinz_tiny', "sample_heinz_nano"]
samplesToIgnore  = []
quakeignore      = []
quakecutoff      = {
                        'sample_pennelli'  : 4,
                        'sample_heinz'     : 4,
                        'sample_heinz_tiny': 1,
                        'sample_heinz_nano': 1,
                    }


setup_forbidden = [ # system libs
                    'os'             , 'sys'            , 'inspect'        ,
                    # shared variables
                    'currAbsPath'    , 'currFtpPath'    , 'constants'      , 'setup_forbidden',
                    # temporary variables
                    'denovoLibsHeinz', 'denovoLibsPenne', 'setup_forbidden' ]
