var jsonFile = "db/structDBKey.json";
var numCols  = 6;

var namesToFunctions = {};
namesToFunctions["data"                        ] = makeDataTable;
namesToFunctions["contaminationGraph"          ] = plotGraph;
namesToFunctions["fastqcGraphs"                ] = plotFastQcGraphs;
namesToFunctions["hash"                        ] = identicon;

namesToFunctions["duplication levels"          ] = plotGraph;
namesToFunctions["per base gc content"         ] = plotGraph;
namesToFunctions["per base n content"          ] = plotGraph;
namesToFunctions["per base quality"            ] = plotGraph;
namesToFunctions["per base sequence content"   ] = plotGraph;
namesToFunctions["per sequence gc content"     ] = plotGraph;
namesToFunctions["per sequence quality"        ] = plotGraph;
namesToFunctions["sequence length distribution"] = plotGraph;

namesToFunctions['qualsRaw'                    ] = dumb;
namesToFunctions['qualsConvProp'               ] = dumb;
namesToFunctions['qualsConv'                   ] = dumb;
namesToFunctions['contaminationData'           ] = array2table;

// cosmetic
namesToFunctions['mtime'                       ] = mtime2date;
namesToFunctions['size'                        ] = sizeReadable;
namesToFunctions['numSeqs'                     ] = formatNumber;
namesToFunctions['seqLen'                      ] = formatNumber;
namesToFunctions['sumQuals'                    ] = formatNumber;
namesToFunctions['sumRealFileSize'             ] = formatNumber;
namesToFunctions['Ns'                          ] = formatNumber;


var namesToIgnore = {};
namesToIgnore[   'py/object'                   ] = null;

var namesToAppend = {};
namesToAppend[   "id"                          ] = appendTr;
namesToAppend[   "infile"                      ] = appendTr;
namesToAppend[   "librarySize"                 ] = appendTr;
namesToAppend[   "libraryType"                 ] = appendTr;
namesToAppend[   "projectName"                 ] = appendTr;
namesToAppend[   "projectStatusName"           ] = appendTr;
namesToAppend[   "projectType"                 ] = appendTr;
namesToAppend[   "py/object"                   ] = appendTr;
namesToAppend[   "sampleId"                    ] = appendTr;
namesToAppend[   "sampleLibrary"               ] = appendTr;
namesToAppend[   "sequenceTech"                ] = appendTr;
namesToAppend[   "pairName"                    ] = appendTr;

function dumb(key, value, level, $dst, $line) {
}

function mtime2date(key, value, level, $dst, $line) {
    var d = new Date(value);
    $line.append(d.toLocaleString());
}

function sizeReadable(key, value, level, $dst, $line) {
    $line.append(bytesToSize(value, 2));
}

function formatNumber(key, value, level, $dst, $line) {
    $line.append(value);
    $line.digits();
}

function plotGraph(key, value, level, $dst, $line) {
    $line.append($('<img/>', { 'src': "data:image/png;base64,"+value,
                                class: 'imgSmall' }));
    $line.append("<br/>");
}


function identicon(key, value, level, $dst, $line) {
    $line.append($('<img/>', { 'src': 'http://www.gravatar.com/avatar/'+value+'?s='+value.length+'&d=identicon&r=PG',
                                class: 'imgIcon' }));
}

function plotFastQcGraphs(key, value, level, $dst, $line) {
    var $tb = $('<table/>');
    var $th = $('<tr/>');
    var $tr = $('<tr/>');

    $.each(value, function (key2, value2) {
        $th.append($('<td/>', { 'id': key2 }).html(key2));
        var $td =  $('<td/>', { 'id': key2 });

        if ( key2 in namesToFunctions ){
            func = namesToFunctions[key2];
            func(   key2, value2, level+1, $dst, $td);

        } else {
            if (typeof value2 == "object") {
                $td.html(""+value2);

            } else {
                $td.html(""+value2);

            }
        }

        $tr.append($td);
    });

    $tb.append($th);
    $tb.append($tr);
    $line.append($tb);
}

function appendTr(key, value, level, $dst, $tr) {
    $tr.remove();
    var $td  = $dst.find("td:last");
    $td.append($('<b/>').html(key));
    $td.append('&nbsp;' + value + '&nbsp;');
}

function array2table(key, value, level, $dst, $line) {
    var $divS = $('<div/>');
    $divS.append($('<div/>', { class: 'contaminationTableDivShow' }).html('show'));
    var $div  = $('<div/>' , { class: 'contaminationTableDiv'     });
    var $tb   = $('<table/>');
    var $th   = $('<tr/>');
    var cells = [];
    $.each(value, function (key2, value2) {
        var $td = $('<th/>');
        $td.append(key2);
        $th.append($td);

        var $tv = $('<td/>');
        var nums = [];
        $.each(value2, function (key3, value3) {
            nums.push(parseFloat(value3).toFixed(2));
        });
        $tv.append(nums.join('<br/>'));
        cells.push($tv);
    });
    $tb.append($th);

    var $tr = $('<tr/>');
    $.each(cells, function (index, value2) {
        $tr.append(value2);
    });
    $tb.append($tr);

    $div.append($tb);
    $divS.append($div);
    $line.append($divS);
}

function makeDataTable(key, value, level, $main, $line) {
    //$line.find("td:last").html(item);
    var $tbl     = $('<table/>');
    var $th      = $('<tr/>');
    var cols     = {};
    var ths      = {};
    var keys     = [];
    var maxLines = 0;
    $line.find("td:last").html('');

    $.each(value, function (key2, value2) {
        cols[key2]        = [];
        var currLines     = 0;
        keys[keys.length] = key2;

        $.each(value2, function (key3, value3) {
            currLines++;
            var vals = []
            var $tdh = $('<th/>', { 'parent': key3, 'gparent': key2, class: 'header3' }).append(key3);
            var $td  = $('<td/>', { 'parent': key3, 'gparent': key2, class: 'value3'  });

            if ( key3 in namesToFunctions ){
                func = namesToFunctions[key3];
                func(   key3, value3, level+1, $main, $td);

            } else {
                if (typeof value3 == "object") {
                    $td.html(""+value3);
                } else {
                    $td.html(""+value3);
                }
            }

            vals.push($tdh);
            vals.push($td);
            (cols[key2]).push(vals);
        });

        if ( currLines > maxLines ) {
            maxLines = currLines;
        }

        var $td  = $('<th/>', { 'id': key2, 'colspan': 2, class: 'header2', 'gparent': key2 });
        $td.html(  key2);
        ths[key2] = $td;
    });


    for ( var p = 0; p <  keys.length; p++ ) {
        var key2   = keys[p];
        var $head = ths[ key2];
        $th.append($head);
    }
    $tbl.append($th);


    for ( var i = 0; i < maxLines; i++ ) {
        var $tr   = $('<tr/>', { 'lineid': i });
        var key2  = cols[i];

        for ( var p = 0; p < keys.length; p++ ) {
            var key2  = keys[p   ];
            var vals  = cols[key2];
            var $val  = vals[i   ];
            var $val1;
            var $val2;

            if ( typeof $val == 'undefined' ) {
                $val1  = $('<th/>', { class: 'empty', 'gparent': key2 });
                $val2  = $('<td/>', { class: 'empty', 'gparent': key2 });

            } else {
                $val1  = $val[0];
                $val2  = $val[1];
            }

            $tr.append($val1);
            $tr.append($val2);
        }

        $tbl.append($tr);
    }

    $line.find("td:last").append($tbl);
}

function getList(key, value, level, $dst, $line) {
    if (null != value) {
        if (typeof value == "object") {
            $.each(value, function (key2, value2) {
                if ( key2 in namesToIgnore ) {
                    return;
                };

                var $tr  = $('<tr/>', {
                    class: 'level'+level
                });

                for ( i = 0; i < numCols; i++ ) {
                    if ( i == level ) {
                        var $td = $('<td/>');
                        $td.html(key2);
                        $tr.append($td);
                    } else {
                        var $td = $('<td/>');
                        $tr.append($td)
                    }
                }

                $dst.append($tr);

                if      ( key2 in namesToFunctions ) {
                    func = namesToFunctions[key2];
                    func( key2, value2, level+1, $dst, $tr );

                }
                else if ( key2 in namesToAppend    ) {
                    func = namesToAppend[key2];
                    func( key2, value2, level+1, $line, $tr );

                } else {
                    getList(key2, value2, level+1, $dst, $tr );

                }
            });
        } else { // item not object
            $line.find("td:last").html(value2);
        }
    } else { // item null
        //$list.append("<li/><br/>");
    }
}

function appendProgress(msg) {
    $('#progress').append('<small>'+msg+'</small><br/>');
}

function rewriteProgress(msg){
    $('#progress').html($('<input/>', { 'type': 'text', 'id': 'progressText', }));
    appendProgress(msg);
}

function parseJson(fn, json) {
    if (json != "Nothing found."){
        //$('#display').html('<h2 class="loading">Well, gee whiz! We found you a poster, skip!</h2>');
        $('#display').html('');
        var fnShort = "" + fn + "";
        if ( fnShort.substr(0,3) == "db/" ) {
            fnShort = fnShort.substring(3, fnShort.length)
        }
        if ( fnShort.substr(0,9) == "structDB_" ) {
            fnShort = fnShort.substring(9, fnShort.length)
        }
        if ( fnShort.substr(-5) == ".json" ) {
            fnShort = fnShort.substring(0, fnShort.length-5)
        }

        fnShort = fnShort.replace(/_/g, " ")
        $('#sppName').html(fnShort);

        var $table  = $('<table/>', {
            'id': 'displayTable',
            //'src':
            });

        var $tr = $('<tr/>');

        for ( i = 0; i < numCols; i++ ) {
            var $th = $('<th/>', {
                'class': 'level'+i
                });
            $tr.append($th)
        }

        $table.append($tr);

        $('#display').html($table);

        var $dst = $('#displayTable');
        $('#progressText').focus();

        getList('root', json, 0, $dst, $tr);
    } else {
        appendProgress('nothing found');
    }
}

function getDb (jsonFileName, callback) {
     appendProgress("Processing");

     var $req = $.getJSON(jsonFileName)
     .success ( function ( res )              { callback(      jsonFileName, res          ); } )
     .error(    function ( req, status, err ) { appendProgress("ERROR "+status+" " +err+""); } )
     .complete( function ()                   { appendProgress("Completed"                ); } );
}

function makeButtons(fn, json){
    if (json != "Nothing found."){
        $('#fetch').html('');
        rewriteProgress('Loading buttons');
        if (null != json) {
            $('#fetch').html('');
            $.each(json, function (key, value) {
                name     = value[0]
                fileName = value[1]
                $btn = $("<input/>", {
                        'type'    : 'radio',
                        'id'      : name,
                        'fileName': fileName,
                        'name'    : 'radio'
                    })
                $lbl = $('<label/>', {
                        'for': name
                    })
                $lbl.append(name);
                //$("#radio").buttonset();
                $('#fetch').append($btn);
                $('#fetch').append($lbl);
            });
            $('#fetch').buttonset();
            $('#fetch :radio').click(function(e) {
                var rawElement = this;
                var $element   = $(this);
                var fileName   = $element.attr("fileName");
                rewriteProgress('Selected to load: ' +fileName+ '');
                $("#display").html('');
                getDb(fileName + '.json', parseJson);
                });
        } else {
            rewriteProgress('error loading buttons. no data');
        }
    } else {
        appendProgress("Nothing found");
    }
}

$(document).ready(function(){
    $.fn.digits = function(){
        return this.each(function(){
            $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
        })
    }

    getDb(jsonFile, makeButtons);

    $('#display').on("click", 'img.imgSmall', function(event) {
        $(this).attr('class', 'imgBig');
        //alert('changing class to big');
        });

    $('#display').on("click", 'img.imgBig', function(event) {
        $(this).attr('class', 'imgSmall');
        //alert('changing class to small');
    });

    $('#display').on("click", '.contaminationTableDivShow', function(event) {
        //$line.find("td:last").html(item);
        //$(this).attr('class', 'imgBig');
        //alert('changing class to big');
        if ($(this).parent().find("div:first").html() == 'hide') {
            $(this).parent().find("div:first").html('show');
            $(this).parent().find("div:last").hide();
        }
        else if ($(this).parent().find("div:first").html() == 'show') {
            $(this).parent().find("div:first").html('hide');
            $(this).parent().find("div:last").show();
        }
    });

   //$('#term').focus(function(){
   //   var full = $("#display").has("img").length ? true : false;
   //   if(full == false){
   //      $('#display').empty();
   //   }
   //});

   //$('#search').click(getPoster);
   //$('#term').keyup(function(event){
   //    if(event.keyCode == 13){
   //        getPoster();
   //    }
   //});

});


/**
* Convert number of bytes into human readable format
*
* @param integer bytes     Number of bytes to convert
* @param integer precision Number of digits after the decimal separator
* @return string
*/
function bytesToSize(bytes, precision)
{
   var kilobyte = 1024;
   var megabyte = kilobyte * 1024;
   var gigabyte = megabyte * 1024;
   var terabyte = gigabyte * 1024;

   if ((bytes >= 0) && (bytes < kilobyte)) {
       return bytes + ' B';

   } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
       return (bytes / kilobyte).toFixed(precision) + ' KB';

   } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
       return (bytes / megabyte).toFixed(precision) + ' MB';

   } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
       return (bytes / gigabyte).toFixed(precision) + ' GB';

   } else if (bytes >= terabyte) {
       return (bytes / terabyte).toFixed(precision) + ' TB';

   } else {
       return bytes + ' B';
   }
}
