#!/usr/bin/python
from constants    import *
#import os.path
#import copy

######################
## PERSONAL VARIABLES
######################
genomeSize              = 950000000
base                    = "/mnt/nexenta/assembly/nobackup/dev_150/" # base of all projects

maxThreadsGlobal        = 5
maxThreads              = maxThreadsGlobal * 1
sleepWhileWaiting       = 10
Qborder                 = 30
DoAdaptamers            = True

ignoreDb                = False # ignore db file and re-do the work
loadLocalDbs            = True

skipIllumina            = False  # skip illumina dataset
skip454                 = False # skip 454 dataset

reDecompress            = False

runGenData              = True
redoData                = False # re-do get data

doQual                  = True  # TRUE do qual or FALSE create phony data
redoQuals               = False # re-do get quals

doHash                  = True  # TRUE do hash or FALSE create phony data
redoHash                = False # re-do get hash

runFastQCForReal        = True
redoFastQC              = False

runContaminationForReal = True
redoContamination       = False

exportToFile            = True  # export to files or print in the screen
replaceFiles            = True  # when exporting to file replace files or leave output as tmp

redoReport              = True
mergePdfs               = True

debug                   = False # print extra information
#dumpBoth               = False # dump in both yaml (if available) and pickle


#folders for RIL sub-project
#www.tomatogenome.net/accessions.html
#./sample_heinz
#./sample_heinz/454
#./sample_heinz/454/08000
#./sample_heinz/454/20000
#./sample_heinz/454/shotgun
#./sample_heinz/illumina
#./sample_heinz/illumina/MP_2000
#./sample_heinz/illumina/MP_3000
#./sample_heinz/illumina/MP_4000
#./sample_heinz/illumina/MP_5000
#./sample_heinz/illumina/PE_250
#./sample_heinz/illumina/PE_300
#./sample_heinz/illumina/PE_500
#
#./sample_pennellii
#./sample_pennellii/454
#./sample_pennellii/454/03000
#./sample_pennellii/454/08000
#./sample_pennellii/454/20000
#./sample_pennellii/illumina
#./sample_pennellii/illumina/MP_3000
#./sample_pennellii/illumina/MP_5000
#./sample_pennellii/illumina/PE_300
#./sample_pennellii/illumina/PE_600




#libraries created to the de novo assembly
denovoLibsHeinz  =  [
                        ["illumina",    [
                                            #[ "PE_250",  'PE',  '250' ],
                                            #[ "PE_300",  'PE',  '300' ],
                                            #[ "PE_500",  'PE',  '500' ],
                                            #[ "MP_2000", 'MP', '2000' ],
                                            #[ "MP_3000", 'MP', '3000' ],
                                            #[ "MP_4000", 'MP', '4000' ],
                                            [ "MP_5000", 'MP', '5000' ],
                                        ]
                        ],
                        ["454",          [
                                            [ "08000"  , 'MP' ,  '8000' ],
                                            #[ "20000"  , 'MP' , '20000' ],
                                            #[ "shotgun", 'WGS',    None ],
                                        ]
                        ]
                    ]
denovoLibsPenne  =  [
                        ["illumina",    [
                                            [ "PE_300",  'PE',  '300' ],
                                            [ "PE_600",  'PE',  '600' ],
                                            [ "MP_3000", 'MP', '3000' ],
                                            [ "MP_5000", 'MP', '5000' ],
                                        ]
                        ],
                        ["454",          [
                                            [ "03000"  , 'MP' ,  '3000' ],
                                            [ "08000"  , 'MP' ,  '8000' ],
                                            [ "20000"  , 'MP' , '20000' ],
                                        ]
                        ]
                    ]

denovoLibsHeinzTiny =  [
                        ["illumina",    [
                                            [ "PE_500",  'PE',  '500' ],
                                            [ "MP_5000", 'MP', '5000' ],
                                        ]
                        ],
                        ["454",          [
                                            [ "08000"  , 'MP' ,  '8000' ],
                                            [ "shotgun", 'WGS',    None ],
                                        ]
                        ]
                    ]

#####
## END OF PERSONAL VARIABLES
####


#description of the data
folders     =   [
                    #root  name                type     subs  libs              to be cleaned
                    [base, "sample_heinz"     , DENOVO, None, denovoLibsHeinz    , True],
                    [base, "sample_pennellii" , DENOVO, None, denovoLibsPenne    , True],
                    [base, "sample_heinz_tiny", DENOVO, None, denovoLibsHeinzTiny, True],
                ]
