import tools
import Image
import base64
import StringIO
import pprint
import zlib
import time
import sys
from tools import *
from prepareFtpFiles import GenData

def fixImages(db):
    s1    = 0
    s2    = 0
    s3    = 0
    pairs = [
        ['contamination', 'contaminationGraph'                           ],
        ['fastqc'       , 'fastqcGraphs', "duplication levels"           ],
        ['fastqc'       , 'fastqcGraphs', "per base gc content"          ],
        ['fastqc'       , 'fastqcGraphs', "per base n content"           ],
        ['fastqc'       , 'fastqcGraphs', "per base quality"             ],
        ['fastqc'       , 'fastqcGraphs', "per base sequence content"    ],
        ['fastqc'       , 'fastqcGraphs', "per sequence gc content"      ],
        ['fastqc'       , 'fastqcGraphs', "per sequence quality"         ],
        ['fastqc'       , 'fastqcGraphs', "sequence length distribution" ]
    ]

    for filename in db:
        reg       = db[filename]
        print "FILENAME %s REG %s" % (filename, reg)
        #sys.exit(0)

        data      = reg.data
        for pair in pairs:
            p1        = pair[0]
            p2        = pair[1]
            graphStr1 = data[p1][p2]

            if len(pair) == 3:
                p3        = pair[2]
                graphStr1 = data[p1][p2][p3]
                print "%s > %s > %s = %s" % (p1, p2, p3, graphStr1[:50])
            else:
                print "%s > %s = %s" % (p1, p2, graphStr1[:50])

            graphImg1 = base64.b64decode(graphStr1)
            im        = Image.open(StringIO.StringIO(graphImg1)).convert('L')
            imo       = StringIO.StringIO()
            im.save(imo, 'PNG', optimize=True, bits=8)
            graphImg2 = imo.getvalue()
            imo.close()
            graphStr2 = base64.b64encode(graphImg2)
            #graphStr3 = base64.b64encode(zlib.compress(graphImg2, 9))
            #data['contamination']['contaminationGraph'] = graphStr2
            l1 = len(graphStr1)
            l2 = len(graphStr2)
            #l3 = len(graphStr3)
            s1 += l1
            s2 += l2
            #s3 += l3
            #print '%d vs %d vs %d (%.2f / %.2f)' % (l1, l2, l3, ( l2*1.0/l1*100.0 ), ( l3*1.0/l1*100.0 ))
            print '%d vs %d (%.2f)' % (l1, l2, ( l2*1.0/l1*100.0 ))
            if len(pair) == 3:
                p3        = pair[2]
                db[filename].data[p1][p2][p3] = graphStr2
            else :
                db[filename].data[p1][p2]     = graphStr2

    #print '%.2f vs %.2f vs %.2f (%.2f / %.2f)' % (s1*1.0/1024/1024, s2*1.0/1024/1024, s3*1.0/1024/1024, ( s2*1.0/s1*100.0 ), ( s3*1.0/s1*100.0 ))
    print '%.2f vs %.2f (%.2f)' % (s1*1.0/1024/1024, s2*1.0/1024/1024, ( s2*1.0/s1*100.0 ))
    return db

if __name__ == '__main__':
    startTime  = getTimestamp()
    db = {}

    dbFile     = 'db/filesDB'
    tmpDb = tools.loadDump(dbFile)
    if tmpDb is not None: db = tmpDb
    print "LOADED"

    db = fixImages(db)
    saveDump(dbFile, db, startTime=startTime, verbose=True)
