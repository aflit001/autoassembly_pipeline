#!/usr/bin/python
import sys,os
import tools
import time
import pprint
from prepareFtpFiles import GenData


def main():
    if len(sys.argv) != 4:
        sys.stderr.write("NEEDS 2 FORMATS AMONG: %s\nAND A FILENAME\nAS IN %s <FMT SRC> <FMT DST> <FILE>\n\n" % (", ".join(tools.availDumpers), sys.argv[0]))
        sys.exit(1)

    fmt1 = sys.argv[1]
    if fmt1 not in tools.availDumpers.keys():
        sys.stderr.write("FORMAT NOT AMONG VALID: %s\n" % ", ".join(tools.availDumpers))
        sys.exit(1)
    fmt1Ext  = tools.availDumpers[fmt1][0]


    fmt2 = sys.argv[2]
    if fmt2 not in tools.availDumpers.keys():
        sys.stderr.write("FORMAT NOT AMONG VALID: %s\n" % ", ".join(tools.availDumpers))
        sys.exit(1)
    fmt2Ext = tools.availDumpers[fmt2][0]

    if fmt1 == fmt2:
        sys.stderr.write("FORMAT 1 (%s) IS THE SAME OF FORMAT 2 (%s)\n" % (fmt1, fmt2))
        sys.exit(1)

    inFileName = sys.argv[3]
    if not os.path.exists(inFileName):
        sys.stderr.write("INPUT FILE %s DOES NOT EXISTS\n" % (inFileName))
        sys.exit(1)

    if not inFileName.endswith(fmt1Ext):
        sys.stderr.write("INPUT FILE %s DOES NOT ENDS WITH DESIRED EXTENSION %s\n" % (inFileName, fmt1Ext))
        sys.exit(1)

    outFileName  = inFileName
    outFileName  = outFileName[:len(outFileName)-len(fmt1Ext)]
    outFileName += fmt2Ext

    if os.path.exists(outFileName):
        sys.stderr.write("OUTPUT FILE %s EXISTS\n" % (outFileName))
        sys.exit(1)

    print "CONVERTING FROM %6s to %6s" % (fmt1,    fmt2   )
    print "CONVERTING FROM %6s to %6s" % (fmt1Ext, fmt2Ext)
    print "CONVERTING FILE %s"         % inFileName
    print "        TO FILE %s\n"       % outFileName

    struct = tools.loadDump(inFileName, dumperNameLocal=fmt1)
    pprint.pprint(struct)
    tools.saveDump(outFileName, struct, time.time(), dumperNameLocal=fmt2)
    print "  SUCCESSFULLY CONVERTED"

if __name__ == '__main__': main()
