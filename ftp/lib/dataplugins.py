#!/usr/bin/python
import os,sys,time
import fnmatch
import shutil
import traceback
from   glob import glob
import subprocess
import inspect
import pprint

sys.path.insert(0, '.')

import setup
import ParseFastQ
import dbdump
import templater
from   tools import *
import Plugins

#import pprint
#pprint.pprint(globals())


def getDstFolders(dataObj, fileName):
    base        = dataObj.getProjectBase()                 # /home/assembly/tomato150/ril
    outPath     = dataObj.getTmpFolder()                   # 608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz
    outFileBase = os.path.join(outPath   , fileName)  # 608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz/seq.fastq
    outPathTmp  = os.path.join(base      , setup.tmpFolder)    # /home/assembly/tomato150/ril/_tmp
    outPathFull = os.path.join(outPathTmp, outPath)     # /home/assembly/tomato150/ril/_tmp/608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz
    outFile     = os.path.join(outPathTmp, outFileBase)   # /home/assembly/tomato150/ril/_tmp/608_illumina_pairedend_500_608_ACAGTG_L002_R2_001_fastq_gz/seq.fastq
    return (base, outPath, outFileBase, outPathTmp, outPathFull, outFile)

def getEntityName(data):
    projectName = data.getprojectName()
    if projectName is not None:
        outPath     = os.path.join('db', projectName)

        if not os.path.exists(outPath):
            os.makedirs(outPath)

        fn = data.getFileName().replace('.', '_').replace('/', '_').replace('\\', '_')

        dbFileName  = os.path.join(outPath, fn)
        return dbFileName
    return None

def saveIndividualEntity(data, startTime):
    dbFileName = getEntityName(data)
    if dbFileName is not None:
        dbdump.saveDump(dbFileName, data, startTime=startTime, verbose=True)#setup.debug)

def saveIndividualEntities(db, startTime):
    dbKeys = db.keys()
    dbKeys.sort()
    for filename in dbKeys:
        data           = db[filename]
        saveIndividualEntity(data, startTime)

def loadIndividualEntity(data):
    dbFileName    = getEntityName(data)
    dbFileNameExt = os.path.join(dbFileName + dbdump.dumperExt)
    #print "db File name",dbFileName
    tmpDb = None
    if os.path.exists(dbFileNameExt):
        #print "    file exists"
        tmpDb = dbdump.loadDump(dbFileName)
        #if tmpDb is not None:
        #    #print tmpDb.pp()
        #    data = tmpDb

    return tmpDb

def loadIndividualEntities(db):
    dbKeys = db.keys()
    dbKeys.sort()
    for filename in dbKeys:
        data           = db[filename]
        data           = loadIndividualEntity(data)
        db[filename]   = data

    return db

def mergePdfs(projectReports, outPdf, force=False):
    print "          MERGING PDFs",projectReports,"TO %s"%outPdf
    outFiles = []
    #TODO: SEND TO RUNSTRING
    if len(projectReports) > 1:
        inFiles = " ".join(projectReports)

        res = subprocess.call('pdfunite %s %s' % (inFiles, outPdf), shell=True, stdout=open(os.devnull, 'wb'), stderr=open(os.devnull, 'wb'))
        if res != 0:
            print "error merging pdfs %s into %s . %d" % (inFiles, outPdf, res)
            if os.path.exists(outPdf):
                os.remove(outPdf)
            sys.exit(1)

        if not os.path.exists(outPdf):
            print "error merging pdfs %s into %s . %d" % (inFiles, outPdf, res)
            print "output file not created"
            sys.exit(1)

        outFiles.append(outPdf)
    else:
        if force:
            shutil.copy(projectReports[0], outPdf)
            outFiles.append(outPdf)
        else:
            outFiles.extend(projectReports)

    print "          MERGING PDFs COMPLETED"
    return outFiles




def exportDB(struct):
    """
    given the database and the complete structure, create the reports summarizing the finds
    """
    print "EXPORTING"
    #dbKeys = db.keys()
    #dbKeys.sort()

    #if setup.debug:
    #    for fileName in dbKeys:
    #        data = db[fileName]
    #        print data.pp()


        #"COV": 0.2442651094736842,
    #"Ns": 71554,
        #"Ns%": 10.29977803815671,
        #"Q30": 0.0, "Q30BP": 232051854, "Q30COV": 0.2442651094736842,
    #"_validQual": true,
    #"adaptamerSum": 398988,
    #"adaptamerSum%": 0.5743197920295258,
    #"flx%": 0.0,
    #"flxSum": 0,
        #"numSeqs": 694714,
    #"seqAvgHarmLen": 272.55054142225475,
        #"seqAvgLen": 334.02501461032887,
        #"seqLenSum": 232051854,
        #"sumRealFileSize": 519653628,
    #"xlr%": 0.5743197920295258,
    #"xlrSum": 398988

    #qProp                       = data.getQPropCumm(30)
    #qProp                      *= 100.0
    #qSumCumm                    = data.getQSumCumm(30)
    #shaShort                    = data.getFileName()

    #fileSize                    = data.getPluginResult('info' , 'size'    )
    #fileSum['FILE_SIZE'       ] = fileSize
    #Ns                          = data.getPluginResult('quals', 'Ns'      )
    #numSeqs                     = data.getPluginResult('quals', 'numSeqs' )
    #seqLenSum                   = data.getPluginResult('quals', 'seqLenSum')
    #genomeSize                  = setup.genomeSize
    #fileSum['%_Q>=30'         ] = qProp
    #fileSum['COV'             ] = (  seqLenSum * 1.0 ) / ( genomeSize * 1.0 )
    #fileSum['BP_Q>=30'        ] =    seqLenSum - qSumCumm
    #fileSum['COV_Q>=30'       ] = (( seqLenSum - qSumCumm) * 1.0 ) / ( genomeSize * 1.0 )
    #fileSum['%_READS_WITH_Ns' ] = (( Ns * 1.0 ) / ( numSeqs * 1.0 )) * 100.0


    titles  =   [   #TITLE                         FUNCTION     FORMAT
                    [ 'FILENAME',                   None ,       '%s'    ], # defined in the loop
                    [ 'FILE_SIZE',                  'SUM',       '%d'    ],
                    [ 'DECOMPRESSED_SIZE',          'SUM',       '%d'    ],
                    [ 'AVG_READ_LENGTH',            'AVG',       '%d'    ],
                    [ 'NUM_SEQUENCES',              'SUM',       '%d'    ],
                    [ 'BP',                         'SUM',       '%d'    ],
                    [ 'COV',                        'SUM',       '%.2f'  ],
                    [ '%_Q>='  +str(setup.Qborder), 'AVG',       '%6.2f' ],
                    [ 'BP_Q>=' +str(setup.Qborder), 'SUM',       '%d'    ],
                    [ 'COV_Q>='+str(setup.Qborder), 'SUM',       '%.2f'  ],
                    [ '%_READS_WITH_Ns',            'AVG',       '%6.2f' ],
                    [ '%_454_FLX',                   None,       '%6.2f' ],
                    [ '%_454_XLR',                   None,       '%6.2f' ],
                ]

    headers =   [   #plugin name        plugin value             table column
                    ['info',            'size',                  'FILE_SIZE'                  ],
                    ['quals',           'sumRealFileSize',       'DECOMPRESSED_SIZE'          ],
                    ['quals',           'seqAvgLen',             'AVG_READ_LENGTH'            ],
                    ['quals',           'numSeqs',               'NUM_SEQUENCES'              ],
                    ['quals',           'seqLenSum',             'BP'                         ],
                    ['quals',           'COV',                   'COV'                        ],
                    ['quals',           'Q%d'   % setup.Qborder, '%_Q>='  +str(setup.Qborder) ],
                    ['quals',           'Q%sBP' % setup.Qborder, 'BP_Q>=' +str(setup.Qborder) ],
                    ['quals',           'Q%dCOV'% setup.Qborder, 'COV_Q>='+str(setup.Qborder) ],
                    ['quals',           'Ns%',                   '%_READS_WITH_Ns'            ],
                    ['quals',           'flx%',                  '%_454_FLX'                  ],
                    ['quals',           "xlr%",                  '%_454_XLR'                  ]
                ]




    title    = """\
#Preliminary quality assessment of sequences
#  FILENAME          : Path for the file in the raw/filtered folder
#  FILE_SIZE         : Size of the compressed file
#  DECOMPRESSED_SIZE : Size of the deceompressed file (when applicable)
#  AVG_READ_LENGTH   : Average read length
#  NUM_SEQUENCES     : Number of reads
#  BP                : Number of base pairs
#  COV               : Coverage (considering estimated size of 950Mbp) using BP
#  %_Q>=30           : Percentage of the base pairs with Q (phred quality) greater or equal to 30 [High quality]
#  BP_Q>=30          : Number of base pairs with Q (phred quality) greater or equal to 30 [High quality]
#  COV_Q>=30         : Coverage (considering estimated size of 950Mbp) using BP_Q>=30  [High quality]
#  %_READS_WITH_Ns   : Percentage of reads containing a non-base call (N)
#  %_454_FLX         : Percentage of reads containing FLX Mate Pair linker
#  %_454_XLR         : Percentage of reads containing XLR Mate Pair linker
"""
    title   += "#"+"\t".join([x[0] for x in titles])+"\n"

    contaminationTitle = """\
#Contaminations as found by fastq_screen against custom databases
#Species are:
#             EcoliM   = Pool of 13 E. coli strains
#                        str. K12 subst. DH10B
#                        str. K12 subst. W3110
#                        str. K12 subst. mg1655
#                        O55:H7 str. CB9615
#                        APEC01, CFT073, E24377A,HS, BM2952
#                        UMN026, ATCC 8739, UTI89, 55989
#             Hsap     = Homo sapiens (GRCh37, GCA_000001405.6, Ensembl)
#             IllCont  = Set of common illumina contaminations
#             Insects  = set of insects which could contaminate the data
#                        Drosophila melanogaster r5.45 (flybase)
#                        Aleyrodiformes (White fly)
#                             NCBI TAXONOMIC ID 33376  all dna sequences
#                        Aphidomorpha   (Aphids)
#                             NCBI TAXONOMIC ID 143691 all dna sequences
#             Mmus     = Mus musculus (NCBI m37, GCA_000001635.18, Ensembl)
#             PhX174   = Phi X 174 virus (NCBI NC_001422)
#             Sce      = Saccharomyces cerevisiae S288C (EF3 Release 64, Sep2011, Ensembl)
#             Slyc     = Solanum lycopersicum var Heinz v 2.40 (Sol Genomics Network)
#             UniVec   = NCBI UniVec database (build 7.0)
#             Viruses  = Pool of all phytoviruses (dpvweb.net)
#             Viruses2 = Specific selection of viruses important to tomato
#                        Pepino Mosaic Virus       NCBI ID 12176
#                        Tomato Spotted Wilt Virus NCBI ID 11613
#Numbers are the percentage of reads which mapped in each database
#(in a 50,000 sample of the reads):
#
#%Unmapped / %One_hit_one_library / %Multiple_hits_one_library / %One_hit_multiple_libraries / %Multiple_hits_multiple_libraries
"""
#unmapped / uniquely mapped / multi mapped (more than one location in the genome)

    #struct[projectName] = { '_projectType': projectType, '_projectBase': projectBase }
    #struct[projectName][projectStatus][sampleId][sequenceTech][sampleLibrary][sampleLibrary] = {
    #    '_fileName' : fileName,
    #    '_fileMTime': fileMTime,
    #    '_fileSize' : fileSize,
    #    '_data'     : data
    #}
    structKeys = struct.keys()
    structKeys.sort()

    for projectName in structKeys:
        projStatuses = struct[projectName]
        projectBase  = os.path.join(setup.getProjectRoot(projectName), projectName)
        print "PROJECT NAME %s BASE %s" % (projectName, projectBase)
        projectSum   = {}

        projStatusesKeys = projStatuses.keys()
        projStatusesKeys.sort()
        projectReports = []

        for  projectStatus in projStatusesKeys:
            fileCount = 0
            #if projectStatus[0] == '_': continue
            #projectStatus = statusDef[1]

            print "  STATUS %s"%projectStatus
            statusSum = {}

            srcDir = setup.getStatusFolder(projectStatus)
            dstSum = setup.getStatusSummary(projectStatus)
            dstSha = setup.getStatusSha(projectStatus)


            dstShaFileName    = os.path.join(projectBase , dstSha, projectName + ".sha256")
            dstConFileName    = os.path.join(projectBase , dstSum, projectName + "." + projectStatus.lower() + ".contamination.csv")
            dstSumFileName    = os.path.join(projectBase , dstSum, projectName + "." + projectStatus.lower() + ".summary.csv")
            dstSumLibFileName = os.path.join(projectBase , dstSum, projectName + "." + projectStatus.lower() + ".libs.summary.csv")


            print "  SHA           :: %s"% dstShaFileName
            print "  SUM           :: %s"% dstSumFileName
            print "  SUM LIB       :: %s"% dstSumLibFileName
            print "  CONTAMINATION :: %s"% dstConFileName


            if setup.exportToFile:
                try:            os.remove(dstShaFileName    + '.tmp' )
                except OSError: pass
                try:            os.remove(dstConFileName    + '.tmp' )
                except OSError: pass
                try:            os.remove(dstSumFileName    + '.tmp' )
                except OSError: pass
                try:            os.remove(dstSumLibFileName + '.tmp' )
                except OSError: pass
                dstShaFile    = open(dstShaFileName         + '.tmp', 'w')
                dstConFile    = open(dstConFileName         + '.tmp', 'w')
                dstSumFile    = open(dstSumFileName         + '.tmp', 'w')
                dstSumLibFile = open(dstSumLibFileName      + '.tmp', 'w')

                dstConFile.write(contaminationTitle)
                dstSumFile.write(title)
                dstSumLibFile.write(title)


            if not projectStatus in projStatuses:
                print "  NO FILE IN STATUS: ",projectStatus
                continue

            if setup.debug or not setup.exportToFile: print "SUM LINE %s" % title
            #if debug or not exportToFile: print "SHA LINE     :: STATUS %s DST SHA %s" % (projectStatus, dstShaFileName)
            #if debug or not exportToFile: print "SUM LINE     :: STATUS %s DST SUM %s" % (projectStatus, dstSumFileName)
            #if debug or not exportToFile: print "SUM LIB LINE :: STATUS %s DST SUM %s" % (projectStatus, dstSumFileName)

            outFqDir      = os.path.join(projectBase, dstSum, 'FASTQC' + "_" + projectStatus.lower() + '_tmp')
            outFqDirFinal = os.path.join(projectBase, dstSum, 'FASTQC' + "_" + projectStatus.lower())
            reportDstPath = os.path.join(projectBase, dstSum, 'report' + "_" + projectStatus.lower())
            reportDstFile = os.path.join(reportDstPath, projectName    + "_" + projectStatus.lower())

            fqGlob = os.path.join(outFqDirFinal,"*_fastQC.zip")
            #print fqGlob
            for f in glob(fqGlob):
                print "    FILES TO DELETE %s"%f
                try:            os.remove(f)
                except OSError: pass

            if not os.path.exists(reportDstPath):
                os.makedirs(reportDstPath)

            rpGlob = os.path.join(reportDstPath, "*.pdf")
            #print fqGlob
            for f in glob(rpGlob):
                print "    FILES TO DELETE %s"%f
                try:            os.remove(f)
                except OSError: pass

            samples       = projStatuses[projectStatus]

            samplesKeys = samples.keys()
            samplesKeys.sort()
            statusReports = []

            for sampleId in samplesKeys:
                sampleIdName = ""
                if sampleId != '':
                    sampleIdName = '_' + sampleId

                techs = samples[sampleId]

                print "   SAMPLE %s"%sampleId
                if len(techs) == 0: continue
                sampleSum = {}

                techsKeys = techs.keys()
                techsKeys.sort()
                sampleReports = []

                for sequenceTech in techsKeys:
                    libs = techs[sequenceTech]

                    print "      TECH %s"%sequenceTech
                    if len(libs) == 0: continue
                    techSum = {}

                    libsKeys = libs.keys()
                    libsKeys.sort()
                    techReports = []

                    for sampleLibrary in libsKeys:
                        fileNames = libs[sampleLibrary]

                        print "        LIB %s"%sampleLibrary
                        if len(fileNames) == 0: continue
                        libSum = {}

                        fileNamesKeys = fileNames.keys()
                        fileNamesKeys.sort()
                        libReports = []

                        for fileName in fileNamesKeys:
                            print "          FILE NAME %s"%fileName#,"MTIME",fileMTime,"SIZE",fileSize#,"ID",fileId#,"DATA",data.pp()
                            fileCount           += 1
                            data                = fileNames[fileName]
                            fileNames[fileName] = data

                            contamination       = data.getPlugin('contamination')
                            contaminationString = ""
                            contaminationLine   = ""

                            if contamination is not None:
                                #contaminationData   = contamination['contaminationData']
                                contaminationString = contamination.getContanimationString()
                                print "          CONTAMINATION :: %s :: CONTAMINATION EXISTS" % (fileName)
                                contaminationLine   = data.getFileName() + "\t" + contaminationString[1] + "\n"
                                if fileCount == 1: contaminationLine = "#FILENAME\t"+contaminationString[0]+"\n"+contaminationLine
                            else:
                                print "          CONTAMINATION :: %s :: CONTAMINATION DOES NOT EXISTS !!!!" % (fileName)


                            fastqc = data.getPlugin('fastqc')
                            if fastqc is not None and fastqc.valid():
                                fastQcFile = fastqc['fastQcFile']
                                if fastQcFile is not None:
                                    fastQcFilePath = os.path.join(projectBase, setup.tmpFolder, data.getTmpFolder(), fastQcFile)
                                    if os.path.exists(fastQcFilePath):
                                        print "          FASTQC :: %s :: FASTQC (%s) EXISTS" % (fileName, fastQcFilePath)
                                        dstShort       = data.getFileName()
                                        dstFQCLong     = dstShort.replace(  "/", "_")
                                        dstFQCLong     = dstFQCLong.replace(".", "_")

                                        if setup.exportToFile:
                                            print "          FASTQC :: %s :: FASTQC OUT DIR %s" % (fileName, outFqDir)
                                            if not os.path.exists(outFqDir):
                                                try:            os.makedirs(outFqDir)
                                                except OSError: pass

                                            dstFQCFileName = os.path.join(outFqDir, dstFQCLong + "_fastQC.zip")
                                            #TODO. DELETE FILE IN _TMP
                                            print "          FASTQC :: %s :: SRC %s\n" % (fileName, fastQcFilePath)
                                            print "          FASTQC :: %s :: DST %s\n" % (fileName, dstFQCFileName)

                                            try:            os.remove(dstFQCFileName)
                                            except OSError: pass

                                            shutil.copyfile(fastQcFilePath, dstFQCFileName)

                                            if not os.path.exists(dstFQCFileName):
                                                print "          FASTQC :: %s :: ERROR COPYING FASTQC REPORT" % (fileName)
                                                sys.exit(2)
                                    else:
                                        print "          FASTQC :: %s :: NO FASTQC (%s) EXISTS. ERROR" % (fileName, fastQcFilePath)
                                        sys.exit(2)
                                else:
                                    print "          FASTQC :: %s :: NO FASTQC FILE DEFINED !!!!" % (fileName)
                            else:
                                print "          FASTQC :: %s :: NO FASTQC !!!!" % (fileName)


                            reportPdf = os.path.join(projectBase, setup.tmpFolder, data.getTmpFolder(), 'report.pdf')
                            libReports.append(reportPdf)
                            if not os.path.exists(reportPdf) or setup.redoReport:
                                templater.parseDb(data)

                            if not os.path.exists(reportPdf):
                                print "NOT ABLE TO CREATE PDF REPORT %s" % reportPdf
                                sys.exit(1)


                            dstRepLong    = data.getFileName().replace(  "/", "_").replace(".", "_")
                            outFileReport = reportDstFile + sampleIdName + '_' + sequenceTech + '_' + sampleLibrary + '_' + dstRepLong + '.pdf'
                            if os.path.exists(outFileReport):
                                os.remove(outFileReport)
                            shutil.copy(reportPdf, outFileReport)
                            libReports.append(outFileReport)


                            sumShort                    = data.getFileName()

                            fileSum = {}
                            fileSum['FILENAME'        ] = sumShort
                            for x in headers:
                                try:
                                    dataFileSum   = data.getPluginResult(x[0], x[1])
                                except KeyError:
                                    print data.pp()
                                    print "KEY ERROR %s"%x
                                    sys.exit(1)
                                fileSum[x[2]] = dataFileSum


                            #print pprint.pformat(fileSum)

                            for x in titles:
                                if x[1] is not None:
                                    if x[1] == 'SUM':
                                        try:
                                            libSum[x[0]] += fileSum[x[0]]
                                        except KeyError:
                                            libSum[x[0]]  = fileSum[x[0]]
                                    elif x[1] == 'AVG':
                                        try:
                                            libSum[x[0] + 'SUM'  ] += fileSum[x[0]]
                                            libSum[x[0] + 'COUNT'] += 1
                                            libSum[x[0]]            = libSum[x[0] + 'SUM'  ] / libSum[x[0] + 'COUNT']
                                        except KeyError:
                                            libSum[x[0] + 'SUM'  ]  = fileSum[x[0]]
                                            libSum[x[0] + 'COUNT']  = 1
                                            libSum[x[0]]            = fileSum[x[0]]

                            dataHash = data.getPluginResult('hash', 'hash')
                            shaline  = dataHash + "  " + sumShort + "\n"
                            sumline  = "\t".join([x[2] % fileSum[x[0]] for x in titles]) + "\n"

                            if setup.debug or not setup.exportToFile: print "SHA LINE ( STATUS %s ) [FILE] %s" % (projectStatus, shaline)
                            if setup.debug or not setup.exportToFile: print "SUM LINE ( STATUS %s ) [FILE] %s" % (projectStatus, sumline)

                            if setup.exportToFile:
                                dstShaFile.write(shaline)
                                dstSumFile.write(sumline)
                                if contamination is not None:
                                    dstConFile.write(contaminationLine)


                        #pp(fileSum)
                        sumVals = []
                        for x in titles:
                            if x[1] is not None:
                                if x[1] == 'SUM':
                                    try:
                                        techSum[x[0]] += libSum[x[0]]
                                    except KeyError:
                                        techSum[x[0]]  = libSum[x[0]]
                                elif x[1] == 'AVG':
                                    try:
                                        techSum[x[0] + 'SUM'  ] += libSum[x[0]]
                                        techSum[x[0] + 'COUNT'] += 1
                                        techSum[x[0]]            = techSum[x[0] + 'SUM'  ] / techSum[x[0] + 'COUNT']
                                    except KeyError:
                                        techSum[x[0] + 'SUM'  ]  = libSum[x[0]]
                                        techSum[x[0] + 'COUNT']  = 1
                                        techSum[x[0]]            = libSum[x[0]]
                                sumVals.append(x[2] % libSum[x[0]])
                            else:
                                if x[0] == "FILENAME":
                                    sumVals.append("PROJECT "+projectName+" SAMPLE "+sampleId+" TECH "+sequenceTech+" LIB "+sampleLibrary)
                                else:
                                    sumVals.append('-')

                        sumline  = "\t".join([x for x in sumVals]) + "\n"
                        if setup.debug or not setup.exportToFile: print "SUM LINE ( STATUS %s ) [LIB] %s" % (projectStatus, sumline)
                        if setup.exportToFile and len(libs) > 1: dstSumFile.write(sumline)

                        if setup.mergePdfs:
                            libReports.sort()
                            outLibReport      = reportDstFile + sampleIdName + '_' + sequenceTech + '_' + sampleLibrary + '.pdf'
                            outLibReportFiles = mergePdfs(libReports, outLibReport)
                            techReports.extend(outLibReportFiles)

                    if setup.mergePdfs:
                        techReports.sort()
                        outTechReport      = reportDstFile + sampleIdName + '_' + sequenceTech + '.pdf'
                        outTechReportFiles = mergePdfs(techReports, outTechReport)
                        sampleReports.extend(outTechReportFiles)



                    #pp(techSum)
                    sumVals = []
                    for x in titles:
                        if x[1] is not None:
                            if x[1] == 'SUM':
                                try:
                                    sampleSum[x[0]] += techSum[x[0]]
                                except KeyError:
                                    sampleSum[x[0]]  = techSum[x[0]]
                            elif x[1] == 'AVG':
                                try:
                                    sampleSum[x[0] + 'SUM'  ] += techSum[x[0]]
                                    sampleSum[x[0] + 'COUNT'] += 1
                                    sampleSum[x[0]]            = sampleSum[x[0] + 'SUM'  ] / sampleSum[x[0] + 'COUNT']
                                except KeyError:
                                    sampleSum[x[0] + 'SUM'  ]  = techSum[x[0]]
                                    sampleSum[x[0] + 'COUNT']  = 1
                                    sampleSum[x[0]]            = techSum[x[0]]
                            sumVals.append(x[2] % techSum[x[0]])
                        else:
                            if x[0] == "FILENAME":
                                sumVals.append("PROJECT " + projectName + " SAMPLE " + sampleId + " TECH " + sequenceTech)
                            else:
                                sumVals.append('-')

                    sumline  = "\t".join([x for x in sumVals]) + "\n"
                    if setup.debug or not setup.exportToFile: print "SUM LINE ( STATUS %s ) [TECH] %s" % (projectStatus, sumline)
                    if setup.exportToFile and len(techs) > 1: dstSumFile.write(sumline)


                if setup.mergePdfs:
                    sampleReports.sort()
                    outSampleReport      = reportDstFile + sampleIdName + '.pdf'
                    outSampleReportFiles = mergePdfs(sampleReports, outSampleReport)
                    statusReports.extend(outSampleReportFiles)


                #pp(sampleSum)
                sumVals = []
                for x in titles:
                    if x[1] is not None:
                        if x[1] == 'SUM':
                            try:
                                statusSum[x[0]] += sampleSum[x[0]]
                            except KeyError:
                                statusSum[x[0]]  = sampleSum[x[0]]
                        elif x[1] == 'AVG':
                            try:
                                statusSum[x[0] + 'SUM'  ] += sampleSum[x[0]]
                                statusSum[x[0] + 'COUNT'] += 1
                                statusSum[x[0]]            = statusSum[x[0] + 'SUM'  ] / statusSum[x[0] + 'COUNT']
                            except KeyError:
                                statusSum[x[0] + 'SUM'  ]  = sampleSum[x[0]]
                                statusSum[x[0] + 'COUNT']  = 1
                                statusSum[x[0]]            = sampleSum[x[0]]
                        sumVals.append(x[2] % sampleSum[x[0]])
                    else:
                        if x[0] == "FILENAME":
                            sumVals.append("PROJECT "+projectName+" SAMPLE "+sampleId)
                        else:
                            sumVals.append('-')

                sumline  = "\t".join([x for x in sumVals]) + "\n"
                if setup.debug or not setup.exportToFile: print "SUM LINE ( STATUS %s ) [SAMPLE] %s" % (projectStatus, sumline)
                if setup.exportToFile:
                    #if len(samples) > 1:
                    dstSumFile.write(sumline)
                    dstSumLibFile.write(sumline)

            if setup.mergePdfs:
                statusReports.sort()
                outStatusReport      = os.path.join(projectBase, dstSum, 'report_' + projectName + '.pdf')
                outStatusReportFiles = mergePdfs(statusReports, outStatusReport, force=True)
                projectReports.extend(outStatusReportFiles)



            #pp(statusSum)
            sumVals = []
            for x in titles:
                if x[1] is not None:
                    if x[1] == 'SUM':
                        try:
                            projectSum[x[0]] += statusSum[x[0]]
                        except KeyError:
                            projectSum[x[0]]  = statusSum[x[0]]
                    elif x[1] == 'AVG':
                        try:
                            projectSum[x[0] + 'SUM'  ] += statusSum[x[0]]
                            projectSum[x[0] + 'COUNT'] += 1
                            projectSum[x[0]]            = projectSum[x[0] + 'SUM'  ] / projectSum[x[0] + 'COUNT']
                        except KeyError:
                            projectSum[x[0] + 'SUM'  ]  = statusSum[x[0]]
                            projectSum[x[0] + 'COUNT']  = 1
                            projectSum[x[0]]            = statusSum[x[0]]
                    sumVals.append(x[2] % statusSum[x[0]])
                else:
                    if x[0] == "FILENAME":
                        sumVals.append("PROJECT "+projectName)
                    else:
                        sumVals.append('-')



            if setup.exportToFile:
                print "  STATUS %s :: CLOSING FILES" % (projectStatus)
                dstShaFile.close()
                dstConFile.close()
                dstSumFile.close()
                dstSumLibFile.close()

                if setup.replaceFiles:
                    print "  STATUS %s :: SAVING FILES" % (projectStatus)
                    try:            os.remove(dstShaFileName    )
                    except OSError: pass
                    try:            shutil.move(dstShaFileName    + '.tmp', dstShaFileName )
                    except IOError: pass


                    try:            os.remove(dstConFileName    )
                    except OSError: pass
                    try:            shutil.move(dstConFileName    + '.tmp', dstConFileName)
                    except IOError: pass

                    try:            os.remove(dstSumFileName    )
                    except OSError: pass
                    try:            shutil.move(dstSumFileName    + '.tmp', dstSumFileName)
                    except IOError: pass

                    try:            os.remove(dstSumLibFileName )
                    except OSError: pass

                    try:            shutil.move(dstSumLibFileName + '.tmp', dstSumLibFileName)
                    except IOError: pass

                    if os.path.exists(outFqDir):
                        if os.path.exists(outFqDirFinal):
                            shutil.rmtree(outFqDirFinal)

                        os.rename(outFqDir, outFqDirFinal)

                    if os.path.exists(dstShaFileName   ) and os.path.getsize(dstShaFileName   ) == 0:
                        print "  STATUS %s :: SHASUM FILE %s IS EMPTY. DELETING" % (projectStatus, dstShaFileName)

                        try:
                            os.remove(dstShaFileName)
                        except OSError:
                            pass

                    if os.path.exists(dstConFileName   ) and os.path.getsize(dstConFileName   ) == len(contaminationTitle):
                        print "  STATUS %s :: CONTAMINATION FILE %s IS EMPTY. DELETING" % (projectStatus, dstConFileName)

                        try:
                            os.remove(dstConFileName)
                        except OSError:
                            pass

                    if os.path.exists(dstSumFileName   ) and os.path.getsize(dstSumFileName   ) == len(title             ):
                        print "  STATUS %s :: SUMMARY FILE %s IS EMPTY. DELETING" % (projectStatus, dstSumFileName)

                        try:
                            os.remove(dstSumFileName)
                        except OSError:
                            pass
                    else:
                        #print "  STATUS %s :: SUMMARY FILE %s IS NOT EMPTY (%d vs %d). KEEPING" % (projectStatus, dstSumFileName, [os.path.getsize(f) for f in dstSumFileName if os.path.exists(f)][0], len(title))
                        pass

                    if os.path.exists(dstSumLibFileName) and os.path.getsize(dstSumLibFileName) == len(title             ):
                        print "  STATUS %s :: LIBRARY SUMMARY FILE %s IS EMPTY. DELETING" % (projectStatus, dstSumLibFileName)

                        try:
                            os.remove(dstSumLibFileName)
                        except OSError:
                            pass

        #pp(projectSum)
        #sumVals = []
        #for x in titles:
        #    if x[1]:
        #        sumVals.append(x[2] % projectSum[x[0]])
        #    else:
        #        if x[0] == "FILENAME":
        #            sumVals.append("PROJECT "+projectName)
        #        else:
        #            sumVals.append('-')
        #
        #sumline  = "\t".join([x for x in sumVals]) + "\n"
        #if setup.debug or not exportToFile: print "SUM LINE ( STATUS %s ) [PROJ] %s" % (projectStatus, sumline),
        #if exportToFile:
        #    dstSumFile.write(sumline)
        #    dstSumLibFile.write(sumline)
        pass

        #outProjectReport = os.path.join(projectBase, dstSum, 'report_' + projectName + '.pdf')
        #mergePdfs(projectReports, outProjectReport, force=True)
    print "FINISHED EXPORTING"




class GenData(object):
    #http://codeghar.wordpress.com/2011/05/20/python-script-to-calculate-checksum-of-file/
    """
    General class containing the information of each sequencing file. Its statistics and properties in a
    path free maner so that the data can be moved freely.
    """
    def __init__(               self, inFile, projectName, pairName, projectType, projectStatusName, \
                                sequenceTech, sampleId, sampleLibrary, libType, libSize):
        """
        The initializer.
        inFile              = File path AFTER base path, library name and project status
        project name        = name of the project. first folder after setup.getProjectRoot(self.projectName)
        project type        = mapping or denovo. will serve for pipiline path selection
        project status name = current status name. to be queried through setup.getstatusfolder, etc
        sequence tech       = technology used for this sample. illumina or 454 so far
        sample id           = id grouping samples
        sample library      = sequensing library identifier
        """
        self.update(   inFile, projectName, pairName, projectType, projectStatusName, sequenceTech, sampleId, sampleLibrary, libType, libSize)
        #self.shortName         = inFile[len(projectBase) + 1:]

        self.data           = {}
        self.pluginRegistry = []
        self.addPlugin(Plugins.pluginInfo)
        self.runPlugin('info')
        #print "INFO", self.getPlugin('info')

    def getFileName(            self): return self.infile
    def getprojectName(         self): return self.projectName
    def getPairName(            self): return self.pairName
    def getProjectType(         self): return self.projectType
    def getProjectStatus(       self): return self.projectStatusName
    def getSequenceTech(        self): return self.sequenceTech
    def getSampleId(            self): return self.sampleId
    def getSampleLibrary(       self): return self.sampleLibrary
    def getLibType(             self): return self.libType
    def getLibSize(             self): return self.getLibSize

    def getDstFolders(          self, name): return getDstFolders(self, name)

    def __str__(                self):
        """
        The string representation is its size and modification time. if this changes the sequence will not
        be found back.
        """
        return "%s %d %d" % (self.infile, self.getPluginResult('info', 'size'), self.getPluginResult('info', 'mtime'))

    def __repr__(               self):
        return "CLASS GenData %s" % self.__str__()

    def getTmpFolder(           self):
        """
        returns the folder under the setup.tmpFolder to which all temporary analysis files
        shall be saved
        """
        dstShort    = self.getFileName()
        tmpPath     = dstShort.replace("/", "_")
        tmpPath     = tmpPath.replace(".", "_")
        return tmpPath

    def getProjectBase(         self):
        """
        returns the full path to the project. setup.getProjectRoot(self.projectName) + project name
        """
        return os.path.join(setup.getProjectRoot(self.projectName), self.projectName)

    def getFileNameStatus(      self):
        """
        returns the folder under the project name in which to find the file.
        queries setup.getstatusfolder to find the folder in which files in the
        current status is (preliminar, ready, etc)
        """
        return os.path.join(setup.getStatusFolder(self.getProjectStatus()), self.infile)

    def getFileNameProject(     self):
        """
        return the whole path above setup.getProjectRoot(self.projectName)
        """
        return os.path.join(self.projectName, self.getFileNameStatus())

    def getFileNamePath(        self):
        """
        returns the whole path to the file
        """
        #print "PNAME = %s" % self.projectName
        #print "PROOT = %s" % setup.getProjectRoot(self.projectName)
        #print "FNAME = %s" % self.getFileNameProject()
        return os.path.join(setup.getProjectRoot(self.projectName), self.getFileNameProject())

    def update(                 self, inFile, projectName, pairName, projectType, projectStatusName, \
                                sequenceTech, sampleId, sampleLibrary, libType, libSize):
        """
        Updates the attributes defined in the __init__
        TODO: INCOMPLETE
        """
        self.infile            = inFile
        #self.id                = inFile
        self.projectName       = projectName
        self.pairName          = pairName
        self.projectType       = projectType
        self.projectStatusName = projectStatusName
        self.sequenceTech      = sequenceTech
        self.sampleId          = sampleId
        self.sampleLibrary     = sampleLibrary
        self.libraryType       = libType
        self.librarySize       = libSize

    def eq(                     self, data2):
        """
        checks whether a given data2 data object is equal to the current object
        I was not able to use __eq__ because of pickling
        """
        data1info = self.getPlugin('info')
        data2info = data2.getPlugin('info')

        if data1info is not None:
            if data2info is not None:
                for attr in data2info.keys():
                    if attr == 'parent': continue

                    if attr in data1info:
                        if data1info[attr] == data2info[attr]:
                            pass
                        else:
                            print "ATTR %s DIFFERS %s vs %s" % (attr, str(data1info[attr]), str(data2info[attr]))
                            import pprint
                            pprint.pprint(data1info)
                            pprint.pprint(data2info)
                            return False
                    else:
                        print "ATTR %s (%s) NOT FOUND IN SELF" % (attr, str(data2info[attr]))
                        import pprint
                        pprint.pprint(data1info)
                        pprint.pprint(data2info)
                        return False

                for attr in data1info.keys():
                    if attr == 'parent': continue
                    if attr in data2info:
                        if data1info[attr] == data2info[attr]:
                            pass
                        else:
                            print "ATTR %s DIFFERS %s vs %s" % (attr, str(data1info[attr]), str(data2info[attr]))
                            import pprint
                            pprint.pprint(data1info)
                            pprint.pprint(data2info)
                            return False
                    else:
                        print "ATTR %s (%s) NOT FOUND IN COMP" % (attr, str(data1info[attr]))
                        import pprint
                        pprint.pprint(data1info)
                        pprint.pprint(data2info)
                        return False
            else:
                print "DATA2 IS NONE %s" % str(data2info)
                return False
        else:
            print "SELF HAS NO INFO %s" % str(data1info)
            return False
        return True

    def pp(                     self):
        """
        pretty print in a single string all the values asigned to this file so far
        """
        retStr   = ""
        retStr  += "FILE "            + str(self.infile              ) + " "
        dataKeys = self.data.keys()
        dataKeys.sort()

        for key in dataKeys:
            retStr    += key.upper() + " :: "
            values     = self.getPlugin(key)
            valuesKeys = values.keys()
            valuesKeys.sort()

            for vkey in valuesKeys:
                value   = values[vkey]
                retStr += vkey.upper() + " " + str(value) + " | "


        retStr += "PATH "            + str(self.getFileNamePath()   ) + " "
        retStr += "DIR STATUS "      + str(self.getFileNameStatus() ) + " "
        retStr += "DIR PROJECT "     + str(self.getFileNameProject()) + " "
        retStr += "PROJECT BASE "    + str(self.getProjectBase()    ) + " "

        #qualsP  = self.getPlugin('quals')
        #print "QUALSP", qualsP
        #retStr += "Q>30 PROP "       + str(qualsP.getQPropCumm(30)) + " "
        #retStr += "Q>30 SUM "        + str(qualsP.getQSumCumm( 30)) + " "

        return retStr

    def genData(                self):
        """
        wrapper to generate the quality count and hashing
        """
        dstShort = self.getFileName()

        for key in self.getPlugins():
            print "      %s :: %s :: %s - RUNNING"  % ( self.projectName, dstShort, key.upper() )
            self.runPlugin(key)
            print "      %s :: %s :: %s - FINISHED" % ( self.projectName, dstShort, key.upper() )

    def valid(                  self):
        for key in self.getPlugins():
            pRes = self.getPluginResult(key, 'valid')
            if   pRes is None:
                return False
            elif pRes is False:
                return False
            elif pRes.iscallable():
                return pRes()
        return True

    def getPlugins(             self):
        return self.pluginRegistry

    def hasPlugin(              self, pluginName):
        if pluginName in self.data:
            return True
        else:
            return False

    def hasPluginResult(        self, pluginName, pluginKey):
        if self.hasPlugin(pluginName):
            val = self.data[pluginName]
            if pluginKey in val:
                return True
            else:
                return False
        else:
            return False

    def getPluginResult(        self, pluginName, pluginKey):
        if self.hasPluginResult(pluginName, pluginKey):
            val = self.data[pluginName]
            if pluginKey in val:
                return val[pluginKey]
            else:
                return None
        else:
            return None

    def getPlugin(              self, pluginName):
        if self.hasPlugin(pluginName):
            return self.data[pluginName]
        else:
            return None

    def addPlugin(              self, pluginClas):
        pluginName = pluginClas.className
        print "            ADDING PLUGIN %s" % pluginName
        if not self.hasPlugin(pluginName):
            print "            ADDING PLUGIN %s :: NEW PLUGIN" % pluginName
            self.data[pluginName]           = pluginClas(self)
            self.pluginRegistry.append(pluginName)
        else:
            print "            ADDING PLUGIN %s :: PLUGIN EXISTS" % pluginName
            #pprint.pprint(self.data[pluginName])
            self.data[pluginName]['parent'] = self

    def addPlugins(             self, plugins):
        #plugins =   [   # name             class             order  weight
        #            [ 'compression'  , pluginCompression   , 0,     10 ],
        for pluginNfo in plugins:
            pluginClas = pluginNfo[0]
            self.addPlugin(pluginClas)

    def setPluginResult(        self, pluginName, pluginKey, pluginValue):
        if self.hasPlugin(pluginName):
            self.data[pluginName][pluginKey] = pluginValue

    def runPlugin(              self, pluginName, name="", force=False):
        plugin = self.getPlugin(pluginName)
        plugin.run(name=name, force=force)

    def saveYourself(           self):
        saveIndividualEntity(self, setup.startTime)
