#!/usr/bin/python
#http://scipher.wordpress.com/2010/05/06/simple-python-fastq-parser/
import gzip
preload             = True
bufferSize          =   16 * 1024 * 1024
readLinesBufferSize =  128 * 1024 * 1024

class ParseFastQ(object):
    """Returns a read-by-read fastQ parser analogous to file.readline()"""
    def __init__(self,filePath,headerSymbols=['@','+'], seek=None, until=None):
        """Returns a read-by-read fastQ parser analogous to file.readline().
Exmpl: parser.next()
-OR-
Its an iterator so you can do:
for rec in parser:
... do something with rec ...

rec is tuple: (seqHeader,seqStr,qualHeader,qualStr)
"""
        if filePath.endswith('.gz'):
            self._file = gzip.open( filePath )
        else:
            self._file = open(filePath, 'rU', bufferSize)
            
        self._currentLineNumber = 0
        self._hdSyms            = headerSymbols
        self._lines             = []
        self._until             = until
        self._break             = False
        self._breakReal         = False
        
        if seek is not None:
            if until is not None:
                    assert until <= seek,\
               "** ERROR: SEEK %d > UNTIL %d.\n\Please check setup\n**" % (seek, until)
                    
            self._file.seek(seek)
            lines = []
            self._file.readline()
            for p in range(4):
                lines.append(self._file.readline())
            
            if   lines[0][0] == self._hdSyms[0] and lines[2][0] == self._hdSyms[1]: pass
            elif lines[1][0] == self._hdSyms[0] and lines[3][0] == self._hdSyms[1]: self._file.readline()
            elif lines[0][0] == self._hdSyms[1] and lines[2][0] == self._hdSyms[0]: self._file.readline(); self._file.readline()
            elif lines[1][0] == self._hdSyms[1] and lines[3][0] == self._hdSyms[0]: self._file.readline(); self._file.readline(); self._file.readline()
            else:
                sys.stderr.write("unknown frame. " + str(lines))
                sys.exit(1)
            #              0 1 2 3
            #p 0 q 2 = q   @ s + q.        = +0
            #p 1 q 3 = +   q @ s +   Q.    = +1
            #p 2 q 4 = s   + q @ s   + Q.  = +2
            #none    = @   s + q @   S + Q.= +3

            
            

    def __iter__(self):
        return self
    
    def next(self):
        """Reads in next element, parses, and does minimal verification.
Returns: tuple: (seqHeader,seqStr,qualHeader,qualStr)"""
        # ++++ Get Next Four Lines ++++
        elemList = []

        if self._breakReal:
            raise StopIteration

        if self._break:
            self._breakReal = True
            
        #if preload:
        #    if len(self._lines) <= 4:
        #        self._lines.extend(self._file.readlines(readLinesBufferSize))
            
        for i in range(4):
            #print "READING LINE",
            #if preload:
            #    line = self._lines.pop(0)
            #    #print line
            #else:
            
            line = self._file.readline()
            self._currentLineNumber += 1 ## increment file position
            if line:
                elemList.append(line.strip('\n'))
            else:
                elemList.append(None)
        
        
        # ++++ Check Lines For Expected Form ++++
        trues = [bool(x) for x in elemList].count(True)
        nones = elemList.count(None)
        
        
        # -- Check for acceptable end of file --
        if nones == 4:
            raise StopIteration
        # -- Make sure we got 4 full lines of data --
        assert trues == 4,\
               "** ERROR: It looks like I encountered a premature EOF or empty line.\n\
Please check FastQ file near line number %s (plus or minus ~4 lines) and try again\n%s**" % (self._currentLineNumber, str(elemList))
        # -- Make sure we are in the correct "register" --
        assert elemList[0][0] == self._hdSyms[0],\
               "** ERROR: The 1st line in fastq element does not start with '%s'.\n\
Please check FastQ file near line number %s (plus or minus ~4 lines) and try again\n%s**" % (self._hdSyms[0],self._currentLineNumber, str(elemList))
        assert elemList[2][0] == self._hdSyms[1],\
               "** ERROR: The 3rd line in fastq element does not start with '%s'.\n\
Please check FastQ file near line number %s (plus or minus ~4 lines) and try again\n%s**" % (self._hdSyms[1],self._currentLineNumber, str(elemList))
        # -- Make sure the seq line and qual line have equal lengths --
        assert len(elemList[1]) == len(elemList[3]), "** ERROR: The length of Sequence data and Quality data of the last record aren't equal.\n\
Please check FastQ file near line number %s (plus or minus ~4 lines) and try again\n%s**" % (self._currentLineNumber, str(elemList))
        
        pos = self._file.tell()
        if self._until is not None and pos > self._until:
            self._break = True
        # ++++ Return fatsQ data as tuple ++++
        return tuple(elemList)
        
if __name__ == '__main__':
    import sys
    parser = ParseFastQ(sys.argv[1])

    for record in parser:
        seqHeader  = record[0]
        seqStr     = record[1]
        qualHeader = record[2]
        qualStr    = record[3]
        print "HEADER",seqHeader
        print "QUAL  ",qualStr