#!/usr/bin/python
import sys,os,time,inspect
import pprint
fullPath        = os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
#print fullPath
sys.path.insert(0, fullPath)

inTemplate = os.path.join(fullPath, 'templateOdf/template01.odt')
import setup
from templateOdf import odfParser


def sendRequest(outFile, variables, images):
    odfParser.odfConverter(inTemplate, outFile, variables, images)

def genTable(data):
    return ""

def formatVariables(variables):
    outvars = {
        'projectName'                    : variables['projectName'      ],
        'projectStatusName'              : variables['projectStatusName'],
        'projectType'                    : variables['projectType'      ],
        'sampleId'                       : variables['sampleId'         ],
        'sequenceTech'                   : variables['sequenceTech'     ],
        'librarySize'                    : variables['librarySize'      ],
        'libraryType'                    : variables['libraryType'      ],
        'infile'                         : variables['infile'           ],
        'pairName'                       : variables['pairName'         ],
        'data'                           : {
            'info'                          : {
                                                    'mtime': time.ctime(variables['data']['info']['mtime']),
                                                    'size' : '{:,d}'.format(variables['data']['info']['size' ]) + " (%.2f GB)" % (variables['data']['info']['size' ]*1.0 / 1024/1024/1024)
                                                },
            'quals'                          : {
                                                    'COV'          :               "%.2f"    % variables['data']['quals']['COV'          ],
                                                    'Ns'           : '{:,d}'.format(variables['data']['quals']['Ns'           ]),
                                                    'Ns%'          :               "%6.2f%%" % variables['data']['quals']['Ns%'          ],
                                                    'Q30'          :               "%6.2f%%" % variables['data']['quals']['Q30'          ],
                                                    'adaptamerSum%':               "%6.2f%%" % variables['data']['quals']['adaptamerSum%'],
                                                    'numSeqs'      : '{:,d}'.format(variables['data']['quals']['numSeqs'      ]),
                                                    'seqAvgLen'    : '{:,.1f}'.format(variables['data']['quals']['seqAvgLen'    ])
                                                },
            #'contamination'                    : {
            #                                        'contaminationData': genTable(variables['data']['contamination']['contaminationData'])
            #                                    }
            }
        }
    return outvars

def parseDb(data):
    tmpFolder   = data.getTmpFolder()
    projectName = data.projectName
    projectBase = os.path.join(setup.getProjectRoot(projectName), projectName)
    baseFolder  = os.path.join(projectBase, setup.tmpFolder, tmpFolder)
    images      = {}

    fastQcFile = data.getPluginResult('fastqc', 'fastQcFile')
    if fastQcFile is not None:
        #fastQcFilePath = os.path.join(baseFolder, fastQcFile)
        images["Per sequence quality scores"  ] = os.path.join(baseFolder, 'seq_fastqc', 'Images', 'per_sequence_quality.png'        )
        images["Sequence Length Distribution" ] = os.path.join(baseFolder, 'seq_fastqc', 'Images', 'sequence_length_distribution.png')
        images["Sequence Duplication Levels"  ] = os.path.join(baseFolder, 'seq_fastqc', 'Images', 'duplication_levels.png'          )
        images["Per base N content"           ] = os.path.join(baseFolder, 'seq_fastqc', 'Images', 'per_base_n_content.png'          )
        images["Per base sequence content"    ] = os.path.join(baseFolder, 'seq_fastqc', 'Images', 'per_base_sequence_content.png'   )
        images["Per base sequence quality"    ] = os.path.join(baseFolder, 'seq_fastqc', 'Images', 'per_base_quality.png'            )
        images["Per base GC content"          ] = os.path.join(baseFolder, 'seq_fastqc', 'Images', 'per_base_gc_content.png'         )
        images["Kmer Content"                 ] = os.path.join(baseFolder, 'seq_fastqc', 'Images', 'kmer_profiles.png'               )
        images["Per sequence GC content"      ] = os.path.join(baseFolder, 'seq_fastqc', 'Images', 'per_sequence_gc_content.png'     )

        #['Sequence Duplication Levels' , 'duplication_levels.png'          ],
        #['Per base GC content'         , 'per_base_gc_content.png'         ],
        #['Per base N content'          , 'per_base_n_content.png'          ],
        #['Per base sequence quality'   , 'per_base_quality.png'            ],
        #['Per base sequence content'   , 'per_base_sequence_content.png'   ],
        #['Per sequence GC content'     , 'per_sequence_gc_content.png'     ],
        #['Per sequence quality scores' , 'per_sequence_quality.png'        ],
        #['Sequence Length Distribution', 'sequence_length_distribution.png'],
        #['Kmer Content'                , 'kmer_profiles.png'               ],
        #['Basic Statistics'            , None                              ],
        #['Overrepresented sequences'   , None                              ]


    contam            = data.getPlugin('contamination')
    if contam is not None:
        images["contaminationGraph"           ] = os.path.join(baseFolder, 'seq_screen.png'                                        )

    #pprint.pprint(images)

    variables   = formatVariables(data.__dict__)
    #pprint.pprint(variables, indent=4, depth=6)

    outFile     = os.path.join(baseFolder, 'report.pdf')
    sendRequest(outFile, variables, images)

    #contamination:"contaminationGraph"
    #fastqc:"duplication levels":
    #fastqc:"per base gc content":
    #fastqc:"per base n content":
    #fastqc:"per base quality":
    #fastqc:"per base sequence content":
    #fastqc:"per sequence gc content":
    #fastqc:"per sequence quality":
    #fastqc:"sequence length distribution":

    #variables = {
    #    'projectName'                    : "project name is experiment",
    #    'projectStatusName'              : "project status is experiment",
    #    'projectType'                    : "project type is denovo",
    #    'sampleId'                       : "sample id is experiment",
    #    'sequenceTech'                   : "sequence techology is experimental",
    #    'librarySize'                    : "library size is 100",
    #    'libraryType'                    : "library type is 454",
    #    'infile'                         : "input file is temp",
    #    'pair_name'                      : "pair name is couple",
    #    'info'                           : {
    #                                            'mtime': "creation time",
    #                                            'size' : "size is big"
    #                                        },
    #    'quals'                          : {
    #                                            'COV'          : "coverage is low",
    #                                            'Ns'           : "Ns is big",
    #                                            'ns%'          : "Ns > 100%",
    #                                            'Q30'          : "Q30 is 40",
    #                                            'adaptamerSum%': "% of adaptamers id weird",
    #                                            'numSeqs'      : "number of sequences is ok",
    #                                            'seqAvgLen'    : "average length is huge",
    #                                        },
    #    'contamination'                    : {
    #                                            'contaminationData': "contamination table",
    #    }
    #}
    #
    #base     = '/home/assembly/tomato150/denovo/arcanum/_tmp/454_20000_HP1TBXB01_sff/'
    #imagesG  = {
    #    #fastqc:fastqcGraphs:
    #    #contamination:contaminationGraph:
    #    "per sequence quality"         : os.path.join(base, 'seq_fastqc/Images/', 'per_sequence_quality.png'),
    #    "sequence length distribution" : os.path.join(base, 'seq_fastqc/Images/', 'sequence_length_distribution.png'),
    #    "duplication levels"           : os.path.join(base, 'seq_fastqc/Images/', 'duplication_levels.png'),
    #    "per base n content"           : os.path.join(base, 'seq_fastqc/Images/', 'per_base_n_content.png'),
    #    "per base sequence content"    : os.path.join(base, 'seq_fastqc/Images/', 'per_base_sequence_content.png'),
    #    "per base quality"             : os.path.join(base, 'seq_fastqc/Images/', 'per_base_quality.png'),
    #    "per base gc content"          : os.path.join(base, 'seq_fastqc/Images/', 'per_base_gc_content.png'),
    #    "kmer profile"                 : os.path.join(base, 'seq_fastqc/Images/', 'kmer_profiles.png'),
    #    "per sequence gc content"      : os.path.join(base, 'seq_fastqc/Images/', 'per_sequence_gc_content.png'),
    #    "contaminationGraph"           : os.path.join(base, 'seq_screen.png')
    #}

    pass



#if __name__ == '__main__': main()
