import sys,os,time,inspect,traceback
import fnmatch
fullPath        = os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, fullPath)
sys.path.insert(0, os.path.abspath(os.path.join(fullPath, '..')))
from shared import *
from tools  import *
import dbdump

class generalError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

class decompressFastQError(generalError):
    def __init__(self, message):
        generalError.__init__(self, message)

# COMPRESSION
class pluginCompression(plugin):
    className = "compression"
    outFn     = 'seq.fastq'
    def __init__(               self, parent):
        self['parent'          ] = parent
        self['decompressed'    ] = False
        self['compressOK'      ] = False
        self['decompressedFile'] = None
        self['isCompressed'    ] = True
        self.printStats('INITIALIZING')

    def valid(                  self):
        self.printStats('CHECKING VALIDITY')
        if self.redo():
            self.printStats('CHECKING VALIDITY :: REDOING')
            return False

        base, outPath, outFileBase, outPathTmp, outPathFull, outFile = getDstFolders(self['parent'], self.outFn)
        if self['isCompressed']:
            if  os.path.exists(outFile          ) and \
                os.path.exists(outFile + '.t.ok') and \
                os.path.exists(outFile + '.ok'  ):
                if self['decompressed'] and self['compressOK']:
                    self.printStats('CHECKING VALIDITY :: VALID 1')
                    self['parent'].saveYourself()
                    return True
                else:
                    self['decompressed'    ] = True
                    self['compressOK'      ] = True
                    self['decompressedFile'] = self.outFn
                    self.printStats('CHECKING VALIDITY :: VALID 2')
                    self['parent'].saveYourself()
                    return True
            else:
                self.printStats('CHECKING VALIDITY :: NOT VALID 1')
                return False
        else:
            if  os.path.exists(outFile          ) and \
                os.path.exists(outFile + '.ok'  ):
                self.printStats('CHECKING VALIDITY :: VALID 3')
                #self['parent'].saveYourself()
                return True
            else:
                self.printStats('CHECKING VALIDITY :: NOT VALID 2')
                return False

    def run(                    self, name="", force=False):
        if name is "":
            name = self.className

        if not self.valid() or force:
            return self.decompressFastQ(name=name)

    def getCompress(            self):
        """
        checks if file is compressed or not (using setup.extensions) and returns the
        extension description on how to deal with it. If not compressed, return NONE.
        """
        for extNum in range(len(setup.constants.extensions)):
            ext    = setup.constants.extensions[extNum]
            if fnmatch.fnmatch(self['parent'].getFileNamePath(), ext[0]):
                return ext
        return None

    def getFastQPath(           self):
        dataObj     = self['parent']
        base, outPath, outFileBase, outPathTmp, outPathFull, outFile = getDstFolders(dataObj, self.outFn)
        return outFile

    def decompressFastQ(        self, name=""):
        """
        Wrapper to facilitate the decompression of fastq files. Using the information
        in the data class, creates a folder in the TMP folder and decompress the fastq
        file in it using a standard protocol. Also, let the data class know that the file
        has been decompressed and where.
        """
        dataObj     = self['parent']
        filename    = dataObj.getFileNamePath()
        dstShort    = dataObj.getFileName()                    # 601/illumina/pairedend_500/601_CGATGT_L002_R1_001.fastq.gz
        base, outPath, outFileBase, outPathTmp, outPathFull, outFile = getDstFolders(dataObj, self.outFn)

        if not os.path.exists(outPathFull):
            os.makedirs(outPathFull)
        self.printStats("OUT FASTQ %s" % (outFile))


        outFileTest = outFile + ".t.ok"
        outFileDec  = outFile + ".ok"
        self.printStats("CHECKING :: TEST %s DESCOMP %s" % (outFileTest, outFileDec))

        for mFile in [outFile, outFileTest, outFileDec]:
            if os.path.exists(mFile):
                os.remove(mFile)

        comp   = self.getCompress()

        if comp[5]: # if compressed
            self.printStats("CHEKING :: COMPRESSED :: %s" % (str(comp)))
            self['isCompressed'] = True

            if     not os.path.exists(outFileTest):# \
                #or not dataObj.hasPluginResult('compression', 'compressOK') \
                #or not dataObj.getPluginResult('compression', 'compressOK'):
                self.printStats("CHEKING :: NOT TESTED :: TESTING")

                self.printStats("VERIFYING :: EXISTS %s COMPRESS OK %s" % \
                (os.path.exists(outFileDec), self['compressOK']))

                resTest = testCompression(comp, \
                            { 'in': filename }, self.getPrintStr()+" CHECKING :: OUT PATH %s" % (outPath))

                if resTest != 0:
                    self.printStats("VERIFYING :: OUT PATH %s :: ZIP ERROR" % (outPath))
                    self['compressOK'] = False

                    traceback.print_stack()
                    traceback.print_exc()
                    msg    = self.getPrintStr()+" VERIFYING \"%s\" WITH \"%s\"" % (filename, comp[3] % { 'in': filename })
                    reason = "ZIP TEST ERROR. RETURNED %d" % (resTest)
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    raise decompressFastQError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))

                else:
                    self.printStats("VERIFYING :: OUT PATH %s :: ZIP OK" % (outPath))
                    with open(outFileTest, 'w') as f:
                        f.write('ok')
                    self['compressOK'] = True

            else:
                self.printStats("CHEKING :: ALREADY TESTED :: SKIPPING TEST")
                self.printStats("VERIFYING :: OUT PATH %s :: ZIP OK :: EXISTS %s HAS %s" % \
                               (outPath, str(os.path.exists(outFileTest)), self['compressOK']))

                with open(outFileTest, 'w') as f:
                    f.write('ok')
                self['compressOK'] = True



            if     not os.path.exists(outFile   ) \
                or not os.path.exists(outFileDec) \
                or self.redo():
                #or not dataObj.hasPluginResult('compression', 'decompressed') \
                #or not dataObj.getPluginResult('compression', 'decompressed') \
                self.printStats("CHEKING :: NOT DECOMPRESSED :: DECOMPRESSING")

                self.printStats("DECOMPRESSING :: EXISTS %s = %s | %s = %s DECOMPRESSED %s" % \
                (   outFile,    os.path.exists(outFile   ), \
                    outFileDec, os.path.exists(outFileDec), \
                    self['decompressed']))

                try:
                    if os.path.exists(outFile):
                        os.remove(outFile)
                    else:
                        self.printStats("removed file")
                except OSError:
                    self.printStats("no file to be removed")

                try:
                    if os.path.exists(outFileDec):
                        os.remove(outFileDec)
                    else:
                        self.printStats("removed file")
                except OSError:
                    self.printStats("no file to be removed")


                outFileFileName = outFile


                resDec = decompressFile(comp, { 'in': filename, 'out': outFileFileName }, \
                            self.getPrintStr()+" DECOMPRESSING :: OUT PATH %s" % (outPath))

                if resDec != 0:
                    self.printStats("DECOMPRESSING :: OUT PATH %s :: DECOMPRESS ERROR" % (outPath))
                    self['decompressed'] = False

                    try:
                        os.remove(outFile)
                    except OSError:
                        self.printStats("no file to be removed")
                    else:
                        self.printStats("removed file")

                    try:
                        if os.path.exists(outFileDec):
                            os.remove(outFileDec)
                        else:
                            self.printStats("removed file")
                    except OSError:
                        self.printStats("no file to be removed")

                    traceback.print_stack()
                    traceback.print_exc()
                    msg    = self.getPrintStr()+" DECOMPRESSING \"%s\" WITH \"%s\"" % (filename, comp[2] % { 'in': filename, 'out': outFileFileName })
                    reason = "ZIP DECOMPRESS ERROR. RETURNED %d" % (resTest)
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    raise decompressFastQError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))

                else:
                    self.printStats("DECOMPRESSING :: OUT PATH %s :: DECOMPRESS OK" % (outPath))

                    with open(outFileDec, 'w') as f:
                        f.write('ok')
                    self['decompressed'    ] = True
                    self['decompressedFile'] = os.path.basename(outFileBase)
            else: #already decompressed
                self.printStats("CHEKING :: ALREADY DECOMPRESSED :: SKIPPING DECOMPRESSING")
                self.printStats("DECOMPRESSING :: OUT PATH %s :: ALREADY DECOMPRESSED :: EXISTS %s = %s | %s = %s HAS %s" % \
                                    (outPath,        \
                                    outFile,    str(os.path.exists(outFile)),    \
                                    outFileDec, str(os.path.exists(outFileDec)), \
                                    self['compressOK']
                                    )
                                )

                with open(outFileDec, 'w') as f:
                    f.write('ok')
                self['decompressed'    ] = True
                self['decompressedFile'] = os.path.basename(outFileBase)
        else:
            self.printStats("CHEKING :: NOT COMPRESSED :: %s" % (str(comp)))
            self.printStats("DECOMPRESSING :: OUT PATH %s :: NO DECOMPRESSION NEEDED" % (outPath))
            self['isCompressed'] = False

            try:
                os.remove(outFile)
            except OSError:
                print "no file to be removed"
            else:
                print "removed file"

            try:
                if os.path.exists(outFileDec):
                    os.remove(outFileDec)
                else:
                    self.printStats("removed file")
            except OSError:
                self.printStats("no file to be removed")

            try:
                self.printStats("DECOMPRESSING :: OUT PATH %s :: COPYING %s to %s" % (outPath, filename, outFile))
                #shutil.copyfile(filename, outFile)
                os.link(filename, outFile)
                self['decompressed    '] = True
                self['decompressedFile'] = os.path.basename(outFileBase)
                with open(outFileDec, 'w') as f:
                    f.write('ok')
            except:
                traceback.print_stack()
                traceback.print_exc()
                msg    = self.getPrintStr()+" DECOMPRESSING \"%s\"" % (name.upper())
                reason = "ERROR COPYING FROM %s TO %s" % (filename, outFile)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                raise decompressFastQError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))

        #self.filename    = filename
        #self.dstShort    = dstShort
        #self.base        = base
        #self.outPath     = outPath
        #self.outPathTmp  = outPathTmp
        #self.outPathFull = outPathFull
        #self.outFq       = outFq

        return [filename, dstShort, base, outPath, outPathTmp, outPathFull, outFile]

    def getDecompressedFilePath(self):
        """
        returns the whole path to the decompressed version of this file
        """
        return os.path.join(setup.getProjectRoot(self['parent'].projectName), self['parent'].getprojectName(), \
                setup.tmpFolder, self['parent'].getTmpFolder(), self['decompressedFile'])




def testCompression(comp, dataObj, name):
    """
    wrapper to facilitate the checking of a compressed files.
    Getting a standard comp list contaning data about the format and
    how to decompress it. Run the command in the shell using the runstring
    library
    """
    #extensions = [
    #        #extension     compression rate   compression test technology name
    #        ['*.fastq.gz', 2.7,               'gunzip -t', 'illumina'],
    #        ['*.fastq'   , 1.0,               None       , 'illumina'],
    #        #['*.sff'     , 1.0,               None       , '454'     ],
    #    ]

    extStr = comp[0]
    rate   = comp[1]
    unzip  = comp[2]
    test   = comp[3]
    tech   = comp[4]

    if test is not None:
        for testStr in test:
            resTest = run.runString(name, testStr % dataObj )
            if resTest != 0:
                return resTest
        return 0
    else:
        return 0

def decompressFile(comp, dataObj, name):
    """
    wrapper to facilitate the decompression of a compressed files.
    Getting a standard comp list contaning data about the format and
    how to decompress it. Run the command in the shell using the runstring
    library
    """
    extStr = comp[0]
    rate   = comp[1]
    unzip  = comp[2]
    test   = comp[3]
    tech   = comp[4]

    if unzip is not None:
        for testStr in unzip:
            resDesc = run.runString(name, testStr % dataObj )
            if resDesc != 0:
                return resDesc
        return 0
    else:
        return 0
