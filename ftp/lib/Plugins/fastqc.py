import sys,os,time,inspect,traceback
fullPath        = os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, fullPath)
sys.path.insert(0, os.path.abspath(os.path.join(fullPath, '..')))

import shutil
from shared import *
from tools  import *
import dbdump

setupExe = setup.constants.programs['fastqc']

class genFastqcError(generalError):
    def __init__(self, message):
        generalError.__init__(self, message)




# FASTQC
class pluginFastQC(plugin):
    className = "fastqc"
    outFn     = 'seq_fastqc.zip'
    def __init__(    self, parent):
        self['parent'      ] = parent
        self['fastQcOK'    ] =  False
        self['fastqcGraphs'] =  {}
        self['fastQcFile'  ] = None

    def valid(       self):
        """
        Checks whether the contamination analysis has succeeded and whether all
        the data has been loaded properly
        """
        self.printStats('CHECKING VALIDITY')
        if self.redo():
            self.printStats('CHECKING VALIDITY :: REDOING')
            return False

        base, outPath, outFileBase, outPathTmp, outPathFull, outFile = getDstFolders(self['parent'], self.outFn)
        outFileOk      = outFile + '.ok'
        outPathFullSub = os.path.join(outPathFull, 'seq_fastqc')

        if  self['fastQcOK'] and \
            os.path.exists(outFile) and \
            os.path.exists(outPathFullSub) and \
            os.path.exists(outFileOk):

            self.printStats('CHECKING VALIDITY :: VALID 1')
            return True

        else:
            if os.path.exists(outFile) and os.path.exists(outPathFullSub) and os.path.exists(outFileOk):
                self.parseResult()
                if  self['fastQcOK']:
                    self.printStats('CHECKING VALIDITY :: VALID 2')
                    self['parent'].saveYourself()
                    return True
                else:
                    self.printStats('CHECKING VALIDITY :: NOT VALID 1')
                    self.printStats("CHECKING VALIDITY :: NOT VALID 1 :: FILES :: " + str(getDstFolders(self['parent'], self.outFn)))
                    self['parent'].saveYourself()
                    return False
            else:
                self.printStats('CHECKING VALIDITY :: NOT VALID 2')
                self.printStats("CHECKING VALIDITY :: NOT VALID 2 :: FILES :: " + str(getDstFolders(self['parent'], self.outFn)))
                return False

    def run(         self, name=None, force=False):
        if name is None:
            name = self.className

        if not self.valid() or force:
            return self.runFastQC()

    def clean(       self):
        self['fastQcOK'    ] = False
        self['fastQcFile'  ] = None
        self['fastqcGraphs'] = {}

        base, outPath, outFileBase, outPathTmp, outPathFull, outFile = getDstFolders(self['parent'], self.outFn)
        outFileOk = outFile + '.ok'
        outPathFullSub = os.path.join(outPathFull, 'seq_fastqc')

        try:
            os.remove(outFile)
        except OSError:
            pass

        try:
            os.remove(outFileOk)
        except OSError:
            pass

        try:
            shutil.rmtree(outPathFullSub)
        except OSError:
            pass

    def runFastQC(   self):
        """
        for each data object, run fastqc
        """
        data = self['parent']
        try:

            compression = self['parent'].getPlugin('compression')
            #compression.run(name=self.className)
            outFq       = compression.getDecompressedFilePath()

            base, outPath, outFileBase, outPathTmp, outPathFull, outFile = getDstFolders(self['parent'], self.outFn)
            outFileOk      = outFile + '.ok'
            outPathFullSub = os.path.join(outPathFull, 'seq_fastqc')
            #print "BASE %s OUTPATH %s OUTFILEBASE %s OUTPATHTMP %s OUTPATHFULL %s OUTFILE %s OUTFQ %s" % ( base, outPath, outFileBase, outPathTmp, outPathFull, outFile, outFq )
            #BASE /mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_tiny OUTPATH illumina_MP_5000_3018DAAXX_4_r_fastq
            #OUTFILEBASE illumina_MP_5000_3018DAAXX_4_r_fastq/seq_fastqc.zip
            #OUTPATHTMP /mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_tiny/_tmp
            #OUTPATHFULL /mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_tiny/_tmp/illumina_MP_5000_3018DAAXX_4_r_fastq
            #OUTFILE /mnt/nexenta/assembly/nobackup/dev_150/sample_heinz_tiny/_tmp/illumina_MP_5000_3018DAAXX_4_r_fastq/seq_fastqc.zip
            #OUTFQ seq.fastq
            self.printStats("OUT PATH %s"    % ( base ))


            if self.do():
                fastQcCmd = '%(exe)s --threads %(threads)s --outdir %%(dst)s %%(in)s' % (setupExe)

                if  not os.path.exists(outPathFullSub) or \
                    not os.path.exists(outFile       ) or \
                    not os.path.exists(outFileOk     ) or \
                    not self['fastQcOK'    ]           or \
                    self.redo():

                    self.printStats("RUNNING :: OUT %s :: EXISTS %s %s ; %s %s ; %s %s ; OK %s REDO %s" % \
                                    ( outFile,
                                      outPathFullSub, os.path.exists( outPathFullSub ),
                                      outFile,        os.path.exists( outFile        ),
                                      outFileOk,      os.path.exists( outFileOk      ),
                                     self['fastQcOK'], self.redo() ))

                    self.clean()

                    resTest = run.runString(self.getPrintStr() +" RUNNING :: OUT PATH %s" % \
                                ( base ),  fastQcCmd % { 'in': outFq, 'dst': outPathFull } )

                    if resTest != 0:
                        self.printStats("RUNNING :: ERROR :: %s" % ( base ))

                        try:
                            os.remove(outFile)
                        except OSError:
                            pass

                        try:
                            shutil.rmtree(outPathFullSub)
                        except OSError:
                            pass

                        #saveIndividualEntity(data, setup.startTime)
                    else:
                        self.parseResult()

                else:
                    self.printStats("ALREADY RUN :: OUT %s :: EXISTS %s %s ; %s %s ; %s %s ; OK %s REDO %s" % \
                        ( outFile,
                          outPathFullSub, os.path.exists( outPathFullSub ),
                          outFile,        os.path.exists( outFile        ),
                          outFileOk,      os.path.exists( outFileOk      ),
                         self['fastQcOK'], self.redo() ))
                    self.parseResult()

                if  not os.path.exists(outPathFullSub) or \
                    not self['fastQcOK']:
                    self.printStats("NO RESULT :: CLEANING :: OUT %s :: EXISTS %s OK %s REDO %s" % ( outPathFullSub, os.path.exists(outPathFullSub), self['fastQcOK'], self.redo()))
                    self.clean()

        except Exception, e:
            traceback.print_stack()
            traceback.print_exc()
            msg    = self.getPrintStr()+" UNKNOWN EXCEPTION ON FASTQC TO %s" % data
            reason = "%s" % str(e)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            raise genFastqcError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))

    def getData(self, dataFile):
        #>>Basic Statistics     pass
        ##Measure       Value
        #Filename       seq.fastq
        #File type      Conventional base calls
        #Encoding       Illumina 1.5
        #Total Sequences        202091418
        #Filtered Sequences     0
        #Sequence length        100
        #%GC    35
        #>>END_MODULE
        data = {}
        with open(dataFile, 'r') as d:
            lastSection = None
            for line in d:
                line = line.strip()
                #print "line",line
                if lastSection is None:
                    #print "  01",line[0:1]
                    if line[0:2] == ">>":
                        lastSection,ico = line[2:].split("\t")
                        data[lastSection]         = {'data': [], 'ico': ico}
                    else:
                        continue
                elif line == ">>END_MODULE":
                    lastSection = None
                else:
                    data[lastSection]['data'].append(line)

        return data

    def parseResult(self):
        if not  self['fastQcOK'  ]             or \
                self['fastQcFile'] is None     or \
                len(self['fastqcGraphs']) == 0 or \
                self.redo():

            self.printStats("PARSING :: OK %s FILE %s GRAPHS %d REDO %s" % ( self['fastQcOK'], self['fastQcFile'], len(self['fastqcGraphs']), self.redo() ))

            base, outPath, outFileBase, outPathTmp, outPathFull, outFile = getDstFolders(self['parent'], self.outFn)
            outFileOk      = outFile + '.ok'
            outPathFullSub = os.path.join(outPathFull, 'seq_fastqc')

            self.printStats("PARSING FASTQC :: OUT PATH %s :: FASTQC OK" % (outPath))

            graph = {}
            data  = self.getData( os.path.join(outPathTmp, outPath, 'seq_fastqc', 'fastqc_data.txt'))

            for graphData in [  ['Sequence Duplication Levels' , 'duplication_levels.png'          ],
                                ['Per base GC content'         , 'per_base_gc_content.png'         ],
                                ['Per base N content'          , 'per_base_n_content.png'          ],
                                ['Per base sequence quality'   , 'per_base_quality.png'            ],
                                ['Per base sequence content'   , 'per_base_sequence_content.png'   ],
                                ['Per sequence GC content'     , 'per_sequence_gc_content.png'     ],
                                ['Per sequence quality scores' , 'per_sequence_quality.png'        ],
                                ['Sequence Length Distribution', 'sequence_length_distribution.png'],
                                ['Kmer Content'                , 'kmer_profiles.png'               ],
                                ['Basic Statistics'            , None                              ],
                                ['Overrepresented sequences'   , None                              ]
                            ]:

                graphName               = graphData[0]
                graphFile               = graphData[1]

                if graphFile is not None:
                    outGraph                 = os.path.join(outPath,    'seq_fastqc', 'Images', graphFile)
                    outGraphPath             = os.path.join(outPathTmp,  outGraph)
                    outGraphStr              = image2string(outGraphPath)

                    if graphName not in graph:
                        graph[graphName] = {}

                    graph[graphName]['img']  = outGraphStr

                if graphName in data:
                    if graphName not in graph:
                        graph[graphName] = {}

                    graph[graphName]['data'] = data[graphName]["data"]
                    graph[graphName]['ico' ] = data[graphName]["ico" ]

            self['fastQcOK'    ] = True
            self['fastQcFile'  ] = os.path.basename(outFile)
            self['fastqcGraphs'] = graph

            with open(outFileOk, 'w') as f:
                f.write('ok')
            #print graph
            #sys.exit(1)
