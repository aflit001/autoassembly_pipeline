import sys,os,time,inspect,traceback
fullPath        = os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, fullPath)
sys.path.insert(0, os.path.abspath(os.path.join(fullPath, '..')))
from shared import *
from tools  import *
import dbdump

class generalError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

class createHashError(generalError):
    def __init__(self, message):
        generalError.__init__(self, message)

# HASH
class pluginHash(plugin):
    className = "hash"
    outFn     = 'seq.sha256'
    def __init__(    self, parent):
        self['parent'   ] = parent
        self['validHash'] = False
        self['hash'     ] = None
        self.printStats('INITIALIZING')

    def valid(       self):
        """
        Checks whether the contamination analysis has succeeded and whether all
        the data has been loaded properly
        """
        self.printStats('CHECKING VALIDITY')
        if self.redo():
            self.printStats('CHECKING VALIDITY :: REDOING')
            return False

        base, outPath, outFileBase, outPathTmp, outPathFull, outFile = getDstFolders(self['parent'], self.outFn)
        if  self['validHash']:
            if self['hash'] is not None:
                self.printStats('CHECKING VALIDITY :: VALID 1')
                return True
            else:
                if os.path.exists(outFile):
                    with open(outFile, 'r') as f:
                        sha = f.read()
                        self['hash'] = sha
                    self.printStats('CHECKING VALIDITY :: VALID 2')
                    self['parent'].saveYourself()
                    return True
                else:
                    self.printStats('CHECKING VALIDITY :: NOT VALID 1')
                    self['validHash'] = False
                    self['parent'   ].saveYourself()
                    return False

        else:
            if os.path.exists(outFile):
                self['validHash'] = True
                if self['hash'] is None:
                    with open(outFile, 'r') as f:
                        sha = f.read()
                        self['hash'] = sha
                self.printStats('CHECKING VALIDITY :: VALID 2')
                self['validHash']
                return True
            else:
                self.printStats('CHECKING VALIDITY :: NOT VALID 3')
                return False

    def run(         self, name=None, force=False):
        if name is None:
            name = self.className

        if not self.valid() or force:
            return self.genHash()

    def genHash(     self):
        """
        generates the sha256sum checksum of the input file
        """
        self.printStats("CREATING")

        dataObj     = self['parent']
        base, outPath, outFileBase, outPathTmp, outPathFull, outFile = getDstFolders(dataObj, self.outFn)

        if not os.path.exists(outPathFull):
            os.makedirs(outPathFull)

        #print "          ID %s CREATING HASH STP BASE    %s" % (self.id, setup.getProjectRoot(self.projectName))
        #print "          ID %s CREATING HASH PROJ BASE   %s" % (self.id, self.projectName)
        #print "          ID %s CREATING HASH STATUS      %s" % (self.id, self.getStatus())
        #print "          ID %s CREATING HASH TMP FOLDER  %s" % (self.id, self.getTmpFolder())
        #print "          ID %s CREATING HASH PROJ BASE   %s" % (self.id, self.getProjectBase())
        #print "          ID %s CREATING HASH FN STATUS   %s" % (self.id, self.getFileNameStatus())
        #print "          ID %s CREATING HASH FN PROJ     %s" % (self.id, self.getFileNameProject())
        #print "          ID %s CREATING HASH FN PATH     %s" % (self.id, self.getFileNamePath())
        #print "          ID %s CREATING HASH FN DEC PATH %s" % (self.id, self.getDecompressedFilePath())

        infile               = self['parent'].getFileNamePath()
        self.printStats("OUTPUT %s" % (infile))

        try:
            if os.path.exists(outFile):
                os.remove(outFile)

            if self.do():
                hashSum = dbdump.sha256_for_file(infile, runId=self['parent'].infile)

                #print "          ID %s HASH CREATED" % self.id
                self['validHash'] = True
                self['hash']      = hashSum
                self.printStats("FINISHED %s" % (infile))

                with open(outFile, 'w') as f:
                    f.write(hashSum)
            else:
                #print "          ID %s HASH CREATED" % self.id
                self['validHash'] = False
                self['hash']      = "HASH"+str(self['parent'].getPluginResult('info', 'mtime'))

        except IOError, e:
            self['validHash'] = False
            traceback.print_stack()
            traceback.print_exc()
            msg    = self.getPrintStr()+" File could not be opened: %s" % (infile)
            reason = "%s" % str(e)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            raise createHashError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))
        except Exception, e:
            self['validHash'] = False
            traceback.print_stack()
            traceback.print_exc()
            msg    = self.getPrintStr()+' unknown error while generating hash for' + infile + str(e)
            reason = "%s" % str(e)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            raise createHashError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))
