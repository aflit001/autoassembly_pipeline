import sys,os,time,inspect,traceback
fullPath        = os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, fullPath)
sys.path.insert(0, os.path.abspath(os.path.join(fullPath, '..')))
sys.path.insert(0, os.path.abspath(os.path.join(fullPath, '..', '..')))
import fnmatch
from shared import *
from tools  import *
import ParseFastQ
import dbdump

fqFormats     = setup.constants.fqFormats
ADAPTAMERS    = setup.constants.ADAPTAMERS
newblerBorder = setup.constants.newblerBorder
Qborder       = setup.Qborder
genomeSize    = setup.genomeSize


class convertQualsError(generalError):
    def __init__(self, message):
        generalError.__init__(self, message)

class createQualsError(generalError):
    def __init__(self, message):
        generalError.__init__(self, message)



# FASTQ QUALS
class pluginQuals(plugin):
    className = "quals"
    outFn     = 'quals' + dbdump.dumperExt
    def __init__(    self, parent):
        self['parent'       ] = parent
        #self['qualsRaw'     ] = {}
        #self['qualsConvProp'] = {}
        #self['qualsConv'    ] = {}
        self['data'         ] = {}
        self['validQual'    ] = None
        self['formatType'   ] = None

    def valid(       self):
        """
        Checks whether the contamination analysis has succeeded and whether all
        the data has been loaded properly
        """
        self.printStats('CHECKING VALIDITY')
        if self.redo():
            self.printStats('CHECKING VALIDITY :: REDOING')
            return False

        #print "QUALS OUTFN %s PARENT %s" % (self.outFn, self['parent'])
        base, outPath, outFileBase, outPathTmp, outPathFull, outFile    = getDstFolders(self['parent'], self.outFn)

        if os.path.exists(outFile):
            #self.printStats("OUTFILE %s EXISTS" % outFile)
            if  self['validQual']:
                if  len(self['data']) > 0:
                    #self.printStats("OUTFILE %s EXISTS :: DATA IN" % outFile)
                    self.printStats('CHECKING VALIDITY :: VALID 1')
                    return True
                else:
                    self.printStats("CHECKING VALIDITY :: NO DATA :: LOADING %s" % outFile)
                    data = dbdump.loadDump(outFile)
                    if data is not None:
                        self.printStats("CHECKING VALIDITY :: NO DATA :: LOADING OK")
                        #print self
                        #print data
                        #self = data
                        #print self
                        for key in data:
                            self[key] = data[key]
                        self.printStats('CHECKING VALIDITY :: NO DATA :: LOADING OK :: VALID 2')
                        self['parent'].saveYourself()
                        return True
                    else:
                        os.remove(outFile)
                        self['validQual'] = False
                        self.printStats('CHECKING VALIDITY :: NO DATA :: ERROR LOADING :: NOT VALID 1')
                        return False
            else:
                self.printStats('CHECKING VALIDITY :: NOT VALID 2 - not valid qual')
                self.printStats('CHECKING VALIDITY :: NOT VALID 2 :: FILES :: ' + str(getDstFolders(self['parent'], self.outFn)))
                if self['validQual'] is None:
                    data = dbdump.loadDump(outFile)
                    if data is not None:
                        self.printStats("CHECKING VALIDITY :: NOT VALID 2 :: LOADING OK")
                        #print self
                        #print data
                        #self = data
                        #print self
                        for key in data:
                            self[key] = data[key]
                        self.printStats('CHECKING VALIDITY :: NOT VALID 2 :: LOADING OK :: VALID 3')
                        if  self['validQual']:
                            return True

                return False
        else:
            self.printStats('CHECKING VALIDITY :: NOT VALID 3 - out file does not exists %s' % outFile)
            self.printStats('CHECKING VALIDITY :: NOT VALID 3 :: FILES :: ' + str(getDstFolders(self['parent'], self.outFn)))
            return False

    def run(         self, name=None, force=False):
        if name is None:
            name = self.className

        if not self.valid() or force:
            self.genQuals()

    def genQuals(    self):
        """
        goes through the fastq file and count the appearances of each
        char in the quality strings
        also counts how many sequences have a N in it
        """

        infile          = self['parent'].getFileNamePath()
        base, outPath, outFileBase, outPathTmp, outPathFull, outFile    = getDstFolders(self['parent'], self.outFn)

        if os.path.exists(outFile):
            os.remove(outFile)

        self.printStats("CREATING")

        try:
            compress = self['parent'].getPlugin('compression')
            ext      = compress.getCompress()
            if ext is not None:
                extStr = ext[0]
                zipped = ext[1]
                unzip  = ext[2]
                test   = ext[3]
                tech   = ext[4]

                if self.do():
                    #print "          ID %s CREATING QUALS. CHECKING EXT %s TECH %s" % (self.id, extStr, tech)
                    adaptamers = {}
                    proceed    = False
                    if   tech == 'illumina' and fnmatch.fnmatch(infile, extStr):
                        self.printStats("CREATING :: PARSING :: EXT %s" % (extStr))

                        if setup.skipIllumina:
                            proceed    = False
                        else:
                            proceed    = True

                    elif tech == '454' and fnmatch.fnmatch(infile, extStr):
                        adaptamers = ADAPTAMERS
                        self.printStats("CREATING :: PARSING :: EXT %s" % (extStr))

                        if setup.skip454:
                            proceed    = False
                        else:
                            proceed    = True
                    else:
                        self.printStats("UNKNOWN EXTENSION IN %s" % infile)
                        proceed    = False
                        sys.exit(1)

                    if proceed:
                        size     = self['parent'].getPluginResult('info', 'size') * zipped
                        seqFile = infile

                        compression = self['parent'].getPlugin('compression')
                        compression.run(name="fastq")
                        seqFile = compression.getDecompressedFilePath()
                        size    = os.path.getsize(seqFile)

                        qualRes         = self.getSeqQual(seqFile, size, seqId=self['parent'].infile, adaptamers=adaptamers)

                        for key in qualRes:
                            if key == 'qualsRaw': continue
                            self[key] = qualRes[key]
                        self['data']['qualsRaw'] = qualRes['qualsRaw']

                        if len(qualRes['qualsRaw'       ]) > 0 and qualRes['seqLenSum'] > 0 and qualRes['numSeqs'        ] > 0 and qualRes['sumRealFileSize'] > 0:
                            self['validQual'] = True
                            self.convertQuals()
                            seqLenSum = qualRes['seqLenSum']

                            qProp                       = self.getQPropCumm(Qborder) * 100.0
                            qSumCumm                    = self.getQSumCumm( Qborder)
                            shaShort                    = self['parent'].getFileName()
                            sumShort                    = self['parent'].getFileName()

                            self['COV'                       ] = (  seqLenSum * 1.0 ) / ( genomeSize * 1.0 )
                            self['Q'+str(Qborder)      ] = qProp
                            self['Q'+str(Qborder)+'BP' ] = (   seqLenSum - qSumCumm)
                            self['Q'+str(Qborder)+'COV'] = ((( seqLenSum - qSumCumm) * 1.0 ) / ( genomeSize * 1.0 ))

                            dbdump.saveDump(outFile, self)
                        else:
                            self.printStats("CREATING QUALS FAILED. WRONG NUMBERS")
                            self['validQual'] = False
                            sys.exit(1)
                    else:
                        self.printStats("CREATING QUALS FAILED. DO NOT PROCEED")
                        self['validQual'] = False
                        sys.exit(1)

                else:
                    self.printStats("CREATING QUALS FAILED. DO NOT DO QUAL")
                    self['validQual'] = False
                    dbdump.saveDump(outFile, self)

            else:
                self.printStats("CREATING QUALS FAILED. NOT A VALID EXTENSION")
                self['validQual'] = False
                sys.exit(1)

        except Exception, e:
            print self['parent'].pp()
            traceback.print_stack()
            traceback.print_exc()
            self['validQual'] = False
            msg    = self.getPrintStr()+" CREATING QUALS FAILED. ERROR RAISED :: %s" % (str(e))
            reason = "UNKNOWN"
            exc_type, exc_value, exc_traceback = sys.exc_info()
            raise createQualsError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))

        self.printStats("QUALS CREATED")

    def convertQuals(self):
        """
        converts the quality from illumina to phred for easy comparison
        """
        qualsConv  = [0]*47
        qualsProp  = [0]*47
        quals      = self['data']['qualsRaw']
        sumQuals   = 0

        for orig in quals.keys():
            sumQuals += quals[orig]

        self['formatType'   ] = None
        formatType = self.getFastqType()
        if formatType is not None:
            fmtMin    = fqFormats[formatType][0]
            fmtBorder = fqFormats[formatType][1]
            fmtMax    = fqFormats[formatType][2]
            fmtName   = fqFormats[formatType][3]
            fmtSub    = fqFormats[formatType][4]
            self['formatType'   ] = fmtName

            qKeys     = quals.keys()
            qKeys.sort()

            for orig in qKeys:
                new       = ord(orig) - fmtSub
                pos       = new + 5

                if setup.debug:
                    self.printStats("ORIG %s NUM %d NEW %d X %d" % (orig, ord(orig), new, pos))

                if new < 0:
                    sys.stderr.write("NEW < 0. ERROR")
                    traceback.print_stack()
                    traceback.print_exc()
                    msg    = self.getPrintStr()+" CONVERT QUALS. NEW < 0. ERROR"
                    reason = "WHEN CONVERTING QUALITIES BUT FIRST LETTER %s BECAME A NEGATIVE NUMBER %s" % (str(orig), str(new))
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    raise convertQualsError(genErrorMsg(2, msg, reason, traceback.format_stack(), traceback.format_exception(exc_type, exc_value, exc_traceback)))
                qualsConv[pos] =  quals[orig]
                qualsProp[pos] = (quals[orig] * 1.0) / (sumQuals * 1.0)


        self['data']['qualsConv'    ] = qualsConv
        self['data']['qualsConvProp'] = qualsProp

    def getQ(        self, Q, quals):
        """
        GENERICAL QUERY FUNCTION
        returns, for a given Q in a given database, the corresponding value
        """
        if quals is not None:
            pos = Q + 5
            #print "GETTING Q",Q,"POS",pos
            v   = quals[pos]
            if v is None:
                return 0
            else:
                return v
        else:
            return None

    def getQCumm(    self, Q, quals):
        """
        GENERICAL QUERY FUNCTION
        returns, for a given Q in a given database quals, the cummulative value
        until and including Q
        TODO: CHECK IF IT IS BEHAVING CORRECT. SHOULD NOT INCLUDE Q
        """
        cumm = 0
        for x in range(-5, Q + 1): # sanger starts at -5
            v = self.getQ(x, quals)
            if setup.debug: print "  X %d V %d C %d" % (x, v, cumm)
            if v is None:
                if setup.debug: print "C %d" % cumm
                return None
            else:
                cumm += v
                if setup.debug: print "C %d" % cumm

        return cumm

    def getQProp(    self, Q):
        """
        Q IS IN PHRED - RESULT IS IN PROPORTION TO TOTAL
        returns, for a given Q, what is its individual proportion among the data
        """
        quals = self['data']['qualsRaw']
        return self.getQ(Q, quals)

    def getQPropCumm(self, Q):
        """
        Q IS IN PHRED - RESULT IS IN PROPORTION TO TOTAL
        returns, for a given Q, to which proportion all previous Qs ammounts. Q included.
        """
        quals = self['data']['qualsConvProp']
        qCumm = self.getQCumm(Q, quals) - self.getQ(Q, quals)
        if qCumm is not None:
            return 1 - qCumm
        else:
            return 0

    def getQSum(     self, Q):
        """
        Q IS IN PHRED - RESULT IS IN ACTUAL NUMBER OF APPEARENCES
        returns, for a given Q, the number of time Q have appeared
        """
        quals = self['data']['qualsConv']
        return self.getQ(Q, quals)

    def getQSumCumm( self, Q):
        """
        Q IS IN PHRED - RESULT IS IN ACTUAL NUMBER OF APPEARENCES
        returns, for a a given Q, the cummulative number of times all previous
        qualities have appeared
        """
        quals = self['data']['qualsConv']
        qCumm = self.getQCumm(Q, quals) - self.getQ(Q, quals)
        if qCumm is not None:
            return qCumm
        else:
            return 0

    def getFastqType(self):
        """
        returns, by analyzing the quality values, which version of fastq file
        this dataset belongs
        """
        quals  = self['data']['qualsRaw']
        fqType = None
        if quals is not None:
            if setup.debug: self.printStats("QUALS IN NOT NONE")
            qualsKeys = quals.keys()
            qualsKeys.sort()
            first     = qualsKeys[0]
            firstOrd  = ord(first)
            last      = qualsKeys[-1]
            lastOrd   = ord(last)
            if setup.debug: self.printStats("QUALS FIRST %s (%d) LAST %s (%d)" % (first, firstOrd, last, lastOrd))
            #                   phred    ascii
            #Sanger/Ill 1.8/Sam  0 - 93  33 - 60 (max 126)
            #Solexa/Ill 1.0     -5 - 62  59 - 40 (max 126)
            #Ill 1.3+            0 - 64  64 - 40 (max 126)
            #Ill 1.5+            2 - 64  66 - 40 (max 126)

            #http://en.wikipedia.org/wiki/FASTQ_format
            #    SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS.....................................................
            #    ..........................XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX......................
            #    ...............................IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII......................
            #    .................................JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ......................
            #    LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL....................................................
            #    !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
            #    |                         |    |        |                              |                     |
            #   33                        59   64       73                            104                   126
            #
            #   S - Sanger        Phred+33,  raw reads typically (0, 40)
            #   X - Solexa        Solexa+64, raw reads typically (-5, 40)
            #   I - Illumina 1.3+ Phred+64,  raw reads typically (0, 40)
            #   J - Illumina 1.5+ Phred+64,  raw reads typically (3, 40)
            #      with 0=unused, 1=unused, 2=Read Segment Quality Control Indicator (bold)
            #      (Note: See discussion above).
            #   L - Illumina 1.8+ Phred+33,  raw reads typically (0, 41)

            for fmtNum in range(len(fqFormats)):
                fmtMin = fqFormats[fmtNum][0]

                if   firstOrd == fmtMin:
                    if setup.debug: self.printStats("QUALS FMT MIN %d" % fmtMin)
                    return fmtNum

            for fmtNum in range(len(fqFormats)):
                fmtMin    = fqFormats[fmtNum][0]
                fmtBorder = fqFormats[fmtNum][1]
                fmtMax    = fqFormats[fmtNum][2]
                fmtName   = fqFormats[fmtNum][3]
                if setup.debug: self.printStats("QUALS MIN %02d (%s) MAX %02d (%s) FORMAT NAME %s MIN %02d BORDER %02d MAX %02d" % \
                    (firstOrd, first, lastOrd, last, fmtName, fmtMin, fmtBorder, fmtMax))

                if   firstOrd >= fmtMin and firstOrd < fmtBorder and lastOrd <= fmtMax:
                    if setup.debug: self.printStats("FOUND")
                    return fmtNum


    def getSeqQual(self, seqFile, size, seqId=None, begin=None, end=None, part=None, adaptamers={}):
        """
        given a sequence file name and its size (in bytes), collect the number of
        appearences of each quality char (which can then be converted to prhred
        quality score). Also, count the number of sequences containing N (as
        oposite of containing ACGT).

        It uses the ParseFastQ library to read the fastq file (compressed or not).

        The result is a dict containing the result
        """
        #qualRes   = getSeqQual(seqFile, size, id=self.id)
        #quals     = qualRes['quals'    ]
        #Ns        = qualRes['Ns'       ]
        #seqLenSum = qualRes['seqLenSum']
        #seqCount  = qualRes['seqCount' ]
        quals           = {}
        Ns              = 0
        seqLenSum       = 0
        seqLenSumH      = 0.0
        seqLenCount     = 0
        sumRealFileSize = 0
        numSeqs         = 0
        fastqSplitParts = 3
        flxSum          = 0
        xlrSum          = 0
        part            = int(size/fastqSplitParts)

        flxAdaptor   = ""
        xlrAdaptorF  = ""
        xlrAdaptorR  = ""
        doAdaptamers = False
        if len(adaptamers.keys()) > 0 and setup.DoAdaptamers:
            doAdaptamers = True
            flxAdaptor   = adaptamers['FLX_LINKER'  ]
            xlrAdaptorF  = adaptamers['XLR_LINKER_F']
            xlrAdaptorR  = adaptamers['XLR_LINKER_R']


        if False: #begin is None or end is None or part is None:
            if   begin == -1 and end == -1:
                #no split
                part  = 1
                begin = 0
                end   = size

            elif begin is None and end is None and part is None:
                #outosplit
                #seek=None, until=None
                vals = []

                for x in range(0, fastqSplitParts):
                    b =    x       * part
                    e = (( x + 1 ) * part) - 1
                    if e > size:
                        e = size
                    vals.append({'seek': b, 'until':e})
                vals[-1]['until'] = size
                #getSeqQual
            else: #invalid?
                pass



        else:
            seqId = self.getPrintStr() + " [%12s]"%str(part)

            sizePerc    = size / 100
            lastPerc    = 0

            parser    = ParseFastQ.ParseFastQ(seqFile, seek=begin, until=end)
            StartTime = time.time()

            self.printStats("CREATING")

            for record in parser:
                header, seqStr, header2, qualStr = record
                qualStrLen       = len(qualStr)
                sumRealFileSize += len(header) + len(seqStr) + len(header2) + qualStrLen + 4
                perc             = sumRealFileSize * 1.0 / size * 100.0

                if int(perc) != lastPerc:
                    timerRes = timer(start=StartTime, target=size, curr=sumRealFileSize, )
                    ela      = timerRes['elaStr']
                    etc      = timerRes['etaStr']
                    speed    = timerRes['speed' ]
                    #perc     = timerRes['perc'  ]
                    lastPerc = int(perc)
                    print "%s %12d / %12d [%3d%%] ELA %s ETC %s SPEED %dMB/s" % (seqId, sumRealFileSize, size, perc, ela, etc, speed)

                numSeqs += 1
                #print "%12d %12d %12d %8.4f%%" % (numSeqs, sumRealFileSize, size, perc)
                #seqHeader  = record[0]

                #qualHeader = record[2]
                #qualStr    = r3
                #print "HEADER '"+seqHeader+"'"
                #print "QUAL   '"+qualStr  +"'"
                seqLenSum  += qualStrLen
                seqLenSumH += 1.0 / qualStrLen

                if doAdaptamers:
                    seqPiece     = seqStr
                    seaarchPiece = True
                    if newblerBorder > 0:
                        if len(seqStr) > ((2*newblerBorder)+40):
                            seqPiece = seqStr[newblerBorder:(newblerBorder*-1)]
                        else:
                            seaarchPiece = False

                    if seaarchPiece:
                        if   flxAdaptor  in seqPiece: flxSum += 1
                        elif xlrAdaptorF in seqPiece: xlrSum += 1
                        elif xlrAdaptorR in seqPiece: xlrSum += 1

                for letter in qualStr:
                    try:
                        quals[letter] += 1
                    except KeyError:
                        quals[letter]  = 1

                try:
                    nPos = seqStr.index('N')
                    Ns += 1
                except ValueError:
                    pass


        res =   {
                    'qualsRaw'       : quals,
                    'Ns'             : Ns,
                    'Ns%'            : ((Ns * 1.0) / numSeqs) * 100.0,
                    'seqLenSum'      : seqLenSum,
                    'seqAvgLen'      : seqLenSum * 1.0  / numSeqs,
                    'seqAvgHarmLen'  : (numSeqs  * 1.0) / seqLenSumH,
                    'sumRealFileSize': sumRealFileSize,
                    'numSeqs'        : numSeqs,
                }

        if doAdaptamers:
            res['flxSum'       ] =    flxSum
            res['xlrSum'       ] =    xlrSum
            res['adaptamerSum' ] =    flxSum + xlrSum
            res['flx%'         ] =  ((flxSum           * 1.0) / numSeqs) * 100.0
            res['xlr%'         ] =  ((xlrSum           * 1.0) / numSeqs) * 100.0
            res['adaptamerSum%'] = (((flxSum + xlrSum) * 1.0) / numSeqs) * 100.0
        else:
            res['flxSum'       ] = 0
            res['flx%'         ] = 0.0
            res['xlrSum'       ] = 0
            res['xlr%'         ] = 0.0
            res['adaptamerSum' ] = 0
            res['adaptamerSum%'] = 0.0
        #quals     = qualRes['quals'    ]
        #Ns        = qualRes['Ns'       ]
        #seqLenSum = qualRes['seqLenSum']
        #seqCount  = qualRes['seqCount' ]
        return res
