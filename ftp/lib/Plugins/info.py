import sys,os,time,inspect,traceback
fullPath        = os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, fullPath)
sys.path.insert(0, os.path.abspath(os.path.join(fullPath, '..')))
from shared import *
from tools  import *


# FILE INFO
class pluginInfo(plugin):
    className = "info"
    def __init__(    self, parent):
        self['parent'] = parent
        self['size'  ] = None
        self['mtime' ] = None
        self['ident' ] = None
        #self.printStats('INITIALIZING')

    def valid(       self):
        #self.printStats('CHECKING VALIDITY')
        if self.redo():
            #self.printStats('CHECKING VALIDITY :: REDOING')
            return False

        if self['size'] is not None and self['mtime'] is not None:
            #self.printStats('CHECKING VALIDITY :: VALID')
            return True
        else:
            #self.printStats('CHECKING VALIDITY :: NOT VALID')
            return False

    def run(         self, name=None, force=False):
        """
        generates the file identifier values consisting of file size
        and creation time
        """
        if name is None:
            name = self.className
        fnPath = self['parent'].getFileNamePath()
        self.printStats("PATH :: %s" % fnPath)
        self['size' ] = os.path.getsize(  fnPath )
        self['mtime'] = os.path.getmtime( fnPath )
        self['ident'] = str(self['size']) + str(self['mtime'])
