import glob, os, sys
import optparse
from types import *

__all__ = ["parameters"]

class paramPair():
    def __init__(self):
        self.name    = None
        self.text    = None
        self.dashes  = None
        self.equal   = None
        self.group   = None
        self.type    = None
        self.value   = None
        self.optData = {}
        self.kwargs  = {}

class parameters():
    def __init__(self):
        self.pairs  = []
        self.sriap  = {}
        self.parser = optparse.OptionParser()


    def compile(self, argv=None):
        if argv is not None:
            (options, args) = self.parser.parse_args(argv)
            self.options    = options
            self.args       = args

            #print str(options)
            #print str(options.__dict__)
            for option in options.__dict__:
                for pair in self.pairs:
                    if pair.name == option:
                        pair.value = options.__dict__[pair.name]
                        #print "LOADING OPTION '" + option + "' VALUE '" + str(pair.value) + "'"

            self.makeGroups()
            self.check()
        else:
            sys.stderr.write("NO ARGUMENT GIVEN")
            sys.exit(2)

    def makeGroups(self):
        paramsGroups  = {}
        mandatory     = {}
        params        = self.data
        self.defaults = {}

        for key in params.keys():
            data = params[key]

            if data.has_key('groupKey'):
                self.groupKey = key
                self.groupVal = self.getValue(key)

            if data.has_key('default'):
                #print "ADDING DEFAULTS TO " + key + " VAL " + str(data['default'])
                self.defaults[key] = data['default']

            if data.has_key('groups'):
                groups = data['groups'].split(",")

                manda = False
                if data.has_key('mandatory'):
                    manda = True

                for group in groups:
                    if not paramsGroups.has_key(group) :
                        paramsGroups[group] = []
                    paramsGroups[group].append(key)

                    if manda:
                        if key not in mandatory:
                            mandatory[key] = {}
                        (mandatory[key])[group] = manda
            else:
                if not paramsGroups.has_key('__NONE__') :
                    paramsGroups['__NONE__'] = []
                paramsGroups['__NONE__'].append(key)

                manda = False
                if data.has_key('mandatory'):
                    manda = True

                if manda is True:
                    if key not in mandatory:
                        mandatory[key] = {}
                    (mandatory[key])['__GLOBAL__'] = manda


        if not self.__dict__.has_key('groupKey'):
            print "no group key, falling to none"
            self.groupKey = None
            self.groupVal = "__GLOBAL__"

        #print str(paramsGroups)

        self.paramsGroups = paramsGroups
        self.mandatory    = mandatory

    def check(self):
        paramsGroups = self.paramsGroups
        mandatory    = self.mandatory
        pairs        = self.pairs
        currGroupKey = self.groupKey
        currGroupVal = self.groupVal
        params       = self.data
        #print "CURR GROUP KEY '" + currGroupKey + "' VAL '" + currGroupVal + "'"

        for key in params.keys():
            hasCommand = self.hasParam(key)
            if hasCommand:
                #print "HAS COMMAND " + key
                val = self.getValue(key)
                #print "  VALUE "+ val
                if (self.defaults).has_key(key):
                    dft = self.defaults[key]
                    #print "  DEFAULT " + str(dft)
                    if str(val) == str(dft):
                        #print "    EQUAL. EXCLUDING"
                        self.deleteValue(key)

        if currGroupVal is not None:
            for command in mandatory.keys():
                #print "checking mandatory command " + command
                groups     = mandatory[command]
                hasCommand = self.hasParam(command)

                for group in groups.keys():
                    #print " checking group '" + group + "' MANDATORY ",
                    manda      = groups[group]
                    #print manda, " present ",
                    #print hasCommand

                    if   group == "__GLOBAL__" or currGroupKey is None:
                        #print "    GROUP IS GLOBAL"
                        if hasCommand is False:
                            sys.stderr.write("MANDATORY GLOBAL PARAMETER " + command + " NOT GIVEN")
                            sys.exit(102)
                    elif group == currGroupVal:
                        #print "    GROUP IS THE SAME"
                        if hasCommand is False:
                            sys.stderr.write("MANDATORY PARAMETER " + command + " WHEN " + currGroupKey + " BEING " + currGroupVal + " WAS NOT GIVEN")
                            sys.exit(103)
        else:
            sys.stderr.write("MANDATORY KEY PARAMETER '" + currGroupKey + "' NOT DEFINED\n")
            sys.exit(101)


    def append(self, name, data):
        dashes  = data.get('dashes',  None)
        equal   = data.get('equal',   None)
        group   = data.get('group',   None)
        optType = data.get('optType', None)
        optData = data.get('data',    {})

        text    = ''
        if dashes is not None:
            text    = '-'*dashes
        text   += name

        #print " appending name " + str(name) + " value " + str(value) + " type " + str(type)
        pair         = paramPair()
        pair.name    = name
        pair.text    = text
        pair.dashes  = dashes
        pair.equal   = equal
        pair.group   = group
        pair.type    = optType

        optData['dest'] = name

        if optType is not None:
            if optType in ["bool", 'name', 'value']:
                optData['action'] = 'store_true'
                #print "  NAME " + name + " STORE TRUE"
            else:
                #print "  NAME " + name + " STORE"
                optData['action'] = 'store'
        else:
            #print "  NAME " + name + " STORE"
            optData['action'] = 'store'

        if optData.has_key("choices"):
            optData['action'] = 'store'
            optData['type']   = 'choice'

        pair.optData = optData

        #elif ptype == 'num':
        self.parser.add_option(text, **optData)
        #parser.add_option( '--program',         dest='program',                     help='[string         ] all, count, dump, histo, merge, stats')

        self.pairs.append(pair)
        self.sriap[name] = pair

    def parseList(self, data):
        self.data = data
        for name in data.keys():
            values = data[name]
            self.append(name, values)

    def getCmd(self, forceAll=None, skip=None):
        pairs        = self.pairs
        paramsGroups = self.paramsGroups

        currGroupKey = self.groupKey
        currGroupVal = self.groupVal
        allowedParams = []
        if currGroupKey is not None:
            print "GROUPING BY " + currGroupKey
            allowedParams = paramsGroups[currGroupVal]
        else:
            for x in paramsGroups.values():
                for y in x:
                    if y not in allowedParams:
                        allowedParams.append(y)

        #print "ALLOWED PARAMS",allowedParams
        cmds  = []
        for pair in pairs:
            name  = pair.name
            value = pair.value
            #print "CHECKING PAIR " + name + " VALUE " + str(value)
            if ( value is not None ) and ( currGroupVal is None or forceAll is not None or any(name == s for s in allowedParams )):
                if skip is not None and name in skip:
                    continue
                cmd    = self.getLine(pair)
                #print "  CMD     " + str(cmd)
                cmds.append(cmd)

        #print " CMDS " + str(cmds)
        return " ".join(cmds)


    def getRawData(self):
        return self.data

    def getArgs(self):
        return self.args


    def getLine(self, pair):
        name   = pair.name
        text   = pair.text
        dashes = pair.dashes
        equal  = pair.equal
        group  = pair.group
        ptype  = pair.type
        value  = pair.value

        cmd = ""

        if   ptype is None:
            cmd = text + " " + value
        elif ptype == 'bool':
            cmd = text
        elif ptype == 'text':
            cmd = text + " '" + str(value) + "'"
        elif ptype == 'int':
            cmd = text + " "  + str(value)
        elif ptype == 'float':
            cmd = text + " "  + str(float)
        elif ptype == 'name':
            cmd = name
        elif ptype == 'value':
            cmd = value
        else:
            print "type " + str(ptype) + " is unknown"

        return cmd

    def hasParam(self, qry):
        if self.sriap.has_key(qry):
            pair = self.sriap.get(qry)
            if pair.value is not None:
                return True
            else:
                return False
        else:
            return False

    def getValue(self, qry):
        if self.sriap.has_key(qry):
            pair = self.sriap.get(qry)
            return pair.value
        else:
            return None

    def addValue(self, qry, att, value):
        if self.sriap.has_key(qry):
            pair = self.sriap.get(qry)
            pair.__dict__[att] = value

    def deleteValue(self, qry):
        if self.sriap.has_key(qry):
            pair       = self.sriap.get(qry)
            pair.value = None



def decovolute(value):
    cmds = []

    if type(value) is ListType:
        #print "VALUE " + str(value) + " IS LIST " + str(type(value))
        for el in value:
            if el in StringTypes:
                cmds.append(el)
            else:
                cmd = decovolute(el)
                cmds.extend(cmd)

    elif type(value) in StringTypes:
        #print "VALUE " + str(value) + " IS STRING " + str(type(value))

        files = glob.glob(value)
        if len(files) == 0:
            cmds.append(value)
        else:
            cmds.extend(files)

    elif isinstance(value, io):
        #print "VALUE " + str(value) + " IS IO INSTANCE " + str(type(value))
        cmds.extend(decovolute(value.getFiles()))

    elif type(value) in (FunctionType, InstanceType, MethodType):
        #print "VALUE " + str(value) + " IS FUNCTION " + str(type(value))
        cmd  = value()
        cmds.extend(decovolute(cmd))

    else:
        #print "UNKNOWN TYPE TO DECOVOLUTE " + str(type(value))
        cmds.append(value)

    #print "CMDS DEC " + str(cmds)
    return cmds
