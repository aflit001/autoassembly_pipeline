#!/usr/bin/python
#variables describing the assembly type
DENOVO          = 0
MAPPING         = 1
types           = ['DENOVO', 'MAPPING']

fqFormats = [   #first val  minBorder   max     name                    subtracts
                [33,        59,          74,    "Sanger/Illumina 1.8",  33], #0
                [59,        64,         104,    "Solexa/Illumina 1.0",  64], #1
                [64,        66,         104,    "Illumina 1.3+"      ,  64], #2
                [66,        66,         104,    "Illumina 1.5+"      ,  64], #3
            ]

newblerBorder = 0
ADAPTAMERS = {
    'FLX_LINKER'   : "GTTGGAACCGAAAGGGTTTGAATTCAAACCCTTTCGGTTCCAAC",
    'XLR_LINKER_F' : "TCGTATAACTTCGTATAATGTATGCTATACGAAGTTATTACG",
    'XLR_LINKER_R' : "CGTAATAACTTCGTATAGCATACATTATACGAAGTTATACGA"
}

dbFile      = 'db/filesDB'  # db file prefix to be added .yaml and/or .pickle containing the files information
dumpStruct  = 'db/structDB' # db file prefix to be added .yaml and/or .pickle containing the structure and files information
dumpSetup   = 'db/setupDB'  # db file prefix to be added .yaml and/or .pickle containing the setup of the project

#header of description file
dscHeaders  =   [
                    ['FILE_NAME',      's'],
                    ['PROJECT_NAME',   's'],
                    ['PROJECT_TYPE',   's'],
                    ['SAMPLE_NAME',    's'],
                    ['TECHNOLOGY',     's'],
                    ['LIBRARY_NAME',   's'],
                    ['LIBRARY_TYPE',   's'],
                    ['LIBRARY_SIZE',   's'],
                    ['STATUS_NAME',    's'],
                    ['PAIR_NAME',      's'],
                ]

pv       = ' | pv --buffer-size 16M -q '

programs =  {
                'fastqc'                  : {
                    'exe'                 : 'perl /home/assembly/tomato150/scripts/pipeline/progs/FastQC/fastqc',
                    'threads'             : 4
                },

                'fastqScreen'             :{
                    'exe'                 : 'fastq_screen',
                    'threads'             : 8,
                    'subset'              : 5000
                },

                'solexaqa'                : {
                    'exe'                 : 'perl /home/assembly/tomato150/scripts/pipeline/progs/solexaqa/SolexaQA.pl'
                },

                'quake'                   : {
                    'exe'                 : 'python progs/Quake/bin/quake.py',
                    'tmp'                 : '/home/assembly/tmp'
                    #'tmp'                 : '/mnt/nexenta/assembly/nobackup/tmp'
                },

                'mkplot'                  : {
                    'q'                   : 80,
                    'miY'                 : 3
                },

                'jellyfish'               : {
                    'exe'                 : 'jellyfish',
                    'tmp'                 : '/run/shm',
                    'pv'                  : pv
                },

                'filter454'               : {
                    'exeAnalyze'          : 'python /mnt/nexenta/assembly/nobackup/dev_150/scripts/pipeline/progs/filter454/analyze454Reads.py',
                    'exeFilter'           : 'python /mnt/nexenta/assembly/nobackup/dev_150/scripts/pipeline/progs/filter454/filter454Reads.py',
                    'exeSffFile'          : '/opt/454/2.6_1/bin/sfffile',
                    'exeSffInfo'          : '/opt/454/2.6_1/bin/sffinfo',
                    'exeFq2Fa'            : '/home/assembly/tomato150/scripts/pipeline/progs/fastq_to_fasta',
                    'exeFq2Fa'            : '/home/assembly/bin/fastq_to_fasta',
                    'tmp'                 : '/run/shm',
                },

                'sffExtract'              : {
                    'exeSffFile'          : '/opt/454/2.6_1/bin/sfffile',
                    'exeSffInfo'          : '/opt/454/2.6_1/bin/sffinfo',
                    'exeFastaAndQualMerge': 'python /home/assembly/tomato150/scripts/pipeline/progs/fastqmergefastaandqual.py',
                },

                'fastqCount'              : {
                    #'exe'                 : '/home/assembly/tomato150/scripts/pipeline/progs/fastqCount',
                    'exe'                 : '/home/aflit001/bin/fastqCount',
                    'tmp'                 : '/run/shm'
                },

                'dymTrim'                 : {
                    'exeDynamicTrim'      : 'perl /mnt/nexenta/assembly/nobackup/dev_150/scripts/pipeline/progs/solexaqa/DynamicTrim.pl',
                    'exeLengthSort'       : 'perl /mnt/nexenta/assembly/nobackup/dev_150/scripts/pipeline/progs/solexaqa/LengthSort.pl',
                    'tmp'                 : '/mnt/nexenta/assembly/nobackup/tmp'
                },

                'zip'                     : {
                    'exe'                 : 'pigz'
                }
            }


#how to identify if a file is compressed and commands to uncompress them
pigzd  = [programs['zip']['exe'] + ' -d -p 3 -k -c %(in)s ' + pv + ' > %(out)s']
pigzt  = [programs['zip']['exe'] + ' -t %(in)s'                                               ]
#sffExt = ['sff_extract --fastq --min_left_clip=16 --out_basename=%(out)s %(in)s']
sffExt = [
            programs['sffExtract']['exeSffInfo'          ] + ' -s %(in)s ' + pv + ' > %(out)s.fasta',
            programs['sffExtract']['exeSffInfo'          ] + ' -q %(in)s ' + pv + ' > %(out)s.fasta.qual',
            programs['sffExtract']['exeFastaAndQualMerge'] + ' %(out)s.fasta %(out)s.fasta.qual %(out)s',
            'rm -f %(out)s.fasta %(out)s.fasta.qual'
        ]

extensions = [
                #extension      compression rate    uncompression   compression test    technology name compressed
                ['*.fastq.gz'  , 2.70,              pigzd ,         pigzt,              'illumina',     True ],
                ['*.fq.gz'     , 2.70,              pigzd ,         pigzt,              'illumina',     True ],
                ['*.fq.gz.*.gz', 2.70,              pigzd ,         pigzt,              'illumina',     True ],
                ['*.fastq'     , 1.00,              None,           None ,              'illumina',     False],
                ['*.fq'        , 1.00,              None,           None ,              'illumina',     False],
                ['*.sff'       , 0.33,              sffExt,         None ,              '454'     ,     True ],
            ]
