#!/usr/bin/python
import os,sys
import csv
import time, datetime
import base64
import Image
import run
import StringIO

def genErrorMsg(errNo, msg, reason, tb, ex):
    message = "ERROR #%d. \"%s\"\n\"%s\"\n%s\n%s" % (errNo, msg, reason, "".join(tb), "".join(ex))
    return message

class CommentedFile:
    #http://www.mfasold.net/blog/2010/02/python-recipe-read-csvtsv-textfiles-and-ignore-comment-lines/

    def __init__(self, f, commentstring="#"):
        self.f = f
        self.commentstring = commentstring
    def next(self):
        line = self.f.next()
        while line.startswith(self.commentstring) or not line.strip():
            line = self.f.next()
        return line
    def __iter__(self):
        return self

def limit(list1, list2):
    if list2 is not None:
        return list(set(list1) & set(list2))
    else:
        return list1

class CSVFile:
    def __init__(self, csvFile, commentstring="#", delimiter='\t', headerLine=1):
        self.csv_file      = csv.reader(open(csvFile, "rb"), delimiter=delimiter)
        self.commentstring = commentstring
        self.headerLine    = headerLine
        self.names         = {}
        self.parse()
        #pp(self.__dict__)
        pass

    def parse(self):
        self.numCols = None
        self.grpby   = {}
        self.data    = []
        lineCount    = 0

        for line in self.csv_file:
            lineCount += 1
            #print "LINE",line," ",line[0]

            if self.numCols is None:
                self.numCols = len(line)

            linePos = len(self.data)

            if len(self.names) == 0 and ( self.headerLine != 0 and self.headerLine is not None and lineCount == self.headerLine ) and line[0][0] == self.commentstring:
                #print "  LC",linePos
                line[0] = line[0].lstrip("#")
                self.cols  = line

                for pos in range(len(self.cols)):
                    colName = self.cols[pos]
                    self.names[colName] = pos
                self.title  = True

            elif not line[0][0] == self.commentstring:
                self.data.append(line)

                for colName in self.names.keys():
                    colNum = self.names[colName]
                    colVal = line[colNum]

                    if not self.grpby.has_key(colName):
                        self.grpby[colName]         = {}

                    if not self.grpby[colName].has_key(colVal):
                        self.grpby[colName][colVal] = []

                    self.grpby[colName][colVal].append(linePos)
            else:
                pass

        self.count = len(self.data)

    def getNumLines(self): return self.count
    def getNumCols(self) : return self.numCols

    def getLine(self, pos):
        if pos < self.count:
            return self.data[pos]
        else:
            return None

    def getCol(self, pos, colName):
        if self.names.has_key(colName):
            colNum = self.names[colName]
            return self.getColNum(pos, colNum)
        else:
            print "no column named",colName
            return None

    def getColNum(self, pos, colNum):
        if colNum <= self.numCols:
            line = self.getLine(pos)
            return line[colNum]
        else:
            print "no column number",colNum
            return None

    def getColName(self, colNum):
        if self.numCols <= col:
            self.cols[pos]

    def listCol(self, colName, filter=None):
        if self.grpby.has_key(colName):
            if filter is None:
                return self.grpby[colName].keys()
            else:
                tmpl = []
                for key in self.grpby[colName].keys():
                    val = self.grpby[colName][key]
                    res = limit(val, filter)
                    if len(res) != 0:
                        tmpl.append(key)
                return tmpl
        else:
            return None

    def listAll(self, colName, colValue, filter=None):
        if self.names.has_key(colName):
            if self.grpby[colName].has_key(colValue):
                res = self.grpby[colName][colValue]
                return limit(res, filter)
            else:
                return None
        else:
            return None

    def getAll(self, colName, colValue, filter=None):
        poses = self.listAll(colName, colValue)
        res = []
        for pos in poses:
            res.append(self.getLine(pos))
        return limit(res, filter)

def seconds2str(seconds):
    timeStr   = str(datetime.timedelta(seconds=seconds))
    pointpos  = timeStr.rfind('.')
    return timeStr[:pointpos]

def timer(start=None, target=None, curr=None):
    res = {}
    if start is not None:
        timeDiff      = time.time() - start
        res['ela'   ] = timeDiff
        res['elaStr'] = seconds2str(timeDiff)

        if target is not None and curr is not None and curr <= target and timeDiff > 0 and curr > 0 and target > 0:
            perc          = curr * 1.0 / target * 100.0
            perPerc       = res['ela'] / perc
            eta           = perPerc * ( 100.0 - perc) + 1.0
            res['perc'  ] = perc
            res['eta'   ] = eta
            res['etaStr'] = seconds2str(eta)

            speed         = (curr / 1024.0 / 1024.0) / res['ela']
            res['speed' ] = speed
        else:
            res['perc'  ] = -1
            res['eta'   ] = -1
            res['etaStr'] = "NaN"
            res['speed' ] = -1

    return res

class Tee(object):
    #http://stackoverflow.com/questions/616645/how-do-i-duplicate-sys-stdout-to-a-log-file-in-python
    def __init__(self, name, mode):
        self.file = open(name, mode)
        self.stdout = sys.stdout
        sys.stdout = self
    def __del__(self):
        sys.stdout = self.stdout
        self.file.close()
    def write(self, data):
        self.file.write(data)
        self.stdout.write(data)

def image2string(img):
    if os.path.exists(img):
        #print "IMAGE %s EXISTS" % img
        #imgData = None
        #with open(img, 'rb') as i:
            #imgData = i.read()

        #if imgData is None:
            #print  "IMAGE %s HAS NO DATA" % img
            #return None

        im        = Image.open(img).convert('P')
        imo       = StringIO.StringIO()
        im.save(imo, 'PNG', optimize=True)#, bits=8)
        imageData  = imo.getvalue()
        imo.close()
        imgStr     = base64.b64encode(imageData)

        return imgStr

    else:
        print "IMAGE %s DOES NOT EXISTS" % img
        return None




def correctDB(db):
    dbKeys = db.keys()
    dbKeys.sort()
    for filename in dbKeys:
        fileData = db[filename]
        dataObj  = fileData.__dict__
        #print pprint.pformat(dataObj)
        data     = dataObj['data']
        info     = dataObj['info']
        data2    = {}

        data2['compression'  ]                      = {}
        data2['compression'  ]['compressOK'       ] = dataObj['compressOK'       ] #  "compressOK": true,
        data2['compression'  ]['decompressed'     ] = dataObj['decompressed'     ] #  "decompressed": true,
        data2['compression'  ]['decompressedFile' ] = dataObj['decompressedFile' ] #  "decompressedFile": "001_illumina_pairedend_500_45_1_fq_gz/seq.fastq",

        data2['contamination']                      = {}
        data2['contamination']['contaminationData'] = dataObj['contaminationData'] #  "contaminationData": {
        data2['contamination']['contaminationFile'] = dataObj['contaminationFile'] #  "contaminationFile": "001_illumina_pairedend_500_45_1_fq_gz/seq_screen.txt",
        data2['contamination']['contaminationOK'  ] = dataObj['contaminationOK'  ] #  "contaminationOK": true,

        data2['quals'        ]                      = {}
        data2['quals'        ]['Ns'               ] = dataObj['Ns'               ] #  "Ns": 561585,
        data2['quals'        ]['_validQual'       ] = data[   '_validQual'       ] #    "_validQual": true,
        data2['quals'        ]['numSeqs'          ] = data[   'numSeqs'          ] #    "numSeqs": 181488592,
        data2['quals'        ]['quals'            ] = data[   'qualsRaw'         ] #    "quals": {
        data2['quals'        ]['qualsConv'        ] = data[   'qualsConv'        ] #    "qualsConv": [
        data2['quals'        ]['qualsConvProp'    ] = data[   'qualsConvProp'    ] #    "qualsConvProp": [
        data2['quals'        ]['seqLen'           ] = data[   'seqLen'           ] #    "seqLen": 100,
        #data2['quals'        ]['sumQuals'         ] = data[   'sumQuals'         ] #    "sumQuals": 18148859200,
        data2['quals'        ]['sumRealFileSize'  ] = data[   'sumRealFileSize'  ] #    "sumRealFileSize": 44832974254

        data2['hash'         ]                      = {}
        data2['hash'         ]['_validHash'       ] = data[   '_validHash'       ] #    "_validHash": true,
        data2['hash'         ]['hash'             ] = data[   'hash'             ] #    "hash": "663d18bd95f4130ece9950e9fc12038d3a62142a6d069",

        data2['fastqc'       ]                      = {}
        data2['fastqc'       ]['fastQcFile'       ] = dataObj['fastQcFile'       ] #  "fastQcFile": "001_illumina_pairedend_500_45_1_fq_gz/seq_fastqc.zip",
        data2['fastqc'       ]['fastQcOK'         ] = dataObj['fastQcOK'         ] #  "fastQcOK": true,

        data2['info'         ]                 = {}
        data2['info'         ]['mtime'            ] = info[   'mtime'            ] #    "mtime": 1337837540.0,
        data2['info'         ]['size'             ] = info[   'size'             ] #    "size": 15727037077

        del dataObj['compressOK'       ]
        del dataObj['decompressed'     ]
        del dataObj['decompressedFile' ]
        del dataObj['contaminationData']
        del dataObj['contaminationFile']
        del dataObj['contaminationOK'  ]
        del dataObj['Ns'               ]
        del dataObj['fastQcFile'       ]
        del dataObj['fastQcOK'         ]
        del dataObj['data'             ]
        del dataObj['info'             ]

        dataObj['data'] = data2
        db[filename]    = fileData

        #  "id"               : "001/illumina/pairedend_500/120512_I238_FCC0U42ACXX_L2_SZAXPI008746-45_1.fq.gz",
        #  "infile"           : "001/illumina/pairedend_500/120512_I238_FCC0U42ACXX_L2_SZAXPI008746-45_1.fq.gz",
        #  "projectName"      : "reseq",
        #  "projectStatusName": "CHECKED",
        #  "projectType"      : "MAPPING",
        #  "sampleId"         : "001",
        #  "sampleLibrary"    : "pairedend_500",
        #  "sequenceTech"     : "illumina"
    return db
