#!/usr/bin/python
import os
import sys
import inspect

#currAbsPath     = os.path.dirname(inspect.getfile(inspect.currentframe()))
#currFtpPath     = os.path.abspath(os.path.join(currAbsPath, "ftp"))
#sys.path.append(currFtpPath)

import lib.constants as constants

######################
## PERSONAL VARIABLES
######################
import copy
genomeSize              = 950000000
base                    = "/home/assembly/tomato150" # base of all projects


#folders for RIL sub-project
#www.tomatogenome.net/accessions.html
rilSubs     =   [
                    "601", "603", "608", "609", "610", #05
                    "611", "612", "614", "615", "618", #10
                    "619", "622", "623", "624", "625", #15
                    "626", "630", "631", "634", "639", #20
                    "643", "644", "646", "648", "649", #25
                    "651", "653", "654", "656", "658", #30
                    "659", "660", "665", "666", "667", #35
                    "668", "669", "670", "674", "675", #40
                    "676", "678", "679", "682", "684", #45
                    "685", "688", "691", "692", "693", #50
                    "694", "696", "697", "701", "702", #55
                    "705", "706", "707", "710", "711"  #60
                ]
#folders for the reseq sub-project
reseqSubs  =    [   #Tomato landraces & (old) cultivars
                    "001", "002", "003", "004", "005", #05
                    "006", "007", "008", "011", "012", #10
                    "013", "014", "015", "016", "017", #15
                    "018", "019", "020", "021", "022", #20
                    "023", "024", "026", "027", "028", #25
                    "029", "030", "031", "032", "033", #30
                    "034", "035", "036", "037", "038", #35
                    "039", "040", "041", "077", "078", #40
                    "088", "089", "090", "091", "093", #45
                    "094", "096", "097", "102", "103", #50
                    "105",                             #51

                    #Wild tomato relatives
                    "025", "042", "043", "044", "045", #56
                    "046", "047", "049", "051", "052", #61
                    "053", "054", "055", "056", "057", #66
                    "058", "059", "060", "062", "063", #71
                    "064", "065", "066", "067", "068", #76
                    "069", "070", "071", "072", "073", #81
                    "074", "075", "104"                #84

                    #naturalis
                    "naturalis_Moneymaker-CF4N705",
                    "naturalis_Moneymaker-CF4N706",
                    "naturalis_Moneymaker-oldN703",
                    "naturalis_Moneymaker-oldN704",
                    "naturalis_Slyc-17N701",
                    "naturalis_Slyc-17N702",
                ]


#libraries created to the de novo assembly
denovoLibs  =   [
                    ["illumina",    [
                                        [ "pairedend_170", 'PE',  '170' ],
                                        [ "matepair_2000", 'MP', '2000' ]
                                    ]
                    ],
                    ["454",         [
                                        [ "8000"   , 'MP' ,  '8000' ],
                                        [ "20000"  , 'MP' , '20000' ],
                                        [ "shotgun", 'WGS',    None ],
                                    ]
                    ]
                ]

denovoLibsPenne = copy.deepcopy(denovoLibs)
denovoLibsPenne[1][1].append([ "3000"   , 'MP' ,  '3000' ])

#libraries created to the mapping assembly (ril and reseq)
mappingLibs =   [
                    ["illumina",    [
                                        [ "pairedend_500", 'PE', '500' ]
                                    ]
                    ]
                ]
#####
## END OF PERSONAL VARIABLES
####


#description of the data
folders     =   [
                    #root                          name            type               subs       libs             to be cleaned
                    [os.path.join(base ,"denovo"), "arcanum"     , constants.DENOVO , None     , denovoLibs     , True ],
                    [os.path.join(base ,"denovo"), "habrochaites", constants.DENOVO , None     , denovoLibs     , True ],
                    [os.path.join(base ,"denovo"), "pennellii"   , constants.DENOVO , None     , denovoLibsPenne, True ],
                    [base                        , "ril"         , constants.MAPPING, rilSubs  , mappingLibs    , True ],
                    [base                        , "reseq"       , constants.MAPPING, reseqSubs, mappingLibs    , False],
                ]


statusesToClean  = ['CHECKED']
projectsToIgnore = [ 'reseq']
samplesToIgnore  = []
quakeignore      = [ 'ril', 'reseq' ]
quakecutoff      = {
                        'arcanum':      4,
                        'habrochaites': 4,
                        'pennellii':    4
                    }

setup_forbidden = [ # system libs
                    'os'         , 'sys'        , 'inspect'   , 'copy',
                    # shared variables
                    'currAbsPath', 'currFtpPath', 'constants' , 'setup_forbidden',
                    # temporary variables
                    'rilSubs'    , 'reseqSubs'  , 'denovoLibs', 'denovoLibsPenne',
                    'mappingLibs']
