#!/usr/bin/python
import sys,os
import re
import pprint
import time
import base64
import datetime
import time
import simplejson
import jsonpickle
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, make_response, jsonify, Markup, Response
from contextlib import closing

sys.path.insert(0, 'lib')

import run
import tools
import setup
import dbdump
import constants
import dataplugins
from techs import general, dataset

#TODO: ADD PIPELINE DATA




setupfile = 'uisetup.json'
if not os.path.exists(setupfile):
    print "count not find setup file %s" % setupfile
    sys.exit(1)

for k,v in jsonpickle.decode(open(setupfile, 'r').read())['server'].items():
    #print "SERVER K %s V %s" % (k, v)
    globals()[k] = v

for k,v in jsonpickle.decode(open(setupfile, 'r').read())['datas' ].items():
    #print "DATAS K %s V %s" % (k, v)
    globals()[k] = v

#CONSTANTS
#DEBUG            = True
#PER_PAGE         = 20
#MAX_QUERY_BUFFER = 100
SECRET_KEY       = 'development key'


#VARIABLES
setupDB          = None
structure        = None
projectStatuses  = None
plugins          = None
dbMtime          = None
app              = Flask(__name__)
app.config.from_object(__name__)
app.jinja_env.globals['trim_blocks'       ] = True
app.jinja_env.add_extension('jinja2.ext.do')



def sizeToGb(*args):
    size = args[-1]

    if size is None:
        return "None"

    try:
        float(size)
    except TypeError:
        print "COULD NOT CONVERT TO FLOAT", size
        raise


    if size <= 1024:
        return "%.3f bytes" % size

    size = float(size) / 1024
    if size <= 1024:
        return "%.3f Kb"  % size

    size = float(size) / 1024
    if size <= 1024:
        return "%.3f Mb"  % size

    size = float(size) / 1024
    if size <= 1024:
        return "%.3f Gb"  % size

    size = float(size) / 1024
    return "%.3f Tb"  % size

def sizeTo(*args):
    size = args[-1]

    if size is None:
        return "None"

    try:
        float(size)
    except TypeError:
        print "COULD NOT CONVERT TO FLOAT", size
        raise

    if size <= 1000:
        return "%.3f"  % size

    size = float(size) / 1000
    if size <= 1000:
        return "%.3f K"  % size

    size = float(size) / 1000
    if size <= 1000:
        return "%.3f Mi"  % size

    size = float(size) / 1000
    if size <= 1000:
        return "%.3f Bi"  % size

    size = float(size) / 1000
    return "%.3f Tr"  % size

def perc(*args):
    value = args[-1]

    if value is None:
        return "None"

    try:
        float(value)
    except TypeError:
        print "COULD NOT CONVERT TO FLOAT", value
        raise

    return '%.2f%%' % value

def roundToTwo(*args):
    value = args[-1]

    if value is None:
        return "None"

    try:
        float(value)
    except TypeError:
        print "COULD NOT CONVERT TO FLOAT", value
        raise

    fmt = "%.2f"
    return fmt % value

def convTime(*args):
    value = args[-1]

    if value is None:
        return "None"

    try:
        float(value)
    except TypeError:
        #print "COULD NOT CONVERT TYPE TO FLOAT", value
        return ""
    except ValueError:
        #print "COULD NOT CONVERT VALUE TO FLOAT", value
        return ""

    return time.ctime(value)





class summing(object):
    def __init__(self):
        self.val = 0

    def add(self, val):
        #print "summing",val
        self.val += val

    def get(self):
        #print "summing returning",self.val
        return self.val

class average(object):
    def __init__(self):
        self.val   = 0
        self.count = 0

    def add(self, val):
        self.val   += val
        self.count += 1

    def get(self):
        if self.count == 0:
            return -1
        if self.val == 0:
            return 0

        return float(self.val) / self.count

class returnE(object):
    def add(self, val):
        pass

    def get(self):
        return {}







def graph_line(pluginData, func_name, func_nfo):
    #"#Base\t%GC",
    #"1\t40.209569984588065",
    #"2\t39.756647339140365",

    #//categoriesX = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10-14', '15-19', '20-24', '25-29', '30-34', '35-39', '40-44', '45-49'];
    #//data        = [{
    #//                name: 'Tokyo',
    #//                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
    #//            }, {
    #//                name: 'New York',
    #//                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
    #//            }, {
    #//                name: 'Berlin',
    #//                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
    #//            }, {
    #//                name: 'London',
    #//                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
    #//            }];
    #//
    #//title    = "Relative Enrichment Over Read Length";
    #//subTitle = "FastQC Report";
    #//titleY   = "%";
    #//unityY   = "%";
    #//titleX   = "Position in Read (bp)"
    #//unityX   = "bp";
    #function graph_lines(data, container, title, subTitle, titleY, unityY, titleX, unityX, categoriesX) {

    lists       = []
    categoriesX = []
    dataD       = {}

    title, subTitle, titleY, unityY, titleX, unityX, minY, maxY, minX, maxX = func_nfo

    print "graph_line ",
    print func_nfo

    for line in pluginData:
        #print line
        cols = line.split('\t')
        if cols[0][0] == "#":
            lists = []
            #titleY = cols[0][1:]
            #titleX = cols[1]
            for colName in cols:
                lists.append(colName)
        else:
            categoriesX.append(str(cols[0]))
            for colIndex in range(len(cols)):
                if colIndex == 0:
                    continue

                try:
                    colVal  = float(cols[colIndex])
                except ValueError:
                    colVal  =       cols[colIndex]

                colName = lists[colIndex]
                if colName in dataD:
                    dataD[colName].append(colVal)
                else:
                    dataD[colName] = [ colVal ]

    data = []
    for colName, colData in dataD.items():
        data.append({
            'name': colName,
            'data': colData
            })

    res = {
        'categoriesX' : categoriesX,
        'data'        : data,
        'title'       : title,
        'subTitle'    : subTitle,
        'titleY'      : titleY,
        'unityY'      : unityY,
        'titleX'      : titleX,
        'unityX'      : unityX,
        'minY'        : minY,
        'maxY'        : maxY,
        'minX'        : minX,
        'maxX'        : maxX,
        '_dst_func'   : 'graph_lines',
    }

    #print res

    return jsonpickle.encode(res)

def candle_stick(pluginData, func_name, func_nfo):
    #"Per base sequence quality": {
    # "data": [
    #    0     1     2       3               4               5                6
    #  "#Base\tMean\tMedian\tLower Quartile\tUpper Quartile\t10th Percentile\t90th Percentile",
    #  "1\t36.064507832356185\t37.0\t37.0\t40.0\t28.0\t40.0",
    #  "2\t35.4651882904503\t37.0\t35.0\t40.0\t25.0\t40.0",
    #  "3\t35.13829905728201\t37.0\t34.0\t40.0\t24.0\t40.0",
    #  "4\t34.98659335540772\t37.0\t34.0\t40.0\t24.0\t40.0",
    #  "5\t34.9416264163553\t37.0\t34.0\t40.0\t23.0\t40.0",

    #//[1131926400000,61.54,61.98,60.91,61.45],
    #//[1132012800000,61.60,63.08,61.46,62.28],
    #//[1132099200000,63.15,65.06,63.09,64.95],
    #//[1347321600000,665.27,670.10,656.55,660.59],
    #                 open high     low close

    title, subTitle, titleY, unityY, titleX, unityX, minY, maxY, minX, maxX = func_nfo

    print func_nfo

    data1 = []
    data2 = []
    data3 = []
    data4 = []
    for line in pluginData:
        #print line
        cols = line.split('\t')
        if cols[0][0] == "#":
            continue
        else:
            #print "'%s'" % line
            pos     = cols[0]
            dashPos = pos.find('-')

            if dashPos != -1:
                pos = float( int( pos[:dashPos] ) + int(pos[dashPos+1:]) ) / 2.0
                #print "%d + %d = %d" % (int( pos[:dashPos] ), int(pos[dashPos+1:]), posf)
                #pos = posf
            pos = int(pos)

            data1.append([ pos, float(cols[3]), float(cols[4]) ])
            data2.append([ pos, float(cols[5]), float(cols[6]) ])
            data3.append([ pos, float(cols[1])                 ])
            data4.append([ pos, float(cols[2])                 ])



    res = {
        #'categoriesX' : categoriesX,
        'data1'       : data1,
        'data2'       : data2,
        'data3'       : data3,
        'data4'       : data4,
        'title'       : title,
        'subTitle'    : subTitle,
        'titleY'      : titleY,
        'unityY'      : unityY,
        'titleX'      : titleX,
        'unityX'      : unityX,
        'minY'        : minY,
        'maxY'        : maxY,
        'minX'        : minX,
        'maxX'        : maxX,
        '_dst_func'   : 'graph_candle',
    }

    return jsonpickle.encode(res)

def stacked(pluginData, func_name, func_nfo):
    #var categoriesX = ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas'];
    #var data        = [{
    #        name: 'John',
    #        data: [5, 3, 4, 7, 2]
    #    }, {
    #        name: 'Jane',
    #        data: [2, 2, 3, 2, 1]
    #    }, {
    #        name: 'Joe',
    #        data: [3, 4, 4, 2, 5]
    #    }];

    #"contaminationData": {
    # "EcoliM": [
    #  99.66,
    #  0.0,
    #  0.12,
    #  0.08,
    #  0.14
    # ],


    lists       = []
    categoriesX = []
    categoriesY = []
    data        = []

    title, subTitle, titleY, unityY, titleX, unityX, minY, maxY, minX, maxX = func_nfo

    print func_nfo

    for spp_name in sorted(pluginData):
        values = pluginData[spp_name]
        if spp_name[0] == "_":
            categoriesY = values

    if len(categoriesY) == 0:
        categoriesY = ['%Unmapped', '%One hit one library', '%Multiple hits one library', '%One hit multiple libraries', '%Multiple hits multiple libraries']

    dataD = {}
    for spp_name in sorted(pluginData):
        values = pluginData[spp_name]
        categoriesX.append(spp_name)

        for pos in range(len(values)):
            y_label = categoriesY[pos]
            if y_label not in dataD:
                dataD[y_label] = []
            dataD[y_label].append(values[pos])
            #if spp_name[0] != "_":
            #    data.append({
            #        'name': spp_name,
            #        'data': values
            #        })
            #else:
            #    categoriesX = values

    for y_label in dataD:
        data.append({
            'name': y_label,
            'data': dataD[y_label]
            })

    res = {
        'categoriesX' : categoriesX,
        'data'        : data,
        'title'       : title,
        'subTitle'    : subTitle,
        'titleY'      : titleY,
        'unityY'      : unityY,
        'titleX'      : titleX,
        'unityX'      : unityX,
        'minY'        : minY,
        'maxY'        : maxY,
        'minX'        : minX,
        'maxX'        : maxX,
        '_dst_func'   : 'graph_stacked',
    }

    return jsonpickle.encode(res)







def parseGraph(projectName, projectStatus, projectSample, sequenceTech, libraryName, fileName, pluginName, pluginKey, pluginValue):
    res = []

    lnkConstLst = []
    for kv in ( [ 'projectName',   Markup.escape( projectName   ) ],
                [ 'projectStatus', Markup.escape( projectStatus ) ],
                [ 'projectSample', Markup.escape( projectSample ) ],
                [ 'sequenceTech',  Markup.escape( sequenceTech  ) ],
                [ 'libraryName',   Markup.escape( libraryName   ) ],
                [ 'fileName',      Markup.escape( fileName      ) ],
                [ 'pluginName',    Markup.escape( pluginName    ) ],
                [ 'pluginKey',     Markup.escape( pluginKey     ) ],
                ):
        lnkConstLst.append("=".join(kv))
    lnkConst = "&".join(lnkConstLst)

    url_for_download = url_for('download')
    if isinstance(pluginValue, dict):
        res.append("<table>")
        res.append("    <tr>")

        if (pluginName, pluginKey, None) in graph_mapper:
            lnk = "%s?%s&srctype=json" % (url_for_download, lnkConst)
            res.append("""<a href="%(lnk)s" class="graphjson" title="%(title)s" target="_blank">%(dsc)s</a></td>""" %\
                        { 'lnk': lnk, 'title': pluginKey, 'dsc': '<i class="icon-picture"/>' })

        else:
            for imageName in pluginValue:
                dsc = Markup.escape(imageName)
                if 'ico' in pluginValue[imageName]:
                    ico_name    = 'pluginsIcons/' + pluginName + '/' + pluginValue[imageName]['ico'].replace(" ", "_").lower() + '.png'
                    url_for_ico = url_for('static', filename=ico_name)
                    dsc         = '<img src="%(url)s" class="icon">' % {'url': url_for_ico}


                if (pluginName, pluginKey, imageName) in graph_mapper and \
                'data' in pluginValue[imageName] and \
                pluginValue[imageName]['data'] is not None:
                    lnk = "%s?%s&imageName=%s&srctype=json" % (url_for_download, lnkConst, imageName)
                    res.append("""      <td><a href="%(lnk)s" class="graphjson" title="%(title)s" target="_blank">%(dsc)s</a></td>""" %\
                                   { 'lnk': lnk, 'title': imageName, 'dsc': dsc })

                else:
                    if 'img' in pluginValue[imageName] and pluginValue[imageName]['img'] is not None:
                        lnk = "%s?%s&imageName=%s&srctype=img" % (url_for_download, lnkConst, imageName)
                        #res.append("""<td><img src="%s" class="graphSmall" title="%s"/></td>""" % (lnk, imageName))
                        res.append("""      <td><a href="%(lnk)s" class="graphimg" title="%(title)s" target="_blank">%(dsc)s</a></td>""" % \
                                   { 'lnk': lnk, 'title': imageName, 'dsc': dsc })

                    else:
                        res.append("""      <td title="%(title)s">%(dsc)s</td>""" % { 'title': imageName, 'dsc': dsc })

        res.append("    </tr>")
        res.append("</table>")

    else:
        lnk = "%s?%s" % (url_for_download, lnkConst)
        #res.append("""<td><img src="%s" class="graphSmall" title="%s"/></td>""" % (lnk, pluginKey))
        #res.append("""<td><a href="%s" class="graph" title="%s" target="_blank">%s</a></td>""" % (lnk, pluginKey, pluginKey))
        res.append("""<a href="%(lnk)s" class="graphimg" title="%(title)s" target="_blank">%(dsc)s</a>""" % \
                   { 'lnk': lnk, 'title': pluginKey, 'dsc': '<i class="icon-picture"/>'})

    return "\n".join(res)


class stat(object):
    def __init__(self, translator):
        self.data       = {}
        self.translator = translator

    def add(self, **kwargs):
        convert = None
        if 'plugin' in kwargs:
            plugin = kwargs['plugin']

            if 'key' in kwargs:
                key     = kwargs['key'   ]
                convert = self.translator[plugin][key][2]
                if convert is not None:
                    convert = convert

            else:
                return None
        else:
            return None


        if 'name' in kwargs:
            name = kwargs['name']

            if plugin not in self.data:
                self.data[plugin] = {}

            if key not in self.data[plugin]:
                self.data[plugin][key] = {}

            if name not in self.data[plugin][key]:
                try   : conv = convert()
                except: conv = None
                self.data[plugin][key][name] = { 'stat': conv, 'status': {} }

            if 'status' in kwargs:
                status = kwargs['status']
                if status not in self.data[plugin][key][name]['status']:
                    try   : conv = convert()
                    except: conv = None
                    self.data[plugin][key][name]['status'][status] = { 'stat': conv, 'samples': {} }

                if 'sample' in kwargs:
                    sample = kwargs['sample']
                    if sample not in self.data[plugin][key][name]['status'][status]['samples']:
                        try   : conv = convert()
                        except: conv = None
                        self.data[plugin][key][name]['status'][status]['samples'][sample] = { 'stat': conv, 'techs': {} }

                    if 'tech' in kwargs:
                        tech = kwargs['tech']
                        if tech not in self.data[plugin][key][name]['status'][status]['samples'][sample]['techs']:
                            try   : conv = convert()
                            except: conv = None
                            self.data[plugin][key][name]['status'][status]['samples'][sample]['techs'][tech] = { 'stat': conv, 'libs': {} }

                        if 'lib' in kwargs:
                            lib = kwargs['lib']
                            if lib not in self.data[plugin][key][name]['status'][status]['samples'][sample]['techs'][tech]['libs']:
                                try   : conv = convert()
                                except: conv = None
                                self.data[plugin][key][name]['status'][status]['samples'][sample]['techs'][tech]['libs'][lib] = { 'stat': conv, 'plugins': {} }

                            if 'value' in kwargs:
                                value = kwargs['value']
                                try   : self.data[plugin][key][name]['status'][status]['samples'][sample]['techs'][tech]['libs'][lib]['stat'].add(value)
                                except: pass

                                try   : self.data[plugin][key][name]['status'][status]['samples'][sample]['techs'][tech]['stat'].add(value)
                                except: pass

                                try   : self.data[plugin][key][name]['status'][status]['samples'][sample]['stat'].add(value)
                                except: pass

                                try   : self.data[plugin][key][name]['status'][status]['stat'].add(value)
                                except: pass

                                try   : self.data[plugin][key][name]['stat'].add(value)
                                except: pass

                            else: # lib
                                try   : res = self.data[plugin][key][name]['status'][status]['samples'][sample]['techs'][tech]['libs'][lib]['stat'].get()
                                except: res = None
                                return res
                        else: # tech
                            try   : res = self.data[plugin][key][name]['status'][status]['samples'][sample]['techs'][tech]['stat'].get()
                            except: res = None
                            return res
                    else: # sample
                        try   : res = self.data[plugin][key][name]['status'][status]['samples'][sample]['stat'].get()
                        except: res = None
                        return res
                else: # status
                    try   : res = self.data[plugin][key][name]['status'][status]['stat'].get()
                    except: res = None
                    return res
            else: # name
                try   : res = self.data[plugin][key][name]['stat'].get()
                except: res = None
                return res
        else:
            return None





#APPLICATION CODE :: SETUP
@app.before_request
def before_request():
    global enterprise
    g.enterprise = enterprise
    g.setupDB, g.structure, g.projectStatuses, g.plugins = getDb()


@app.after_request
def after_request(response):
    return response


@app.teardown_request
def teardown_request(exception):
    pass


#APPLICATION CODE :: ROUTE
@app.route('/', methods=['GET'])
def initial():
    resTemp = render_template('display_normal.html')
    return resTemp


@app.route('/query', methods=['GET'])
def query():
    projectName = request.args.get('projectName', '')
    resTemp = render_template('response.html', projectName=projectName)
    return resTemp


@app.route('/download', methods=['GET'])
def download():
    projectName   = request.args.get('projectName'  , None)
    projectStatus = request.args.get('projectStatus', None)
    projectSample = request.args.get('projectSample', None)
    sequenceTech  = request.args.get('sequenceTech' , None)
    libraryName   = request.args.get('libraryName'  , None)
    fileName      = request.args.get('fileName'     , None)
    pluginName    = request.args.get('pluginName'   , None)
    pluginKey     = request.args.get('pluginKey'    , None)
    imageName     = request.args.get('imageName'    , None)
    srctype       = request.args.get('srctype'      , None)

    for var in [projectName, projectStatus, projectSample, sequenceTech, libraryName, fileName, pluginName, pluginKey]:
        if var is None:
            print "none var"
            abort(404)

    try:
        data    = g.structure[projectName][projectStatus][projectSample][sequenceTech][libraryName][fileName]
    except:
        print "no data"
        abort(404)

    #print request.headers

    plugins = data.getPlugins()
    if pluginName in plugins:
        pluginData = data.getPlugin(pluginName)
        if pluginKey in pluginData:
            pluginImages = pluginData[pluginKey]
            pluginImage  = None
            pluginData   = None

            if isinstance(pluginImages, dict):
                if imageName is not None:
                    if imageName in pluginImages:
                        if srctype is None or srctype == 'img':
                            if 'img' in pluginImages[imageName]:
                                print "valid image key %s" % imageName
                                pluginImage = pluginImages[imageName]['img']
                            else:
                                print "no such image key %s" % imageName
                                abort(404)

                        elif srctype is not None and srctype == 'json':
                            if 'data' in pluginImages[imageName]:
                                print "valid image key %s" % imageName
                                pluginData = pluginImages[imageName]['data']
                            else:
                                print "no such image key %s" % imageName
                                abort(404)

                        else:
                            print "no such data key %s to src type %s" % (imageName, srctype)
                            abort(404)

                    else:
                        print "no such image %s" % imageName
                        abort(404)

                elif srctype is not None and srctype == 'json':
                    print 'dict w/ image name'
                    pluginData = pluginImages

                else:
                    print "no such data key %s" % imageName
                    abort(404)

            else:
                pluginImage = pluginImages
                print "not list"


            if pluginImage is None and pluginData is None:
                print "plugin image is none"
                abort(404)

            etag = "".join([str(x) for x in [dbMtime, projectName, projectStatus, projectSample, sequenceTech, libraryName, fileName, pluginName, pluginKey, imageName]])
            if pluginImage is not None:
                try:
                    pluginImageData = base64.b64decode(pluginImage)
                except:
                    print "plugin image error converting"
                    abort(404)

                #response = make_response()
                #reponse.headers['Cache-Control'] = 'no-cache'
                #reponse.headers['Content-Type'] = 'image/png'
                #reponse.headers['Content-Length'] = len(pluginImageData)

                #print "LAST MODIFIED", dbMtime
                #print "ETAG", etag.replace("/", "").replace("\\", "").replace("_", "").replace(".", "").replace("-", "").replace(" ", "").replace(":", "")
                #print pluginImageData
                return Response(pluginImageData,
                                mimetype="image/png",
                                headers={
                                    "Cache-Control" : "public, max-age=36000",
                                    "Content-Length": len(pluginImageData),
                                    "ETag"          : etag,
                                    "Last-Modified" : dbMtime
                                })

            elif pluginData is not None:
                qry = (pluginName, pluginKey, imageName)
                if qry in graph_mapper:
                    func, func_name, func_nfo = graph_mapper[ qry ]
                    print "plugin",func_nfo
                    func_res = func(pluginData, func_name, func_nfo)
                    return Response(func_res,
                                mimetype="application/json",
                                headers={
                                    "Cache-Control" : "public, max-age=36000",
                                    "Content-Length": len(func_res),
                                    "ETag"          : etag,
                                    "Last-Modified" : dbMtime
                                })

                else:
                    print "no data for combination"
                    abort(404)




    abort(404)


@app.context_processor
def utility_processor():
    converter = {
        'info': {
            '_plugin'           : 'Info',
            'size'              : ['File Size'         , sizeToGb   , summing ],
            'mtime'             : ['C. Time'           , convTime   , None    ]
        },
        'quals': {
            '_plugin'           : 'Quality',
            'seqLenSum'         : [ 'Total Size'       , sizeTo     , summing ],
            'Q30BP'             : [ 'Bp (Q>=30)'       , sizeTo     , summing ],
            'COV'               : [ 'Coverage'         , sizeTo     , summing ],
            'adaptamerSum%'     : [ '% w/ Adaptamer'   , perc       , average ],
            'seqAvgLen'         : [ 'Average Length'   , sizeTo     , average ],
            'flx%'              : [ '% FLX'            , perc       , average ],
            'Ns%'               : [ '% Ns'             , perc       , average ],
            'xlr%'              : [ '% XLR'            , perc       , average ],
            'sumRealFileSize'   : [ 'Decomp. File Size', sizeToGb   , summing ],
            'Q30COV'            : [ 'Cov. (Q>=30)'     , roundToTwo , summing ],
            'Q30'               : [ '% Q>=30'          , roundToTwo , average ],
            'numSeqs'           : [ '# Seq.'           , sizeTo     , summing ],
            'formatType'        : [ 'Format'           , None       , None    ]
        },
        'fastqc': {
            '_plugin'           : 'FastQC',
            'fastqcGraphs'      : ['FastQC Graphs'     , parseGraph , returnE ],
        },
        'contamination': {
            '_plugin'           : 'Contamination',
            #'contaminationGraph': ['Graph'             , parseGraph , None    ]
            'contaminationData' : ['Graph'             , parseGraph , None    ]
        }
    }

    statistics = stat(converter)

    def cleanName(filename):
        #print "FILE NAME",filename
        filename = os.path.basename(filename).replace('.gz', '').replace('.fastq', '').replace('.fq', '').replace('.sff', '').replace('.dup.clean', '').replace('.cleaned', '').replace('.clean', '')
        return filename

    def checkPlugin(pluginName):
        return pluginName not in ['compression', 'hash']

    def checkPluginKey(pluginName, pluginKey):
        skipKeys =  {
                        'info': [
                                'ident',
                                'parent'
                        ],
                        'quals': [
                                'data',
                                'xlrSum',
                                'parent',
                                'flrSum',
                                'seqAvgHarmLen',
                                'validQual',
                                'adaptamerSum',
                                'flxSum',
                                'Ns'
                        ],
                        'fastqc': [
                                #'fastqcGraphs', # to convert
                                'fastQcOK',
                                'fastQcFile',
                                'parent'
                                'kmer profile',
                        ],
                        'contamination': [
                                'contaminationGraph',
                                'contaminationFile',
                                #'contaminationData',
                                'contaminationOK',
                                'parent'
                        ]
                    }

        if pluginName in skipKeys:
            if pluginKey in skipKeys[pluginName]:
                return False

        return True

    def converterPlugin(pluginName):
        if pluginName in converter:
            if '_plugin' in converter[pluginName]:
                return converter[pluginName]['_plugin']

        return pluginName

    def converterKey(pluginName, key):
        if pluginName in converter:
            if key in converter[pluginName]:
                conv = converter[pluginName][key][0]
                if conv is not None:
                    return conv

        return key

    def converterValue(projectName, projectStatus, projectSample, sequenceTech, libraryName, fileName, pluginName, pluginKey, pluginValue):
        if pluginValue is None:
            return ""

        if pluginValue == "":
            return ""

        if pluginName in converter:
            if pluginKey in converter[pluginName]:
                conv = converter[pluginName][pluginKey][1]
                if conv is not None:
                    res = conv(projectName, projectStatus, projectSample, sequenceTech, libraryName, fileName, pluginName, pluginKey, pluginValue)
                    if res is None:
                        return ""
                    return res

        return pluginValue

    def parseSetup(data):
        return syntaxHighlight(data)

    def syntaxHighlight(data):
        def json2html(match):
            #print "MATCH",match
            K = match.group(1)
            K = K.strip()
            #print " K '" + K + "'",

            cls = 'jsonnumber'

            if K[0] == '"':
                if K[-1] == ":":
                    cls = 'jsonkey'
                else:
                    cls = 'jsonstring'

            elif K in ('true', 'false'):
                cls = 'jsonboolean'

            elif K == 'null':
                cls = 'jsonnull'

            #print "  CLS",cls
            return '<span class="%s">%s</span>' % (cls, K)

        json    = jsonpickle.encode(data)
        json.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;');
        rep     = r'("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|(?:\b|\s)(true|false|null)(?:\b|\s|,)|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)'
        repc    = re.compile(rep)
        jsonRes = repc.sub(json2html, json)
        #print json
        #print jsonRes
        return jsonRes

    def length(var):
        return len(var)

    def stats(**kwargs):
        s = statistics.add(**kwargs)
        if s is None:
            return ""

        return s

    return dict(cleanName       = cleanName,
                checkPlugin     = checkPlugin,
                checkPluginKey  = checkPluginKey,
                converterPlugin = converterPlugin,
                converterKey    = converterKey,
                converterValue  = converterValue,
                parseSetup      = parseSetup,
                length          = length,
                stats           = stats
                )


#DATABASE
def init_db():
    with app.app_context():
        print "initializing db"

        if not os.path.exists(setupDBFile):
            print "NO SETUP DATABASE (DB) FILE %s" % setupDBFile
            sys.exit(1)

        if not os.path.exists(structureFile):
            print "NO STRUCTURE FILE %s" % structureFile
            sys.exit(1)

        global setupDB
        global structure
        global dbMtime
        dbMtime = os.stat(structureFile).st_mtime
        dbMtime = time.ctime(dbMtime)

        jsonpickle.set_preferred_backend('simplejson')
        jsonpickle.set_encoder_options('simplejson', sort_keys=True, indent=1)

        setupDB   = jsonpickle.decode(open(setupDBFile,   'r').read())
        structure = jsonpickle.decode(open(structureFile, 'r').read())


        if setupDB is None:
            print "no setupDB data"
            sys.exit(1)

        if structure is None:
            print "no structure data"
            sys.exit(1)



        global projectStatuses
        global plugins
        projectStatuses = []
        plugins         = {}
        stop            = False

        for projectName in structure:
            projStatuses = structure[projectName]
            projectBase  = os.path.join(setup.getProjectRoot(projectName), projectName)
            if stop: break
            for projectStatus in projStatuses:
                if projectStatus not in projectStatuses: projectStatuses.append(projectStatus)
                samples     = structure[projectName][projectStatus]
                if stop: break
                for projectSample in samples:
                     if projectSample != None:
                        technologies     = samples[projectSample]
                        if stop: break
                        for sequenceTech in technologies:
                            if sequenceTech != None:
                                libs     = technologies[sequenceTech]
                                if stop: break
                                for libraryName in libs:
                                    if libraryName != None:
                                        fileNames     = libs[libraryName]
                                        if stop: break
                                        for fileName in fileNames:
                                            if fileName != None:
                                                data     = fileNames[fileName]
                                                pairName = data.pairName
                                                for pluginName in data.getPlugins():
                                                    if pluginName not in plugins:
                                                        plugins[pluginName] = []

                                                    if pluginName not in []:
                                                        pluginData = data.getPlugin(pluginName)
                                                        for pluginKey in pluginData:

                                                            if pluginKey == 'parent':
                                                                continue

                                                            if pluginKey not in plugins[pluginName]:
                                                                plugins[pluginName].append(pluginKey)

                                                            #('fastqc', 'fastqcGraphs'), ('contamination', 'contaminationGraph')
                                                            if (pluginName, pluginKey) not in [('info', 'ident'), ('info', 'parent'), ('quals', 'data') ]:
                                                                val = data.getPluginResult(pluginName, pluginKey)

                                                                #if pluginKey == 'fastqcGraphs':
                                                                #    for key in val:
                                                                #        plugins[pluginName].append(key)
                                                stop = True
                                                break

        projectStatuses.sort()

        print "db loaded"


def getDb():
    if setupDB is None:
        init_db()

    return [setupDB, structure, projectStatuses, plugins]




graph_mapper = {
    #                                                                         parser func , graph parser js,   title,                                                 subTitle,      titleY,          , unityY, titleX               , unityX, miny, maxy, minx, maxx
    ('fastqc'       , 'fastqcGraphs'      , 'Sequence Duplication Levels' ): [graph_line  , 'graph_lines' , [ 'Sequence Duplication Levels >=38.4%'                , "FastQC"      , "Perc (%)"       , ""    , "Duplication Level"  , "x"   ,    0,  100, None, None ] ],
    ('fastqc'       , 'fastqcGraphs'      , 'Per base sequence content'   ): [graph_line  , 'graph_lines' , [ 'Sequence Content Across all Bases'                  , "FastQC"      , "Perc (%)"       , ""    , "Position in Read"   , "bp"  ,    0,  100, None, None ] ],
    ('fastqc'       , 'fastqcGraphs'      , 'Per sequence GC content'     ): [graph_line  , 'graph_lines' , [ 'GC Distribution Over all Sequences'                 , "FastQC"      , "Num Sequences"  , ""    , "Mean GC Content (%)", ""    ,    0, None, None, None ] ],
    ('fastqc'       , 'fastqcGraphs'      , 'Sequence Length Distribution'): [graph_line  , 'graph_lines' , [ 'Distribution of sequence Lengths Over all Sequences', "FastQC"      , "Num Sequences"  , ""    , "Sequence Length"    , "bp"  ,    0, None, None, None ] ],
    ('fastqc'       , 'fastqcGraphs'      , 'Per base GC content'         ): [graph_line  , 'graph_lines' , [ 'GC Content Across all Bases'                        , "FastQC"      , "Perc (%)"       , ""    , "Position in Read"   , "bp"  ,    0,  100, None, None ] ],
    ('fastqc'       , 'fastqcGraphs'      , 'Per sequence quality scores' ): [graph_line  , 'graph_lines' , [ 'Quality Score Distribution Over all Sequences'      , "FastQC"      , "Num Sequences"  , ""    , "Position in Read"   , "bp"  ,    0, None, None, None ] ],
    ('fastqc'       , 'fastqcGraphs'      , 'Per base N content'          ): [graph_line  , 'graph_lines' , [ 'N Content Across all Bases'                         , "FastQC"      , "Perc (%)"       , ""    , "Position in Read"   , "bp"  ,    0,  100, None, None ] ],
    ('fastqc'       , 'fastqcGraphs'      , 'Per base sequence quality'   ): [candle_stick, 'candle_stick', [ 'Quality Scores Across all Bases'                    , "FastQC"      , "Quality (Q)"    , ""    , "Position in Read"   , "bp"  ,    0,   42, None, None ] ],
    ('contamination', 'contaminationData' , None                          ): [stacked     , 'stacked'     , [ 'Contamination'                                      , "FastQ Screen", "Perc Mapped (%)", ""    , "Species Db"         , ""    , None, None, None, None ] ],
    #('fastqc'       , 'fastqcGraphs'      , 'Kmer Content'                ): [graph_line  , 'graph_lines' ],
    #over represented sequences
    #('contamination', 'contaminationGraph', None                          ): [stacked     , 'stacked'     , ['contaminationGraph'                                 , "FastQ Screen", "Perc"           , "%", "Number", "#"] ],
}

if __name__ == '__main__':
    app.run(port=PORT)
