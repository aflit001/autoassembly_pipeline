function resizeImage(){
    //console.log($("#graphDiv").html());
    var $divGraph = $("#graphDiv");

    resetDivGraph();

    $divGraph.show();
    var $imgGraph = $('#divImg');

    if ( $imgGraph.size() == 0 ) {
        //console.log("empty");
        return;
    }

    var offset    = $divGraph.offset();
    var x         = window.mouseXPos - offset.left;
    var y         = window.mouseYPos - offset.top;

    //console.log($imgGraph)
    var ix        = $imgGraph.get(0).width;
    var iy        = $imgGraph.get(0).height;
    var ip        = ix / iy;

    if (( ix == 0) || (iy == 0 )) {
        //resizeImage();
        return;
    }

    $divGraph.width( ix);
    $divGraph.height(iy);

    //console.log("B X " + x + " Y " + y + " IX " + ix + " IY " + iy + " IP " + ip);

    if (( x < ( ix * 1.2 )) && ( y < iy * 1.2)){
        //console.log("fixing");
        var nx =  x * .8;
        var ny = nx * ip;
        $imgGraph.get(0).width  = nx;
        $imgGraph.get(0).height = ny;
        $divGraph.width( nx);
        $divGraph.height(ny);

        //var ix = $imgGraph.get(0).width;
        //var iy = $imgGraph.get(0).height;

       //console.log("A X " + x + " Y " + y + " IX " + ix + " IY " + iy + " IP " + ip);
    }
};


var closeImgDiv = ' <div class="pull-right">\
        <i class="icon-remove" id="hideimg"></i>\
    </div>\
    ';

function resetDivGraph(){
    var $divGraph = $("#graphDiv");

    $divGraph.css('width' , window.max_win_width);
    $divGraph.css('height', window.max_win_height);
    $divGraph.width( window.max_win_width);
    $divGraph.height(window.max_win_height);
}

function graph_caller(dataHash){
    //console.log('calling graph');
    resetDivGraph();

    var dst                = dataHash['_dst_func'];
    //console.log('calling graph :: dst: ' + dst);

    var fn                 = window[dst];
    //console.log('calling graph :: fn: ' + fn);

    dataHash['container'] = 'graphDiv';

    //console.log('calling graph :: container: ' + $('divImg'));

    fn(dataHash);

    $("#graphDiv").append(closeImgDiv);
}


$(document).ready(function(){
    var window_height  = $(window).height();
    window.max_win_height = window_height * 0.85;
    window.max_win_width  = window_height * 1.30;

    window.max_win_height = Math.ceil( window_height         *  0.60 );
    window.max_win_width  = Math.ceil( window.max_win_height * (16/9));


    resetDivGraph();

    console.log("window " + window_height + " max height " + max_win_height + " max width " + max_win_width);

    // store mouse position
    $(document).mousemove(function(e){
        window.mouseXPos = e.pageX;
        window.mouseYPos = e.pageY;
        //console.log("X " + window.mouseXPos + " Y " + window.mouseYPos);
     });

    // HIDE/SHOW COLUMNS
    $(document).on('click', ".icon-chevron-right", function(){
        var tgt = $(this).attr("tgt");
        $("." + tgt).each(function(data){
            $(this).show();
        });
        //$(this).attr("class", "icon-chevron-left");
        $(this).toggleClass("icon-chevron-left");
        $(this).toggleClass("icon-chevron-right");
    });

    $(document).on('click', ".icon-chevron-left", function(){
        var tgt = $(this).attr("tgt");
        $("." + tgt).each(function(data){
            $(this).hide();
        });
        $(this).toggleClass("icon-chevron-left");
        $(this).toggleClass("icon-chevron-right");
    });


    //SHOW STATUS
    $(document).on('click', "button[class=statusbtn]", function(){
        var tgt = $(this).attr("tgt");

        $("div[class=statusDiv]").each(function(data){
            $(this).hide();
        });

        $("#" + tgt).each(function(data){
            $(this).show();
        });
    });


    //SHOW IMAGES
    $(document).on('mouseenter', "a[class=graphimg]", function(e){
        $("#graphDiv").html('');
        var href = $(this).attr("href");
        $("#graphDiv").html('<img src="'+href+'" id="divImg" onload="resizeImage()"/>');
        $("#graphDiv").append(closeImgDiv);
        //$("#graphDiv").show();
    });

    //$(document).on('mouseleave', "a[class=graphimg]", function(){
        //$("#graphDiv").html('');
        //$("#graphDiv").hide();
    //});


    //SHOW JSON IMAGES
    $(document).on('mouseenter', "a[class=graphjson]", function(e){
        $("#graphDiv").html('');
        var href = $(this).attr("href");
        //console.log('getting '+href);
        $.getJSON(href, graph_caller);

        //$("#graphDiv").html('<img src="'+href+'" id="divImg" onload="resizeImage()"/>');
        $("#graphDiv").show();
    });

    //$(document).on('mouseleave', "a[class=graphjson]", function(){
    //    $("#graphDiv").html('');
    //    $("#graphDiv").hide();
    //});


    $(document).on('click', "#hideimg", function(){
        $("#graphDiv").html('');
        $("#graphDiv").hide();
    });


    //ROW COLORING
    $(document).on('mouseenter', ".dataline", function(){
        var id = $(this).attr("id");
        $(this).toggleClass("colored");
    });

    $(document).on('mouseleave', ".dataline", function(){
        var id = $(this).attr("id");
        $(this).toggleClass("colored");
    });


    //COLUMN COLORING
    $(document).on('mouseenter', ".dataCol", function(){
        var col = $(this).attr("col");
        $("[col=" + col+"]").each(function(data){
            $(this).toggleClass("colored");
        });
    });

    $(document).on('mouseleave', ".dataCol", function(){
        var col = $(this).attr("col");
        $("[col=" + col+"]").each(function(data){
            $(this).toggleClass("colored");
        });
    });


    // HIDE IMAGE DIV
    $('#graphDiv').hide();
});
