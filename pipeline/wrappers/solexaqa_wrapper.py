#!/usr/bin/env python
"""
./SolexaQA.pl input_files [-p|probcutoff 0.05] [-h|phredcutoff 13] [-v|variance] [-m|minmax] [-s|sample 10000] [-b|bwa] [-d|directory path] [-sanger -solexa -illumina]

-p|probcutoff   probability value (between 0 and 1) at which base-calling error is considered too high (default; p = 0.05) *or*
-h|phredcutoff  Phred score (between 0 and 40) at which base-calling error is considered too high
-v|variance     calculate variance statistics
-m|minmax       calculate minimum and maximum error probabilities for each read position of each tile
-s|sample       number of sequences to be sampled per tile for statistics estimates (default; s = 10000)
-b|bwa          use BWA trimming algorithm
-d|directory    path to directory where output files are saved
-sanger         Sanger format (bypasses automatic format detection)
-solexa         Solexa format (bypasses automatic format detection)
-illumina       Illumina format (bypasses automatic format detection)

#doqcdosolexaqa
#IN="$fastq"
#OUT="$OUTQCDS/$fastqName$fastqExt.png"
#CMD="$SOLEXAQA -s $SOLEXAQASAMPLESIZE $JELLYIN -o $OUTQCDS";
#QUALP="$OUTQCDS/$fastqName$fastqExt.quality.pdf"
#QUALI="$OUTQCDS/$fastqName$fastqExt.quality.pdf.png"
#
#
#if [[ -f "$QUALP" ]]; then
#    echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "CONVERTING '$QUALP' TO '$QUALI'"
#    convert  $QUALP $QUALI
#fi
#
#
#
#HISP="$OUTQCDS/$fastqName$fastqExt.segments.hist.pdf"
#HISI="$OUTQCDS/$fastqName$fastqExt.segments.hist.pdf.png"
#
#
#if [[ -f "$HISP" ]]; then
#    echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "CONVERTING '$HISP' TO '$HISI'"
#    convert  $HISP $HISI
#fi


sample.fastq.hist.pdf
sample.fastq.matrix
sample.fastq.png
sample.fastq.quality
sample.fastq.quality.pdf
sample.fastq.segments


"""

import os, shutil, subprocess, sys, tempfile, glob
import traceback
if __name__=="__main__":
    import os
    fullpath=os.getcwd()

    # add parent folder to path

    #print "CURRENT PATH " + fullpath
    fullpath=os.path.abspath(fullpath)
    #print "PREVIOUS PATH " + fullpath
    sys.path.insert(0, fullpath)
    sys.path.insert(0, os.path.abspath(os.path.join(fullpath, '..')))
    #print "PATH " + str(sys.path)

import tools.parameters
import tools.run
import lib.setup as setup


setupExe = setup.constants.programs['solexaqa']['exe']

def stop_err( msg, errNo=1 ):
    sys.stderr.write( '%s\n' % msg )
    sys.exit(errNo)

def loadParams():
    parameter = tools.parameters.parameters()

    #-p|probcutoff   probability value (between 0 and 1) at which base-calling error is considered too high (default; p = 0.05) *or*
    #-h|phredcutoff  Phred score (between 0 and 40) at which base-calling error is considered too high
    #-v|variance     calculate variance statistics
    #-m|minmax       calculate minimum and maximum error probabilities for each read position of each tile
    #-s|sample       number of sequences to be sampled per tile for statistics estimates (default; s = 10000)
    #-b|bwa          use BWA trimming algorithm
    #-d|directory    path to directory where output files are saved
    #-sanger         Sanger format (bypasses automatic format detection)
    #-solexa         Solexa format (bypasses automatic format detection)
    #-illumina       Illumina format (bypasses automatic format detection)

    params = {
        'bwa'            : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] use BWA trimming algorithm (false)' }},
        'illumina'       : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] Illumina format (bypasses automatic format detection) (false)' }},
        'minmax'         : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] calculate minimum and maximum error probabilities for each read position of each tile [increases run time by 25%] (false)' }},
        'sanger'         : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] Sanger format (bypasses automatic format detection)   (false)'  }},
        'solexa'         : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] Solexa format (bypasses automatic format detection)   (false)'  }},
        'variance'       : { 'optType':'bool',                 'dashes': 2, 'default': False,       'data': { 'help':'[Bool             ] calculate variance statistics (false)' }},

        'directory'      : {                                   'dashes': 2,                         'data': { 'help':'[output folder    ] path to directory where output files are saved (default; d = /tmp)'       }},
        'sample'         : {                    'type':'int',  'dashes': 2, 'default': 10000,       'data': { 'help':'[uint32           ] number of sequences to be sampled per tile for statistics estimates (default; s = 10000)'  }},
        'phredcutoff'    : {                    'type':'int',  'dashes': 2, 'default': 13,          'data': { 'help':'[uint32           ] Phred score (between 0 and 40) at which base-calling error is considered too high (default; Q = 13)'  }},
        'probcutoff'     : {                    'type':'float','dashes': 2, 'default': 0.05,        'data': { 'help':'[float            ] probability value (between 0 and 1) at which base-calling error is considered too high (default; p = 0.05)'  }},
    }


    parameter.parseList(params)
    parameter.compile(sys.argv[1:])
    #parameter.parser.print_usage()
    #parameter.parser.print_help()
    #print sys.argv
    #print parameter.parser.values

    validateParams(parameter)

    return parameter


def validateParams(parameter):
    args      = parameter.getArgs()

    if len(args) == 0:
        stop_err("NO INPUT FILES DEFINED", 4)


    if parameter.hasParam('phredcutoff') and parameter.hasParam('probcutoff'):
        stop_err('phredcutoff AND probcutoff ARE MUTUALLY EXCLUSIVE', 5)



    if parameter.hasParam('phredcutoff'):
        phredcutoff = parameter.getValue('phredcutoff')
        if phredcutorr < 0:
            stop_err('phredcutoff HAS TO BE BETWEEN 0 AND 40', 6)
        if phredcutorr > 40:
            stop_err('phredcutoff HAS TO BE BETWEEN 0 AND 40', 7)



    if parameter.hasParam('probcutoff'):
        probcutoff = parameter.getValue('probcutoff')
        if probcutoff < 0:
            stop_err('probcutoff HAS TO BE BETWEEN 0 AND 1', 8)
        if probcutoff > 1:
            stop_err('probcutoff HAS TO BE BETWEEN 0 AND 1', 9)



    sumFormat = 0
    if parameter.hasParam('illumina'): sumFormat += 1
    if parameter.hasParam('sanger'  ): sumFormat += 1
    if parameter.hasParam('solexa'  ): sumFormat += 1

    if sumFormat > 1:
        stop_err('ONLY ONE FORMAT CAN BE SET', 10)



    if parameter.hasParam('sample'):
        sample = parameter.getValue('sample')
        if sample < 1000:
            stop_err('SAMPLE SIZE HAS TO BE BIGGER THAN 1000', 11)

    if parameter.hasParam('directory'):
        directory = parameter.getValue('directory')



    if parameter.hasParam('directory'):
        directory = parameter.getValue('directory')
        if not os.path.exists(directory):
            #stop_err('OUTPUT DIR '+directory+' DOES NOT EXISTS', 12)
            try:
                os.makedirs(directory)
            except:
                stop_err('ERROR CREATING DIRECTORY '+directory, 12)

        elif not os.path.isdir(directory):
            stop_err('OUTPUT DIR '+directory+' IS NOT A DIRECTORY', 13)
    else:
        parameter.addValue('directory', 'value', '/tmp')



    for arg in args:
        files = glob.glob(arg)
        if len(files) == 0:
            stop_err("no globable input file given in '" + arg + "'", 14)

        for file in files:
            if ( not os.path.exists(file) ):
                stop_err("file '" + arg + "' does not exists", 15)
                outfiles = glob.glob(os.path.join(parameter.getValue('directory'), os.path.basename(file)) + '*')
                if len(outfiles) > 0:
                    for outfile in outfiles:
                        try:
                            os.remove(outfile)
                        except:
                            stop_err("ERROR ERASING PRE-EXISTING FILE'" + outfile + "' FROM THE INPUT PATTERN '"+file+"'", 16)


    outdir   = parameter.getValue('directory')
    for file in parameter.getArgs():
        dir,ext  = os.path.splitext( file )
        name     = os.path.basename( file )
        dir      = os.path.dirname(  dir  )

        outs = [os.path.join(outdir, name + x) for x in ['.matrix', '.png', '.quality', '.segments', '.hist.pdf', '.hist.pdf.png', '.quality.pdf', '.quality.pdf.png']]
        for rmfile in outs:
            print "REMOVING",rmfile
            if os.path.exists(rmfile): os.remove(rmfile)









def localRun(name, id, cmd, errNo):
    try:
        res = tools.run.runString(name + " " + id, cmd)
        if res != 0:
            return 100+res
        return res
    except Exception, e:
        sys.stderr.write(name + " " + id + " :: Exception (Job__launch_out): " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: FAILED TO RUN "                + cmd + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: TRACEBACK :: "                 + " ".join(traceback.format_stack()) + "\n")
        return 200+errNo



def __main__():
    #Parse Command Line
    parameter = loadParams()
    cmd       = parameter.getCmd()

    #perl $HOME/bin/FastQC/fastqc -t 16 $@
    solqa  = setupExe   + '  ' + cmd + ' ' + " ".join(parameter.getArgs())
    name   = 'SOLEXAWA' + " " + solqa
    id     = os.path.commonprefix([os.path.basename(x) for x in parameter.getArgs()])

    if len(id) < 3:
        id = "".join(parameter.getArgs()[0][:10])

    id = id.rstrip('_')
    print "CMD",solqa,"NAME",name,"ID",id

    #doqcdosolexaqa
    #IN="$fastq"
    #OUT="$OUTQCDS/$fastqName$fastqExt.png"
    #CMD="$SOLEXAQA -s $SOLEXAQASAMPLESIZE $JELLYIN -o $OUTQCDS";
    #QUALP="$OUTQCDS/$fastqName$fastqExt.quality.pdf"
    #QUALI="$OUTQCDS/$fastqName$fastqExt.quality.pdf.png"
    #
    #
    #if [[ -f "$QUALP" ]]; then
    #    echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "CONVERTING '$QUALP' TO '$QUALI'"
    #    convert  $QUALP $QUALI
    #fi
    #
    #
    #
    #HISP="$OUTQCDS/$fastqName$fastqExt.segments.hist.pdf"
    #HISI="$OUTQCDS/$fastqName$fastqExt.segments.hist.pdf.png"
    #
    #
    #if [[ -f "$HISP" ]]; then
    #    echoMsg "$SCRIPT_NAME" "$LINENO" "$TITLE" "CONVERTING '$HISP' TO '$HISI'"
    #    convert  $HISP $HISI
    #fi


    #sample.fastq.hist.pdf
    #sample.fastq.matrix
    #sample.fastq.png
    #sample.fastq.quality
    #sample.fastq.quality.pdf
    #sample.fastq.segments


    res = localRun(name, id, solqa, 17)
    if res != 0:
        stop_err(name + " " + id + " :: ERROR RUNNING. ERROR " + str(res) + " CMD " + cmd, res)

    outdir   = parameter.getValue('directory')

    for file in parameter.getArgs():
        dir,ext  = os.path.splitext( file )
        name     = os.path.basename( file )
        dir      = os.path.dirname(  dir  )

        outs = [os.path.join(outdir, name + x) for x in ['.matrix', '.png', '.quality', '.segments', '.hist.pdf', '.quality.pdf']]
        for ifile in outs:
            if not os.path.exists(ifile):
                for rmfile in outs:
                    if os.path.exists(rmfile): os.remove(rmfile)
                stop_err(name + " " + id + " :: ERROR CREATING OUTPUT FILE " + ifile,  18)

            elif os.path.getsize(ifile) == 0:
                for rmfile in outs:
                    if os.path.exists(rmfile): os.remove(rmfile)
                stop_err(name + " " + id + " :: ERROR CREATING OUTPUT FILE " + ifile + " HAS SIZE 0",  19)

            if ifile.endswith('.pdf'):
                ofile = ifile + '.png'
                res2 = localRun('Convert '+os.path.basename(ifile)+' 2 PNG', os.path.basename(ifile)+' 2PNG', 'convert '+ifile+' '+ofile, 20)

                if not os.path.exists(ofile):
                    for rmfile in outs:
                        if os.path.exists(rmfile): os.remove(rmfile)
                    stop_err(name + " " + id + " :: ERROR CREATING OUTPUT FILE " + ofile,  21)
                elif os.path.getsize(ofile) == 0:
                    for rmfile in outs:
                        if os.path.exists(rmfile): os.remove(rmfile)
                    stop_err(name + " " + id + " :: ERROR CREATING OUTPUT FILE " + ofile + " HAS SIZE 0",  22)







if __name__=="__main__": __main__()
