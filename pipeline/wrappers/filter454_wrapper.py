#/usr/bin/python
import os, shutil, subprocess, sys, tempfile, glob
import traceback
import atexit


if __name__=="__main__":
    import os
    fullpath=os.getcwd()

    # add parent folder to path

    #print "CURRENT PATH " + fullpath
    fullpath=os.path.abspath(fullpath)
    #print "PREVIOUS PATH " + fullpath
    sys.path.insert(0, fullpath)
    sys.path.insert(0, os.path.abspath(os.path.join(fullpath, '..')))
    #print "PATH " + str(sys.path)

import lib.parameters as parameters
import lib.run        as run
import lib.setup      as setup

setupExeAnalyze = setup.constants.programs['filter454']['exeAnalyze']
setupExeFilter  = setup.constants.programs['filter454']['exeFilter' ]
setupExeSffFile = setup.constants.programs['filter454']['exeSffFile']
setupExeSffInfo = setup.constants.programs['filter454']['exeSffInfo']
setupExeFq2Fa   = setup.constants.programs['filter454']['exeFq2Fa'  ]
setupTmp        = setup.constants.programs['filter454']['tmp'       ]


progAnalyze = setupExeAnalyze
progFilter  = setupExeFilter

def localRun(name, id, cmd, errNo):
    try:
        print 'running', name + " " + id, cmd
        res = run.runString(name + " " + id, cmd)
        if res != 0:
            return 100+res
        return res
        #return 0
    except Exception, e:
        sys.stderr.write(name + " " + id + " :: Exception (Job__launch_out): " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: FAILED TO RUN "                + cmd + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: TRACEBACK :: "                 + " ".join(traceback.format_stack()) + "\n")
        return 200+errNo

def cleanTmp(tmp_index_dir):
    if os.path.exists( tmp_index_dir ):
        shutil.rmtree( tmp_index_dir )
        pass

def makeTmp(output):
    tmp_index_dir = tempfile.mkdtemp(prefix='filter454_', dir=setupTmp)
    #tmp_index_dir = '/run/shm/filter454_tmp'
    if not os.path.exists(tmp_index_dir):
        os.makedirs(tmp_index_dir)

    outputTmp     = output
    outputTmp     = os.path.basename(outputTmp)
    outputTmp     = os.path.join(tmp_index_dir, outputTmp)
    return (tmp_index_dir, outputTmp)


def main():
    print sys.argv

    if len(sys.argv) < 12:
        print "not enought params given"
        print '%s \
<int: compress homopolymer size> \
<int: seed length> \
<01b: trim 5 prime> \
<int: min compressed size> \
<int: max compressed size> \
<int: max Ns> \
<01b: filter duplicates> \
<path: dst folder, dst single file (default ./res.sff), - to here multiple files, and + to source path> \
<file: out good ids file> \
<file: out bad ids file> \
[1 dir, fasta files, fastq files, sff files]' % sys.argv[0]
        print 'e.g.: ', sys.argv[0], ' 1 50 1 50 900 1 1 res.sff good_f.ids bad_f.ids *.sff'
        sys.exit(1)

    try:
        params = {
            #analyze
            'compressHomopolymerSize': int(  sys.argv[ 1]),
            'seedLength'             : int(  sys.argv[ 2]),
            'trim5'                  : float(sys.argv[ 3]),

            #filter
            'minCompressedSize'      : int(  sys.argv[ 4]),
            'maxCompressedSize'      : int(  sys.argv[ 5]), # why?
            'maxNs'                  : int(  sys.argv[ 6]),
            'filterDuplicates'       : int(  sys.argv[ 7]),
            'dstFolder'              :       sys.argv[ 8],
            'goodIds'                :       sys.argv[ 9],
            'badIds'                 :       sys.argv[10],
        }

        inputFiles = sys.argv[11:]

    except ValueError:
        print 'one of the parameters couuld not be converted to number'
        sys.exit(1)

    except IndexError:
        print 'no imput file given'
        sys.exit(2)

    print params

    (tmp_index_dir, outputTmp) = makeTmp('none')

    atexit.register(cleanTmp, tmp_index_dir)

    dstFolder = os.path.join(os.curdir, 'res.sff')
    if   params['dstFolder'] == '-':
        dstFolder = os.curdir
    elif params['dstFolder'] == '+':
        dstFolder = None
    else:
        dstFolder = params['dstFolder']


    if not (dstFolder is None or dstFolder.endswith('.sff')):
        print 'dst folder %s. several files' % dstFolder

        if not os.path.exists(dstFolder):
            os.makedirs(dstFolder)
        else:
            if not os.path.isdir(dstFolder):
                print 'out dir %s is not a folder' % dstFolder
                sys.exit(10)

    elif dstFolder.endswith('.sff'):
        dstFolder = os.path.abspath(dstFolder)
        print 'dst folder %s. single files' % dstFolder

        if os.path.exists(dstFolder):
            os.remove(dstFolder)

        if not os.path.exists(os.path.dirname(dstFolder)):
            os.makedirs(os.path.dirname(dstFolder))

    os.chdir(tmp_index_dir)

    dataDir = os.path.join(tmp_index_dir, 'dataTmp')
    outpDir = os.path.join(tmp_index_dir, 'outpTmp')

    if not os.path.exists(dataDir):
        os.makedirs(dataDir)

    if not os.path.exists(outpDir):
        os.makedirs(outpDir)

    filesToCheck          = []
    filesToMerge          = []
    filesToFilter         = []
    finalTmpFastaFile     = None

    if len(inputFiles) == 1:
        inputFile = inputFiles[0]
        if os.path.isdir(inputFile):
            for fileName in os.listdir(inputFile):
                filesToCheck.append(fileName)
            #TODO: parse dir, replacing inputFiles by the files inside, converting if necessary

        elif os.path.isfile(inputFile):
            finalTmpFastaFile = fileName
            #TODO: only pass the file

    if len(inputFiles) > 1:
        for fileName in inputFiles:
            if os.path.isfile(fileName):
                filesToCheck.append(fileName)
            else:
                print 'not able to mix path and file. %s' % fileName
                sys.exit(3)


    for inputFile in filesToCheck:
        inputFileLong = os.path.abspath(inputFile)
        inputFileLong = inputFileLong.replace('\\', '_').replace('/', '_').replace('__', '_').replace('__', '_')

        if inputFile.endswith('.sff'):
            fastaName = os.path.join(dataDir, inputFileLong + '.fasta')

            if not os.path.exists(fastaName):
                cmdConv = '%s -notrim -seq %s > %s.tmp' % (setupExeSffInfo, inputFile, fastaName) #convert to fasta

                res = localRun('filter454 :: convert', inputFileLong, cmdConv, 4)

                if res != 0:
                    print "RESULT NOT ZERO"
                    cleanTmp(tmp_index_dir)
                    sys.exit(res)

                shutil.move(fastaName + '.tmp', fastaName)
            filesToMerge.append(fastaName)
            filesToFilter.append(inputFile)
            #TODO: CONVERT TO FASTA AND MERGE

        elif inputFile.endswith('.fastq'):
            fastaName = os.path.join(dataDir, inputFileLong + '.fasta')
            if not os.path.exists(fastaName):
                cmdConv = '%s -n -i %s -o %s.tmp' % (setupExeFq2Fa, inputFile, fastaName) #convert to fasta

                res = localRun('filter454 :: convert', inputFileLong, cmdConv, 5)

                if res != 0:
                    print "RESULT NOT ZERO"
                    cleanTmp(tmp_index_dir)
                    sys.exit(res)

                shutil.move(fastaName + '.tmp', fastaName)
            filesToMerge.append(fastaName)
            #TODO: CONVERT TO FASTA AND MERGE

        elif inputFile.endswith('.fasta') or inputFile.endswith('.fa'):
            filesToMerge.append(inputFile)
            #TODO: LINK AND MERGE



    print "merging fasta"
    if finalTmpFastaFile is None:
        finalTmpFastaFile   = os.path.join(tmp_index_dir, 'tmp.fasta')
        finalTmpFastaFileFh = open(finalTmpFastaFile, 'w')

        for fileToMerge in filesToMerge:
            shutil.copyfileobj(open(fileToMerge, 'r'), finalTmpFastaFileFh)
            finalTmpFastaFileFh.write('\n')

        finalTmpFastaFileFh.close()
    print "merged fasta"

    params['finalTmpFastaFile'] = finalTmpFastaFile
    params['tmpList'          ] = os.path.join(tmp_index_dir, 'tmp.out' )
    params['goodIdsTmp'       ] = os.path.join(tmp_index_dir, 'good.ids')
    params['badIdsTmp'        ] = os.path.join(tmp_index_dir, 'bad.ids' )

    cmd1        = progAnalyze + ' %(finalTmpFastaFile)s %(compressHomopolymerSize)d %(seedLength)d %(trim5).f > %(tmpList)s'                           % params
    cmd2        = progFilter  + ' %(tmpList)s %(goodIdsTmp)s %(badIdsTmp)s %(minCompressedSize)d %(maxCompressedSize)d %(maxNs)d %(filterDuplicates)d' % params



    if not os.path.exists(params['tmpList']):
        res1 = localRun('filter454 :: analyze', 'all', cmd1, 6)

        if res1 != 0:
            print "RESULT NOT ZERO"
            cleanTmp(tmp_index_dir)
            sys.exit(res1)




    if not (os.path.exists(params['goodIdsTmp']) and os.path.exists(params['badIdsTmp'])):
        res2 = localRun('filter454 :: filter', 'all', cmd2, 7)

        if res2 != 0:
            print "RESULT NOT ZERO"
            cleanTmp(tmp_index_dir)
            sys.exit(res2)




    if dstFolder is None or not dstFolder.endswith('.sff'):
        for fileToFilter in filesToFilter:
            fileToFilterLong = os.path.abspath(fileToFilter)
            fileToFilterLong = fileToFilterLong.replace('\\', '_').replace('/', '_').replace('__', '_').replace('__', '_')

            outTmp = os.path.join(tmp_index_dir, outpDir, fileToFilterLong)
            if not os.path.exists(fileToFilterLong):
                cmd3 = '%s -e %s -o %s %s' % ( setupExeSffFile, params['badIdsTmp'], outTmp, fileToFilter )

                res3 = localRun('filter454 :: exporting', fileToFilter, cmd3, 8)

                if res3 != 0:
                    print "RESULT NOT ZERO"
                    cleanTmp(tmp_index_dir)
                    sys.exit(res3)


            fileToFilterOut = fileToFilter + '.filtered.sff'
            if dstFolder is not None:
                fileToFilterOut = os.path.join(dstFolder, os.path.basename(fileToFilter) + '.filtered.sff')

            if os.path.exists(fileToFilterOut):
                os.remove(fileToFilterOut)

            shutil.copy(outTmp, fileToFilterOut)

    elif dstFolder.endswith('.sff'):
        files  = " ".join(filesToFilter)
        outTmp = os.path.join(tmp_index_dir, outpDir, 'res.sff.tmp')

        if not os.path.exists(outTmp):
            cmd3 = '%s -c 220 -e %s -o %s %s' % (setupExeSffFile, params['badIdsTmp'], outTmp, files )

            res3 = localRun('filter454 :: exporting', outTmp, cmd3, 8)

            if res3 != 0:
                print "RESULT NOT ZERO"
                cleanTmp(tmp_index_dir)
                sys.exit(res3)

        if os.path.exists(dstFolder):
            os.remove(dstFolder)

        print 'coying from %s to %s' % (outTmp, dstFolder)
        shutil.copy(outTmp, dstFolder)


    shutil.copy(params['goodIdsTmp'], params['goodIds'])
    shutil.copy(params['badIdsTmp' ], params['badIds' ])






    #- The first argument (required) should be the path to the fasta file that
    #  contains the 454 reads (or alternatively, a directory containing ONLY
    #  fasta files).

    #- The second argument (optional) controls the homopolymer compression.
    #  It is recommended to leave this value at 1.

    #- The third argument (optional) controls the seed length used in clustering.
    #  Depending on the clonality of your data set, changing this may speed up
    #  or slow down the analysis. I can't give any clear guidelines on this, but
    #  0 (default) seems to be reasonably fast.

    #- The fourth (optional) argument controls the end trimming length. With this
    #  parameter it is possible to ignore the last N % or N bases of each read
    #  during the substring identity search. Emperically, a good value is 0.1 (this
    #  will ignore the last 10% of each sequence). Leaving this off may result in
    #  slightly more duplicated sequences being retained in the next step.

    #- Remember to pipe the output to an output file!


    #- The first argument should be the output file produced by analyze454Reads.py
    #- The second argument controls the name of the 'good' reads identifier file.
    #- The third argument controls the name of the 'bad' reads identifier file.
    #- The fourth argument controls the minimum compressed read length, all reads
    #  shorter than this will end up in the 'bad' file.
    #- The fifth argument controls the maximum compressed read length, all reads
    #  longer than this will end up in the 'bad' file.
    #- The sixth argument controls the maximum number of N's allowed per read, any
    #  read with more Ns than this number will end up in the 'bad' file.
    #- The seventh argument toggles the duplicate filtering, when switched on all
    #  duplicate reads will end up in the 'bad' file.



    #/path/to/python analyze454Reads.py 20kb_inserts.fasta 1 50 0.1 > 20kb_table.out
    #
    #This will first compress all the homopolymers in the sequence to 1. Next, it
    #will perform substring searches on the first 90% of each sequence to identify
    #duplicates. The results are reported in a human-readable table 20kb_table.out .
    #
    #After this has finished we can perform the actual filtering:
    #/path/to/python filter454Reads.py 20kb_table.out good.ids bad.ids 50 450 1 1
    #
    #This will evaluate the table produced in the previous step and divide all reads
    #into two categories: 'good' and 'bad'. The 'bad' reads file will contain the
    #identifiers of all reads that are shorter than 50 nt after compression (i.e.,
    #reads that consist of less than 50 flows), all reads longer than 450 nt after
    #compression (i.e., reads that consist of more than 450 flows), all reads with
    #more than 1 N (ambiguous basecall), and all duplicated reads. The 'good' file
    #will then contain all the read idenfiers that were not discarded based on any
    #of these criteria.




    #assembly@assembly:/mnt/nexenta/assembly/nobackup/dev_150/scripts/pipeline$ sfffile
    #Usage:  sfffile [options...] [MIDList@][sfffile | datadir]...
    #Options:
    #       -r               Rescore the reads using the phred-based quality scores
    #       -o output        The output SFF file
    #       -i accnofile     Include only these reads in output
    #       -e accnofile     Exclude these reads from output
    #       -s [midscheme]   Split reads by MID into separate files
    #       -pick #[k | m]   Randomly pick # input bases (k=thousands, m=millions)
    #       -pickb #[k | m]  Randomly pick # input bases (k=thousands, m=millions)
    #       -pickr #[k | m]  Randomly pick # input reads (k=thousands, m=millions)
    #       -c #             Convert all flowgrams to this # of cycles
    #       -xlr | -gsxlr    Convert all flowgrams to GS XLR cycles (200)
    #       -flx | -gsflx    Convert all flowgrams to GS FLX cycles (100)
    #       -20 | -gs20      Convert all flowgrams to GS 20 cycles (42)
    #       -t filename      File containing accno/trim line information
    #       -tr filename     Same as '-t', but resets trim using this file
    #       -mcf filename    Use this MID configuration file for multiplex info
    #       -nmft            Do not write a manifest into the SFF file
    #
    #   The sfffile program constructs a single SFF file containing the reads from
    #   a list of SFF files and/or 454 runs.  The reads written to the new SFF
    #   file can be filtered using inclusion and exclusion lists of accessions.
    #
    #   Any datadir argument given on the command line should specify the path to
    #   a 454 'analysis' directory (typically named D_2005_...), and may be
    #   prepended with a list of regions separated by a colon (i.e.,
    #   "1,3-5,7:R_dir/D_dir" tells the program to use regions 1, 3, 4, 5 and 7
    #   of R_dir/D_dir).
    #
    #   In addition, multiplexing identifier information can be prepended to an
    #   sff file or analysis directory name, separated by an at sign.  For
    #   example, "mid1@myfile.sff" tells the program to only use the reads
    #   whose 5' end matches the sequence to MID 'mid1'.  Also, the "-s" option
    #   can be used to "split" the file by the multiplex identifiers, writing
    #   all of the input reads matching each MID into a separate file.  If no MID scheme
    #   scheme name is given to the "-s" option, the default GSMIDS+RLMIDS set will be used.
    #   With either mode, the trim points for the output reads is reset to just inside
    #   the MID sequence (i.e., the MID sequence is trimmed from the output read).
    #
    #   The output of the program is written to the file "454Reads.sff" in the
    #   current working directory, or the filename given by the "-o" option.  If
    #   the "-s" option is given, then the per-MID output files will append the
    #   MID names to the filename (i.e., by default for the GSMIDS set, the output
    #   files will be "454Reads.MID1.sff", "454Reads.MID2.sff", ...).
    #
    #   By default, all reads in the input are written to the SFF file.  If the
    #   "-i" or "-e" options are specified, then the accessions given in
    #   those files will change the reads that are output (if both are specified,
    #   then the reads that are in the -i file and not in the -e file will be
    #   output).  Those files should list the accessions one per line, where
    #   the accession should be the first word on the line after an optional
    #   '>' (i.e., if the line begins with '>', that character is skipped, then
    #   the characters to the first whitespace character is read as the accession).
    #   These options are cumulative, in that if multiple -i options are given,
    #   the reads included will be the union of the -i accession lists.
    #
    #   By default, a "manifest" listing the set of 454 runs that were the
    #   source of the reads in an SFF file are written into the index section of
    #   the SFF file (to provide an explicit traceback to the origin of the reads).
    #   This includes the run name (the R_... name), the analysis name (the
    #   D_... name) and the full path to the analysis directory used
    #   in the conversion from 454 run data to SFF file(s).  This
    #   program propagates the manifest from the input SFF files into the
    #   output SFF file, unless the "-nmft" option is given.




    #assembly@assembly:/mnt/nexenta/assembly/nobackup/dev_150/scripts/pipeline$ sffinfo
    #Usage:  sffinfo [options...] [- | sfffile] [accno...]
    #Options:
    #       -a or -accno    Output just the accessions
    #       -s or -seq      Output just the sequences
    #       -q or -qual     Output just the quality scores
    #       -f or -flow     Output just the flowgrams
    #       -t or -tab      Output the seq/qual/flow as tab-delimited lines
    #       -n or -notrim   Output the untrimmed sequence or quality scores
    #       -m or -mft      Output the manifest text
    #
    #   The sffinfo program extracts read information from the SFF file, and
    #   reports it in text form.  By default, a text summary of all of the
    #   read information is output, but the output can be limited to only
    #   the accnos, sequences, quality scores, flowgrams or manifest.  All
    #   output is written to standard output.
    #
    #   The -seq, -qual and -flow options cause the program to output the
    #   sequences, quality scores or flowgrams in a FASTA format, unless the
    #   -tab option is given to generate tab-delimited lines of the read
    #   sequences/scores/flowgram.  By default, the trimmed data  is output,
    #   unless the -notrim output is specified.  Only one of -seq, -qual, -flow
    #   or -mft may be specified (the program uses the last on the command-line.
    #
    #   If the -mft option is given, then the manifest is output, if it exists.
    #   in the SFF file, and the -tab and -notrim options and the accnos on the
    #   command-line are ignored.
    #
    #   If no accnos are given on the command-line, the information from all reads
    #   in the file are output.  Otherwise, just the information for the specified
    #   reads are output.  If an SFF file is specified and it contains an sffcall-
    #   or sfffile-created index, then an index-based lookup is used and the reads
    #   are output in the order they appear on the command line.  If not, the
    #   complete SFF file is scanned and the reads are output in the order they
    #   appear in the file.
    #
    #   If the SFF file is replaced by a "-" on the command line, then
    #   standard input is read for the SFF contents.



    #fastq_to_fasta -h
    #usage: fastq_to_fasta [-h] [-r] [-n] [-v] [-z] [-i INFILE] [-o OUTFILE]
    #Part of FASTX Toolkit 0.0.13.2 by A. Gordon (gordon@cshl.edu)
    #
    #   [-h]         = This helpful help screen.
    #   [-r]         = Rename sequence identifiers to numbers.
    #   [-n]         = keep sequences with unknown (N) nucleotides.
    #                  Default is to discard such sequences.
    #   [-v]         = Verbose - report number of sequences.
    #                  If [-o] is specified,  report will be printed to STDOUT.
    #                  If [-o] is not specified (and output goes to STDOUT),
    #                  report will be printed to STDERR.
    #   [-z]         = Compress output with GZIP.
    #   [-i INFILE]  = FASTA/Q input file. default is STDIN.
    #   [-o OUTFILE] = FASTA output file. default is STDOUT.

if __name__ == '__main__': main()
