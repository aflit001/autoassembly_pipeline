#!/usr/bin/env python
import os,sys
import traceback


if __name__=="__main__":
    import os
    fullpath=os.getcwd()

    # add parent folder to path

    #print "CURRENT PATH " + fullpath
    fullpath=os.path.abspath(fullpath)
    #print "PREVIOUS PATH " + fullpath
    sys.path.insert(0, fullpath)
    sys.path.insert(0, os.path.abspath(os.path.join(fullpath, '..')))
    #print "PATH " + str(sys.path)

import lib.run        as run
import lib.setup      as setup

setupQ    = setup.constants.programs['mkplot']['q']
setupMinY = setup.constants.programs['mkplot']['miY']


import __builtin__

class dict2(dict):
    def __add__(self, dict2add):
        #print 'adding'
        z = dict2(self.items() + dict2add.items())
        #print "adding Z",z
        return z

    def __iadd__(self, dict2add):
        #print 'iadding'
        z = self.__add__(dict2add)
        #print 'iadding Z',z
        return z


__builtin__.dict = dict2

def localRun(name, id, cmd, errNo):
    try:
        res = run.runString(name + " " + id, cmd)
        if res != 0:
            return 100+res
        return res
    except Exception, e:
        sys.stderr.write(name + " " + id + " :: Exception (Job__launch_out): " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: FAILED TO RUN "                + cmd + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: TRACEBACK :: "                 + " ".join(traceback.format_stack()) + "\n")
        return 200+errNo

import traceback
def stop_err( msg, errNo=1 ):
    sys.stderr.write( '%s\n' % msg )
    sys.exit(errNo)


#####################
## CONFIG
#####################
lines     = -1
skipFirst = False
lineType  = 'impulses'
minY      = setupMinY
q         = setupQ # percentil to be highlighted



#####################
## CHECKING
#####################
if len(sys.argv) == 1:
    sys.stderr.writelines('NO INPUT FILE DEFINED')
    sys.exit(1)

inputHistoFile = sys.argv[1]
if not os.path.exists(inputHistoFile):
    sys.stderr.writelines('INPUT FILE '+inputHistoFile+' DOES NOT EXISTS')
    sys.exit(2)

if not os.path.isfile(inputHistoFile):
    sys.stderr.writelines('INPUT FILE '+inputHistoFile+' IS NOT A REGULAR FILE')
    sys.exit(3)

if len(sys.argv) == 3 and sys.argv[2].isdigit():
    lines = int(sys.argv[2])



outplot     = inputHistoFile + '.plot'
outHistoPng = inputHistoFile + '.png'


print "IN HISTO FILE", inputHistoFile
print "OUT PLOT FILE", outplot
print "OUT PNG  FILE", outHistoPng
print 'LINES',lines



#####################
## VARIABLES
#####################
xlabel  = 'KMER FREQUENCY FOR EACH NUMBER OF COPIES'
ylabel  = 'VOLUME OF COPIES (NUMBER x APPEARENCES)'
y2label = '% OF TOTAL VOLUME'


title = os.path.basename(inputHistoFile)
title = title.replace('.histo',    '').replace('.fastq',    '').replace('_sequence', '')
title = title.replace('_',        ' ').replace('.', ' ')

plotValues  = dict()

plotValues += { 'title'       : title,
                'xlabel'      : xlabel,
                'ylabel'      : ylabel,
                'y2label'     : y2label,
                'outhistopng' : outHistoPng,
                'lineType'    : lineType,
                'lines'       : lines,
                'miny'        : minY,
            }



#####################
## STATISTICS 1: size sum data
#####################
size   = 0
sumVal = 0
data   = []

if not os.path.exists(inputHistoFile):
    stop_err("GNUPLOT :: ERROR INPUT HISTO FILE DOES NOT EXISTS " + inputHistoFile,  6)
elif os.path.getsize(inputHistoFile) == 0:
    stop_err("GNUPLOT :: ERROR INPUT HISTO FILE HAS SIZE 0 " + inputHistoFile + ' [' + str(os.path.getsize(inputHistoFile)) + ']',  6)

h    = open(inputHistoFile, 'r')
for line in h:
    line = line.rstrip()
    pos, count = line.split()
    #print "POS",pos,"COUNT",count
    pos   = int(pos)
    count = int(count)
    vol   = pos * count
    size += 1
    if pos > minY: sumVal  += vol
    data.append([pos, count, vol, sumVal-vol, sumVal])
h.close()
plotValues += { 'size'  : size,
                'sum'   : sumVal
            }



#####################
## STATISTICS 2: max maxpos minpos posmax posmin
#####################
if lines == -1:
    lines               = size

plotValues['lines'] = lines


maxVal =  0
maxPos =  9999999999999
minPos =  0
pMax   = -9999999999999
pMin   =  9999999999999
for line in data:
    pos       = line[0]
    count     = line[1]
    vol       = line[2]
    sumPrev   = line[3]
    sumNext   = line[4]
    line.append((sumPrev*100.0/sumVal))
    line.append((sumNext*100.0/sumVal))
    sumPrevPc = line[5]
    sumNextPc = line[6]

    if ( pos <  size ) and ( vol >  maxVal ): maxVal = vol

    if ( pos <= minY ) or  ( pos >= size ):
        continue

    if ( vol <= pMin ) and ( pos <= maxPos ):
        pMin   = vol
        minPos = pos

    if ( vol >= pMax ):
        pMax   = vol
        maxPos = pos


plotValues += { 'max'   : maxVal,
                'pmin'  : pMin,
                'pmax'  : pMax,
                'minpos': minPos,
                'maxpos': maxPos,
}

print "MIN Y %(miny)d SIZE %(size)d MAX %(max)d SUM %(sum)d" % plotValues






#####################
## STATISTICS 3: quartis AreaUnderTheCourve
#####################
#limits
first   = 0
last    = lines
max2    = int(maxVal *  1.2 )
linelen = int(max2   / 10.0 )
if skipFirst:
    first = 1

#percentils
iq = 100 - q

sum1 = int(sumVal / (1.0/((iq/100.0)/2.0)))
sum2 = int(sumVal /  2.0                  )
sum3 = int(sumVal - sum1                  )

plotValues += { 'max2'        : max2,
                'first'       : first,
                'last'        : last,
                'linelen'     : linelen,
                'q'           : q,
                'iq'          : iq,
                'sum1'        : sum1,
                'sum2'        : sum2,
                'sum3'        : sum3,
}

print "SUM  %(sum)d\nSUM1 %(sum1)d\nSUM2 %(sum2)d\nSUM3 %(sum3)d" % plotValues

#sum 5607795473 max 618823955 mk
#sum 6409303133 max 618823955 wr

q1  = 0; q2  = 0; q3  = 0
v1  = 0; v2  = 0; v3  = 0
p1  = 0; p2  = 0; p3  = 0
auc = 0

for line in data:
    pos       = line[0]
    count     = line[1]
    vol       = line[2]
    sumPrev   = line[3]
    sumNext   = line[4]
    sumPrevPc = line[5]
    sumNextPc = line[6]

    if pos > minY:
        #print "POS     %12d\nSUM1    %12d\nSUM2    %12d\nSUM3    %12d\nSUMPREV %12d\nSUMNEXT %12d" % (pos, sum1, sum2, sum3, sumPrev, sumNext)
        if ( q1      == 0    and sumNext >= sum1 ): q1  = pos; v1 = sumPrev     ; p1 = sumPrev / sumVal; #print "["
        if ( sumPrev <= sum2 and sumNext >= sum2 ): q2  = pos; v2 = sumNext - v1; p2 = sumNext / sumVal; #print "-"
        if ( sumPrev <= sum3 and sumNext >= sum3 ): q3  = pos; v3 = sumNext - v1; p3 = sumNext / sumVal; #print "]"
        if ( sumPrev <= sum3 and sumNext >= sum1 ): auc = auc + vol
        #print "\n\n"

        if ( q1 > 0 ) and ( q2 > 0 ) and ( q3 > 0 ):
            break



plotValues += { 'q1'        : q1,
                'q2'        : q2,
                'q3'        : q3,
                'auc'       : auc,
                'aucpc'     : int(((auc * 1.0/(sumVal * 1.0))+.005)*100)
}



print plotValues




#####################
## SAVING DESCRIPTION FILE
#####################
desc = """MAX:     %(max)d
SUM:     %(sum)d
SUM1:    %(sum1)d
SUM2:    %(sum2)d
SUM3:    %(sum3)d
Q:       %(q)d
Q1:      %(q1)d
Q2:      %(q2)d
Q3:      %(q3)d
SIZE:    %(size)d
LINES:   %(lines)d
FIRST:   %(first)d
LAST:    %(last)d
LINELEN: %(linelen)d""" % plotValues

i = open(inputHistoFile + '.desc', 'w')
i.write(desc)
i.close()


if not os.path.exists(inputHistoFile + '.desc') or os.path.getsize(inputHistoFile + '.desc') == 0:
    stop_err("GNUPLOT :: ERROR CREATING OUTPUT DESC FILE " + inputHistoFile + '.desc',  2)

#plotValues += { 'desc'        : desc }
print "DESC\n%s" % desc










#####################
## CREATING GRAPHICS DECORATIONS - SHADOW
#####################
plotValues += { 'q2-1': q2-1,
                'q2+1': q2+1 }

bots = """
set object rect from %(q1)d,0   to %(q3)d,   %(max2)d behind fc lt -1 fs solid 0.10 noborder
set object rect from %(q2-1)d,0 to %(q2+1)d, %(max2)d behind fc lt -1 fs solid 0.20 noborder
""" % plotValues

plotValues += { 'bots'        : bots }
#print "BOTS\n%(bots)s" % plotValues







#####################
## CREATING GRAPHICS DECORATIONS - ARROWS
#####################
plotValues += { 'pmin+linelen'   : ( pMin + linelen + 1 ),
                'pmax+linelen'   : ( pMax + linelen + 1 ),
                'pmin+linelen+.1': ( pMin + linelen + (linelen*0.1) ),
                'pmax+linelen+.1': ( pMax + linelen + (linelen*0.1) )
            }

arrow = """
set arrow 1 from %(minpos)d,%(pmin+linelen)d to %(minpos)d,%(pmin)d filled front ls 5
set arrow 2 from %(maxpos)d,%(pmax+linelen)d to %(maxpos)d,%(pmax)d filled front ls 6
set label \"%(minpos)d\" at %(minpos)d,%(pmin+linelen+.1)d
set label \"%(maxpos)d\" at %(maxpos)d,%(pmax+linelen+.1)d
""" % plotValues

plotValues += { 'arrow'   : arrow }
#print "ARROW\n%(arrow)s" % plotValues







#####################
## READING STATS FILE
#####################
statFile    = inputHistoFile.replace('.histo', '.stats')
statFileLen = os.path.basename(statFile)
stats       = []

print "STATFILE", statFileLen

if not os.path.exists(statFile) or os.path.getsize(statFile) == 0:
    stop_err("GNUPLOT :: ERROR INPUT STAT FILE " + statFile,  7)

j = open(statFile, 'r')
for line in j:
    line = line.strip()
    stats.append(line)
j.close()


statVals      = {}
statMaxValLen = 0
statMaxKeyLen = 0
for line in stats:
    line          = line.rstrip()
    key, val      = line.split()
    statVals[key] = val
    if len(val) > statMaxValLen: statMaxValLen = len(val)
    if len(key) > statMaxKeyLen: statMaxKeyLen = len(key)



for key in [['size', 'size'], ['max', 'highiest'], ['lines', 'max count'], ['linelen', 'max shown'], ['q1', 'Q%2d' % iq], ['q2', 'Q50'], ['q3', 'Q%2d' % q], ['auc', 'Volume Q%2d' % q], ['aucpc', 'Volume %% Q%2d' % q]]:
    val  = plotValues[key[0]]
    nkey = key[1] + ":"
    statVals[nkey] = val
    if len(str(val)) > statMaxValLen: statMaxValLen = len(str(val))
    if len(nkey    ) > statMaxKeyLen: statMaxKeyLen = len(nkey    )

if statMaxValLen < 16: statMaxValLen = 16

statsStr  = ""
statsKeys = [[x.upper(), x] for x in statVals.keys()]
statsKeys.sort()


for key in statsKeys:
    val = statVals[key[1]]
    statsStr += key[0].ljust(statMaxKeyLen + 1, '.') + str(val).rjust(statMaxValLen + 1, '.') + '\\n'


plotValues += { 'statsstr'        : statsStr }
print "STATISTICS STRING\n%(statsstr)s" % plotValues







#####################
## GENERATING GRAPHICS DATA
#####################
histotmp = ""
for line in data:
    histotmp += "\t" + "\t".join([str(int(x)) for x in line]) + "\n"
    #print "\t".join([str(int(x)) for x in line])
plotValues += {  'histotmp'    : histotmp }






#####################
## GENERATING PLOTTING SCRIPT
#####################
plot = """
set title   "%(title)s"

set xlabel  "%(xlabel)s"
set ylabel  "%(ylabel)s"
set y2label "%(y2label)s"

set xrange  [0:%(last)s]
set yrange  [0:%(max2)s]
set y2range [0:100]

set format y  "%%2.0tx10^%%L"
set format y2 "%%3.0f"
set y2tics 0,25
set ytics nomirror
set y2tics out

set label "%(statsstr)s"  at graph  0.50, graph  0.98

set bars large

set style line 1 lt rgb 'red'   lw 1
set style line 2 lt rgb 'green' lw 2
set style line 3 lt rgb 'blue'  lw 1

set style line 4 lt rgb 'pink'  lw 2

set style line 5 lt rgb 'red'   lw 3
set style line 6 lt rgb 'blue'  lw 3

%(arrow)s

set grid
set palette model RGB
set pointsize 0.5
set origin 0,0

set terminal png size 1024,768 large font "Courier New,12"
set output "%(outhistopng)s"

%(bots)s

plot '-' [%(first)s:%(last)s] using 1:3 with %(lineType)s ls 1 axis x1y1                 notitle, \\
     ''  [%(first)s:%(last)s] using 1:3 with %(lineType)s ls 2 axis x1y1 smooth csplines notitle, \\
     ''  [%(first)s:%(last)s] using 1:7 with %(lineType)s ls 3 axis x1y2 smooth csplines notitle
%(histotmp)s
end
%(histotmp)s
end
%(histotmp)s
end
exit
""" % plotValues


m = open(outplot, 'w')
m.write(plot)
m.close()

if not os.path.exists(outplot) or os.path.getsize(outplot) == 0:
    stop_err("GNUPLOT :: ERROR CREATING OUTPUT PLOT FILE " + outplot,  2)


#####################
## RUNNING GNUPLOT
#####################
res = localRun('gnuplot', '0', 'gnuplot '+outplot, 1)
if res:
    stop_err("GNUPLOT :: ERROR CREATING OUTPUT PNG FILE " + outHistoPng,  3)

if not os.path.exists(outHistoPng) or os.path.getsize(outHistoPng) == 0:
    stop_err("GNUPLOT :: ERROR CREATING OUTPUT PNG FILE " + outHistoPng,  4)

#os.remove(tmpplot)
#os.remove(tmphisto)
