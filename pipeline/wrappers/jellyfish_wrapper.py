#!/usr/bin/env python
"""
count, stats, histo, dump, merge, query, cite, qhisto, qdump, qmerge, jf
COUNT
 -m, --mer-len=uint32                    *Length of mer
 -s, --size=uint64                       *Hash size
 -t, --threads=uint32                     Number of threads (1)
 -o, --output=string                      Output prefix (mer_counts)
 -c, --counter-len=Length in bits         Length of counting field (7)
     --out-counter-len=Length in bytes    Length of counter field in output (4)
 -C, --both-strands                       Count both strand, canonical representation (false)
 -p, --reprobes=uint32                    Maximum number of reprobes (62)
 -r, --raw                                Write raw database (false)
 -q, --quake                              Quake compatibility mode (false)
     --quality-start=uint32               Starting ASCII for quality values (64)
     --min-quality=uint32                 Minimum quality. A base with lesser quality becomes an N (0)
 -L, --lower-count=uint64                 Don't output k-mer with count < lower-count
 -U, --upper-count=uint64                 Don't output k-mer with count > upper-count
     --matrix=Matrix file                 Hash function binary matrix
     --timing=Timing file                 Print timing information
     --stats=Stats file                   Print stats
STATS
 -L, --lower-count=uint64                 Don't consider k-mer with count < lower-count
 -U, --upper-count=uint64                 Don't consider k-mer with count > upper-count
 -o, --output=c_string                    Output file
HISTO
 -l, --low=uint64                         Low count value of histogram (1)
 -h, --high=uint64                        High count value of histogram (10000)
 -i, --increment=uint64                   Increment value for buckets (1)
 -t, --threads=uint32                     Number of threads (1)
 -f, --full                               Full histo. Don't skip count 0. (false)
 -o, --output=c_string                    Output file
DUMP
 -c, --column                             Column format (false)
 -t, --tab                                Tab separator (false)
 -L, --lower-count=uint64                 Don't output k-mer with count < lower-count
 -U, --upper-count=uint64                 Don't output k-mer with count > upper-count
 -o, --output=c_string                    Output file
MERGE
 -s, --buffer-size=Buffer length          Length in bytes of input buffer (10000000)
 -o, --output=string                      Output file (mer_counts_merged.jf)
     --out-counter-len=uint32             Length (in bytes) of counting field in output (4)
     --out-buffer-size=uint64             Size of output buffer per thread (10000000)
"""

import os, shutil, subprocess, sys, tempfile, glob
import traceback
import atexit

if __name__=="__main__":
    import os
    fullpath=os.getcwd()

    # add parent folder to path

    #print "CURRENT PATH " + fullpath
    fullpath=os.path.abspath(fullpath)
    #print "PREVIOUS PATH " + fullpath
    sys.path.insert(0, fullpath)
    sys.path.insert(0, os.path.abspath(os.path.join(fullpath, '..')))
    #print "PATH " + str(sys.path)

import lib.parameters as parameters
import lib.run        as run
import lib.setup      as setup

setupExe = setup.constants.programs['jellyfish']['exe']
setupTmp = setup.constants.programs['jellyfish']['tmp']
setupPv  = setup.constants.programs['jellyfish']['pv' ]

def stop_err( msg, errNo=1 ):
    sys.stderr.write( '%s\n' % msg )
    sys.stderr.flush()
    sys.exit(errNo)

def loadParams():
    #--output=string   [ count stats histo dump merge ]
    #--threads         [ count       histo            ]
    #--lower-count     [ count stats histo dump       ]
    #--upper-count     [ count stats histo dump       ]
    #--out-counter-len [ count                  merge ]
    #count, stats, histo, dump, merge, query, cite, qhisto, qdump, qmerge, jf

    parameter = parameters.parameters()

    params = {
    #PROGRAM
        'program'        : { 'optType': 'value',               'dashes': 2, 'groupKey': True,                             'mandatory': True,                      'data': { 'help':'[string         ] all, count, dump, histo, merge, stats', 'choices': ['all', 'count', 'dump', 'histo', 'merge', 'stats'] }},
        'histo'          : {                                   'dashes': 2, 'groups': 'all',                                                                      'data': { 'help':'[Histogram file ] Filename to Output Histogram' }},
            #parser.add_option( 'all',               dest='all',                         help='[Bool            ] Makes all steps')
            #parser.add_option( 'count',             dest='count',                       help='[Bool            ] Count k-mers or qmers in fasta or fastq files')
            #parser.add_option( 'dump',              dest='dump',                        help='[Bool            ] Dump k-mer counts')
            #parser.add_option( 'histo',             dest='histo',                       help='[Bool            ] Create an histogram of k-mer occurrences')
            #parser.add_option( 'merge',             dest='merge',                       help='[Bool            ] Merge jellyfish databases')
            #parser.add_option( 'stats',             dest='stats',                       help='[Bool            ] Display some statistics about the k-mers in the hash')
    #ALL
        'histo'          : {                                   'dashes': 2, 'groups': 'all',                                                                      'data': { 'help':'[string          ] Histogram output file' }},
    #COUNT
        'both-strands'   : { 'optType':'bool',                 'dashes': 2, 'groups': 'all,count',                                           'default': False,    'data': { 'help':'[Bool            ] Count both strand, canonical representation (false)' }},
        'counter-len'    : {                    'type':'int',  'dashes': 2, 'groups': 'all,count',                                           'default': 7,        'data': { 'help':'[Length in bits  ] Length of counting field (7)' }},
        'lower-count'    : {                    'type':'int',  'dashes': 2, 'groups': 'all,count,dump,stats',                                'default': 1,        'data': { 'help':'[uint64          ] Don\'t output k-mer with count < lower-count [ count stats histo dump       ]' }},
        'upper-count'    : {                    'type':'int',  'dashes': 2, 'groups': 'all,count,dump,stats',                                'default': 300,      'data': { 'help':'[uint64          ] Don\'t output k-mer with count > upper-count [ count stats histo dump       ]' }},
        'matrix'         : {                                   'dashes': 2, 'groups': 'all,count',                                                                'data': { 'help':'[Matrix file     ] Hash function binary matrix' }},
        'mer-len'        : {                    'type':'int',  'dashes': 2, 'groups': 'all,count',                        'mandatory': True,                      'data': { 'help':'[uint32          ]*Length of mer' }},
        'min-quality'    : {                    'type':'int',  'dashes': 2, 'groups': 'all,count',                                           'default': 0,        'data': { 'help':'[uint32          ] Minimum quality. A base with lesser quality becomes an N (0)' }},
        'size'           : {                    'type':'int',  'dashes': 2, 'groups': 'all,count'      ,                  'mandatory': True,                      'data': { 'help':'[uint64          ]*Hash size' }},
        'output'         : {                                   'dashes': 2, 'groups': 'all,count,histo,dump,merge,stats',                                         'data': { 'help':'[string          ] Output prefix (mer_counts) for COUNT or output file for others [ count stats histo dump merge ]' }},
        'out-counter-len': {                    'type':'int',  'dashes': 2, 'groups': 'all,count,merge',                                     'default': 4,        'data': { 'help':'[Length in bytes ] Length (in bytes) of counter field in output (4) [ count                  merge ]' }},
        'raw'            : { 'optType':'bool',                 'dashes': 2, 'groups': 'all,count',                                           'default': False,    'data': { 'help':'[Bool            ] Write raw database (false)' }},
        'reprobes'       : {                    'type':'int',  'dashes': 2, 'groups': 'all,count',                                           'default': 62,       'data': { 'help':'[uint32          ] Maximum number of reprobes (62)' }},
        'quake'          : { 'optType':'bool',                 'dashes': 2, 'groups': 'all,count,dump,merge,histo',                          'default': False,    'data': { 'help':'[Bool            ] Quake compatibility mode (false)' }},
        'quality-start'  : {                    'type':'int',  'dashes': 2, 'groups': 'all,count',                                           'default': 64,       'data': { 'help':'[uint32          ] Starting ASCII for quality values (64)' }},
        'stats'          : {                                   'dashes': 2, 'groups': 'all,count',                                                                'data': { 'help':'[Stats file      ] Filename to Output Statistics' }},
        'threads'        : {                    'type':'int',  'dashes': 2, 'groups': 'all,count,histo',                                     'default': 1,        'data': { 'help':'[uint32          ] Number of threads (1) [ count       histo            ]' }},
        'timing'         : {                                   'dashes': 2, 'groups': 'all,count',                                                                'data': { 'help':'[Timing file     ] Print timing information' }},
        'version'        : { 'optType':'bool',                 'dashes': 2, 'groups': 'all,count,histo,dump,merge,stats',                                         'data': { 'help':'[Bool            ] Print version' }},
    #STATS
        #'lower-count'   : {                    'type':'int',  'dashes': 2, 'data': { 'help':'[uint64          ] Don\'t output k-mer with count < lower-count [ count stats histo dump       ]' }},
        #'upper-count'   : {                    'type':'int',  'dashes': 2, 'data': { 'help':'[uint64          ] Don\'t output k-mer with count > upper-count [ count stats histo dump       ]' }},
        #'output'        : {                                   'dashes': 2, 'data': { 'help':'[string          ] Output prefix (mer_counts) [ count stats histo dump merge ]' }},
    #HISTO
        'full'           : { 'optType':'bool',                 'dashes': 2, 'groups': 'all,histo',                                           'default': False,    'data': { 'help':'[bool            ] Full histo. Don\'t skip count 0. (false)' }},
        'increment'      : {                    'type':'int',  'dashes': 2, 'groups': 'all,histo',                                           'default': 1,        'data': { 'help':'[uint64          ] Increment value for buckets (1)' }},
        'high'           : {                    'type':'int',  'dashes': 2, 'groups': 'all,histo',                                           'default': 10000,    'data': { 'help':'[uint64          ] High count value of histogram (10000)' }},
        'low'            : {                    'type':'int',  'dashes': 2, 'groups': 'all,histo',                                           'default': 1,        'data': { 'help':'[uint64          ] Low count value of histogram (1)' }},
        #'output'        : {                                   'dashes': 2, 'data': { 'help':'[string          ] Output prefix (mer_counts) [ count stats histo dump merge ]' }},
    #DUMP
        'column'         : { 'optType':'bool',                 'dashes': 2, 'groups': 'all,dump',                                            'default': False,    'data': { 'help':'[bool            ] Column format (false)' }},
        'tab'            : { 'optType':'bool',                 'dashes': 2, 'groups': 'all,dump',                                            'default': False,    'data': { 'help':'[bool            ] Tab separator (false)' }},
        #'lower-count'   : {                    'type':'int',  'dashes': 2, 'data': { 'help':'[uint64          ] Don\'t output k-mer with count < lower-count [ count stats histo dump       ]' }},
        #'upper-count'   : {                    'type':'int',  'dashes': 2, 'data': { 'help':'[uint64          ] Don\'t output k-mer with count > upper-count [ count stats histo dump       ]' }},
        #'output'        : {                                   'dashes': 2, 'data': { 'help':'[string          ] Output prefix (mer_counts) [ count stats histo dump merge ]' }},
    #MERGE
        'buffer-size'     : {                    'type':'int', 'dashes': 2, 'groups': 'all,merge',                                           'default': 10000000, 'data': { 'help':'[uint64          ] Length in bytes of input buffer (10000000)' }},
        'out-buffer-size' : {                    'type':'int', 'dashes': 2, 'groups': 'all,merge',                                           'default': 10000000, 'data': { 'help':'[uint64          ] Size of output buffer per thread (10000000)' }},
        #'out-counter-len' : {                    'type':'int', 'dashes': 2, 'groups': 'all,merge',                                           'default': 4,        'data': { 'help':'[Length in bytes ] Length (in bytes) of counter field in output (4) [ count                  merge ]' }},
        #'output'         : {                                  'dashes': 2, 'data': { 'help':'[string          ] Output prefix (mer_counts) [ count stats histo dump merge ]' }},
    }


    parameter.parseList(params)
    parameter.compile(sys.argv[1:])
    #parameter.parser.print_usage()
    #parameter.parser.print_help()
    #print sys.argv
    #print parameter.parser.values

    validateParams(parameter)

    return parameter

def validateParams(parameter):
    args      = parameter.getArgs()

    if parameter.hasParam('version'):
        return

    if len(args) == 0:
        stop_err("NO INPUT FILES DEFINED", 2)


    basenames = []

    for arg in args:
        files = glob.glob(arg)
        if len(files) == 0:
            stop_err("no globable input file given in '" + arg + "'", 3)

        for file in files:
            basenames.append(os.path.basename(file))
            if ( not os.path.exists(file) ):
                stop_err("file '" + arg + "' does not exists", 4)

    commomPrefix = os.path.commonprefix(basenames)
    commomPrefix = commomPrefix.rstrip("_.-")
    print "COMMOM PREFIX " + commomPrefix
    if commomPrefix == "":
        commomPrefix = "jellyout"


    if parameter.hasParam('program'):
        prog = parameter.getValue('program')
        if prog not in ['all', 'count', 'dump', 'histo', 'merge', 'stats']:
            stop_err("UNKNOWN PROGRAM: " + prog, 5)

        output = parameter.getValue('output')
        if output is None:
            print "NO OUTPUT"
            outName = commomPrefix
            if   prog == 'all' or prog == 'merge':
                outName += ".jf"
            elif prog == 'count':
                outName += "_mer_count"
            elif prog == 'histo':
                outName += ".histo"
            elif prog == 'dump':
                if   parameter.getValue('tab') is not None:
                    outName += ".csv"
                elif parameter.getValue('column') is not None:
                    outName += ".tab"
                else:
                    outName += ".dump.fasta"
            print "NEW OUTPUT "+outName
            parameter.addValue('output', 'value', outName)

def localRun(name, id, cmd, errNo):
    try:
        res = run.runString(name + " " + id, cmd)
        if res != 0:
            return 100+res
        return res
    except Exception, e:
        sys.stderr.write(name + " " + id + " :: Exception (Job__launch_out): " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: FAILED TO RUN "                + cmd + " EXCEPTION " + str(e) + "\n")
        sys.stderr.write(name + " " + id + " :: TRACEBACK :: "                 + " ".join(traceback.format_stack()) + "\n")
        return 200+errNo



def __main__():
    #Parse Command Line
    parameter = loadParams()
    cmd       = parameter.getCmd()
    #args      = parameter.getArgs()

    #print "CMD  " + cmd
    #print "ARGS " + str(args)
    #sys.exit(-1)

    prog         = parameter.getValue('program')
    outputMain   = parameter.getValue('output' )
    outputStat   = parameter.getValue('stats'  )
    outputHisto  = parameter.getValue('histo'  )
    outputMatrix = parameter.getValue('matrix' )
    outputTiming = parameter.getValue('timing' )
    outputColumn = parameter.getValue('column' )
    outputTab    = parameter.getValue('tab'    )
    print "PROGRAM   " + prog.capitalize()
    print "VALID CMD " + cmd

    if prog == 'all':

        print "PROGRAM " + prog.capitalize() + " OUTPUT MAIN  " + outputMain

        if outputStat is None:
            outputStat = outputMain
            outputStat = outputStat.replace(".jf", "")
            outputStat += ".stats"
            parameter.addValue('stats', 'value', outputStat)

        if outputHisto is None:
            outputHisto  = outputMain
            outputHisto = outputHisto.replace(".jf", "")
            outputHisto += ".histo"
            parameter.addValue('histo', 'value', outputHisto)


        print "PROGRAM " + prog.capitalize() + " OUTPUT STATS " + outputStat
        print "PROGRAM " + prog.capitalize() + " OUTPUT HISTO " + outputHisto
        sys.stdout.flush()

        #################
        # ALL :: COUNT
        #################
        count = 'python ' + sys.argv[0] + ' --program count ' + parameter.getCmd() + ' ' + " ".join(parameter.getArgs())
        name  = prog.capitalize() + " COUNT "
        print name + count
        cid = os.path.basename((parameter.getArgs())[0])

        res = localRun(name, cid, count, 6)
        if res != 0:
            stop_err(name + " " + cid + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)

        if (not os.path.exists(outputStat) or os.path.getsize(outputStat) == 0):
            stop_err(name + cid + " :: ERROR CREATING OUTPUT STAT "      + outputStat,   7)

        if outputMatrix is not None and (not os.path.exists(outputMatrix) or os.path.getsize(outputMatrix) == 0):
            stop_err(name + cid + " :: ERROR CREATING OUTPUT MATRIX "    + outputMatrix, 8)

        if outputTiming is not None and (not os.path.exists(outputTiming) or os.path.getsize(outputTiming) == 0):
            stop_err(name + cid + " :: ERROR CREATING OUTPUT TIMING "    + outputTiming, 9)

        if not os.path.exists(outputMain) or os.path.getsize(outputMain) == 0:
            stop_err(name + cid + " :: ERROR CREATING OUTPUT JF  "       + outputMain,  10)


        ##todo: dump

        #################
        # ALL :: HISTO
        #################
        parameter.deleteValue('output')
        histo = 'python ' + sys.argv[0] + ' --program histo ' + parameter.getCmd() + ' --output ' + outputHisto + ' ' + outputMain
        name  = prog.capitalize() + " HISTO "
        print name + histo
        hid = outputMain

        res = localRun(name, hid, histo, 11)
        if res != 0:
            stop_err(name + " " + hid + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)

        if not os.path.exists(outputHisto) or os.path.getsize(outputHisto) == 0:
            stop_err(name + hid + " :: ERROR CREATING OUTPUT HISTO  "       + outputHisto,   12)


        #if outputMatrix is None:
        #    outputMatrix = ""# = outputMain
        #    outputMatrix.replace(".jf", ".jmatrix")

        #if outputTiming is None:
        #    outputTiming = ""# = outputMain
        #    outputTiming.replace(".jf", ".jtiming")
        #
        #if outputColumn is None:
        #    outputColumn = ""# = outputMain
        #    outputColumn.replace(".jf", ".jcol")
        #
        #if outputTab is None:
        #    outputTab = ""# = outputMain
        #    outputTab.replace(".jf", ".jtab")



    elif prog == 'histo':
        print "PROGRAM " + prog.capitalize()

        quake    = parameter.getValue('quake')
        progName = prog
        skip     = ['quake', 'threads', 'output']
        if quake is not None:
            progName = 'q' + prog
        else:
            skip += ['size']

        outfile = parameter.getValue('output')
        histo   = setupExe + ' ' + progName + ' ' + parameter.getCmd(skip=skip) + ' ' + " ".join(parameter.getArgs()) + ' > ' + outfile
        name    = prog.capitalize() + " "
        hid     = outputMain
        print name + histo

        res = localRun(name, hid, histo, 13)
        if res != 0:
            stop_err(name + " " + hid + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)

        if not os.path.exists(outputMain) or os.path.getsize(outputMain) == 0:
            stop_err(name + hid + " :: ERROR CREATING OUTPUT HISTOGRAM " + outputMain,  14)




    elif prog == 'stats':
        print "PROGRAM " + prog.capitalize()
        stats = setupExe + ' stats ' + parameter.getCmd() + ' ' + " ".join(parameter.getArgs())
        name  = prog.capitalize() + " "
        print name + stats
        sid = outputMain

        res = localRun(name, sid, stats, 15)
        if res != 0:
            stop_err(name + " " + sid + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)

        if not os.path.exists(outputMain) or os.path.getsize(outputMain) == 0:
            stop_err(name + sid + " :: ERROR CREATING OUTPUT STAT "      + outputMain,   16)




    elif prog == 'dump':
        print "PROGRAM " + prog.capitalize()

        quake = parameter.getValue('quake')
        progName = prog
        if quake is not None:
            progName = 'q' + prog

        outFile = outputMain
        parameter.deleteValue('output')
        dumpArgs = parameter.getArgs()
        toCleanDump = False

        if len(dumpArgs) > 1:
            print "  MERGING FIRST "
            toCleanDump = True
            merge       = 'python ' + sys.argv[0] + ' --program merge ' + parameter.getCmd() + ' --output ' + outFile + '.jf' + ' ' + " ".join(dumpArgs)
            name        = prog.capitalize() + " COUNT/MERGE "
            cid         = os.path.basename((parameter.getArgs())[0])
            print name + merge

            res = localRun(name, cid, merge, 6)
            if res != 0:
                stop_err(name + " " + cid + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)

            dumpArgs = [ outFile + '.jf' ]
            print "  FINISHED MERGING "


        dump    = setupExe + ' ' + progName + ' ' + parameter.getCmd() + ' ' + " ".join(dumpArgs) + " " + setupPv + " > " + outFile
        name    = prog.capitalize() + " "
        print name + dump
        did     = outputMain

        res = localRun(name, did, dump, 17)
        if res != 0:
            stop_err(name + " " + did + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)

        if not os.path.exists(outputMain) or os.path.getsize(outputMain) == 0:
            stop_err(name + did + " :: ERROR CREATING DUMP "    + outputMain,  18)

        if toCleanDump:
            for outTmpFile in dumpArgs:
                os.remove(outTmpFile)



    elif prog == 'merge':
        print "PROGRAM " + prog.capitalize()
        args  = parameter.getArgs()
        name  = prog.capitalize() + " "
        mid   = outputMain
        quake = parameter.getValue('quake')

        if len(args) == 1:
            if args[0] != outputMain:
                try:
                    shutil.copy(args[0], outputMain)
                except (IOError, os.error), why:
                    stop_err(name + " " + mid + " :: ERROR RUNNING " + name + ". ERROR " + str(19) + " NOT ABLE TO MOVE INTERMEDIATE DB :: " + str(why), 19)

        else:
            progName = prog
            skip=['quake', 'threads']
            if quake is not None:
                progName = 'q' + prog
            else:
                skip    += ['size', 'mer-len']


            parameter.deleteValue('output')
            (tmp_index_dir, outputTmp) = makeTmp(mid)
            atexit.register(cleanTmp, tmp_index_dir)

            inDbFiles = parameter.getArgs()
            inDbFiles.sort()

            merge = setupExe + ' ' + progName + ' ' + parameter.getCmd(skip=skip) + ' --output ' + outputTmp + " " + " ".join(inDbFiles)
            print name + merge

            res = localRun(name, outputTmp, merge, 19)
            if res != 0:
                cleanTmp(tmp_index_dir)
                stop_err(name + " " + mid + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)


            if not os.path.exists(outputTmp) and os.path.exists(outputTmp + '_0'):
                if os.path.exists(outputTmp + '_1'):
                    cleanTmp(tmp_index_dir)
                    stop_err(name + mid + " :: ERROR RUNNING " + name + ". CREATED TOO MANY JF  "       + outputMain,     20)
                shutil.move( outputTmp + '_0', outputTmp )



            if not os.path.exists(outputTmp) or os.path.getsize(outputTmp) == 0:
                cleanTmp(tmp_index_dir)
                stop_err(name + mid + " :: ERROR RUNNING " + name + ". ERROR CREATING OUTPUT JF  "       + outputMain,     20)

            try:
                shutil.move(outputTmp, mid)
            except (IOError, os.error), why:
                stop_err(name + " " + mid + " :: ERROR RUNNING " + name + ". ERROR " + str(22) + " NOT ABLE TO MOVE INTERMEDIATE DB :: " + str(why), 21)

            if not os.path.exists(outputMain) or os.path.getsize(outputMain) == 0:
                stop_err(name + mid + " :: ERROR RUNNING " + name + ". ERROR CREATING OUTPUT JF  "       + outputMain,     23)


        if outputStat is not None:
            statSkip = ['quake', 'threads', 'size', 'out-buffer-size', 'mer-len', 'output']
            stat     = setupExe + ' stats ' + parameter.getCmd(skip=statSkip) + ' --output ' + outputStat + " " + outputMain

            res = localRun(name, outputStat, stat, 24)
            if res != 0:
                if os.path.exists(outputMain): os.remove(outputMain)
                if os.path.exists(outputStat): os.remove(outputStat)
                stop_err(name + " " + outputMain + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + stat, res)


        if outputHisto is not None:
            skipHisto = ['quake', 'threads', 'output']
            if quake is None:
                skipHisto += ['size', 'mer-len', 'out-buffer-size']
            histo     = setupExe + ' histo ' + parameter.getCmd(skip=skipHisto) + ' --output ' + outputHisto + " " + outputMain

            res = localRun(name, outputHisto, histo, 25)
            if res != 0:
                if os.path.exists(outputMain ): os.remove(outputMain)
                if os.path.exists(outputHisto): os.remove(outputHisto)
                stop_err(name + " " + outputMain + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + histo, res)





    elif prog == 'count':
        print "PROGRAM " + prog.capitalize()
        parameter.deleteValue('output')
        (tmp_index_dir, outputTmp) = makeTmp(outputMain)
        outputTmp  = outputTmp.replace(".jf", "")
        outputTmp += "_mer_counts"

        atexit.register(cleanTmp, tmp_index_dir)

        count = setupExe + ' count ' + parameter.getCmd() + ' --output ' + outputTmp + ' ' + " ".join(parameter.getArgs())
        name = prog.capitalize() + " "
        print name + count
        cid = outputTmp

        res = localRun(name, cid, count, 24)
        if res != 0:
            stop_err(name + " " + cid + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)


        if outputMatrix is not None and (not os.path.exists(outputMatrix) or os.path.getsize(outputMatrix) == 0):
            cleanTmp(tmp_index_dir)
            stop_err(name + cid + " :: ERROR RUNNING " + name + ". ERROR CREATING OUTPUT MATRIX "    + outputMatrix, 25)
        if outputTiming is not None and (not os.path.exists(outputTiming) or os.path.getsize(outputTiming) == 0):
            cleanTmp(tmp_index_dir)
            stop_err(name + cid + " :: ERROR RUNNING " + name + ". ERROR CREATING OUTPUT TIMING "    + outputTiming, 26)
        if outputStat   is not None and (not os.path.exists(outputStat)   or os.path.getsize(outputStat)   == 0):
            cleanTmp(tmp_index_dir)
            stop_err(name + cid + " :: ERROR RUNNING " + name + ". ERROR CREATING OUTPUT STAT "      + outputStat,   27)

        outFiles = checkTmp(tmp_index_dir, outputTmp, name, cid)


        parameter.deleteValue('stats')

        merge = 'python ' + sys.argv[0] + ' --program merge ' + parameter.getCmd() + ' --output ' + outputMain + ' ' + " ".join(outFiles)
        name  = prog.capitalize() + " COUNT/MERGE "
        print name + merge
        cid = os.path.basename((parameter.getArgs())[0])

        res = localRun(name, cid, merge, 6)
        if res != 0:
            stop_err(name + " " + cid + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)

        #quake    = parameter.getValue('quake')
        #progName = 'merge'
        #if quake is not None:
        #    progName = 'q' + prog
        #
        #merge = 'jellyfish ' + progName + ' ' + parameter.getCmd(skip=['quake']) + ' --output ' + outputMain  + ' ' + " ".join(outFiles)
        #name  = prog.capitalize() + " MERGE "
        #print name + merge
        #mid = os.path.basename(outputTmp)
        #
        #res = localRun(name, mid, merge, 28)
        #if res != 0:
        #    cleanTmp(tmp_index_dir)
        #    stop_err(name + " " + mid + " :: ERROR RUNNING " + name + ". ERROR " + str(res) + " CMD " + cmd, res)
        #
        #if not os.path.exists(outputMain) or os.path.getsize(outputMain) == 0:
        #    cleanTmp(tmp_index_dir)
        #    stop_err(name + mid + " :: ERROR CREATING OUTPUT JF  "       + outputMain,   29)
        #
        cleanTmp(tmp_index_dir)







def cleanTmp(tmp_index_dir):
    if os.path.exists( tmp_index_dir ):
        shutil.rmtree( tmp_index_dir )
        pass

def checkTmp(index_tmp_dir, outputTmp, name, id):
    outFiles = glob.glob(outputTmp + '*')
    if len(outFiles) == 0:
        if os.path.exists( tmp_index_dir ):
            shutil.rmtree( tmp_index_dir )
        stop_err(name + id + " :: ERROR CREATING OUTPUT TEMPORARY DB " + outputTmp + " NO FILES", 27)

    for file in outFiles:
        if os.path.exists(file):
            size = os.path.getsize(file)
            if ( size == 0 ):
                if os.path.exists( tmp_index_dir ):
                    shutil.rmtree( tmp_index_dir )
                stop_err(name + id + " :: ERROR CREATING OUTPUT TEMPORARY DB " + outputTmp + ". OUTPUT FILE " + file + " IS EMPTY" , 28)
        else:
            if os.path.exists( tmp_index_dir ):
                shutil.rmtree( tmp_index_dir )
            stop_err(name + id + " :: ERROR CREATING OUTPUT TEMPORARY DB " + outputTmp + ". NOT ABLE TO ACCESS " + file , 29)
    return outFiles

def makeTmp(output):
    tmp_index_dir   = tempfile.mkdtemp(prefix='jelly_', dir=setupTmp)
    outputTmp  = output
    outputTmp  = os.path.basename(outputTmp)
    outputTmp  = os.path.join(tmp_index_dir, outputTmp)
    return (tmp_index_dir, outputTmp)


if __name__=="__main__": __main__()


"""
count, stats, histo, dump, merge, query, cite, qhisto, qdump, qmerge, jf



COUNT
Usage: jellyfish count [options] file:path+

Count k-mers or qmers in fasta or fastq files

Options (default value in (), *required):
 -m, --mer-len=uint32                    *Length of mer
 -s, --size=uint64                       *Hash size
 -t, --threads=uint32                     Number of threads (1)
 -o, --output=string                      Output prefix (mer_counts)
 -c, --counter-len=Length in bits         Length of counting field (7)
     --out-counter-len=Length in bytes    Length of counter field in output (4)
 -C, --both-strands                       Count both strand, canonical representation (false)
 -p, --reprobes=uint32                    Maximum number of reprobes (62)
 -r, --raw                                Write raw database (false)
 -q, --quake                              Quake compatibility mode (false)
     --quality-start=uint32               Starting ASCII for quality values (64)
     --min-quality=uint32                 Minimum quality. A base with lesser quality becomes an N (0)
 -L, --lower-count=uint64                 Don't output k-mer with count < lower-count
 -U, --upper-count=uint64                 Don't output k-mer with count > upper-count
     --matrix=Matrix file                 Hash function binary matrix
     --timing=Timing file                 Print timing information
     --stats=Stats file                   Print stats
     --usage                              Usage
 -h, --help                               This message
     --full-help                          Detailed help
 -V, --version                            Version





STATS
Usage: jellyfish stats [options] db:path

Statistics

Display some statistics about the k-mers in the hash:

Unique:    Number of k-mers which occur only once.
Distinct:  Number of k-mers, not counting multiplicity.
Total:     Number of k-mers, including multiplicity.
Max_count: Maximum number of occurrence of a k-mer.

Options (default value in (), *required):
 -L, --lower-count=uint64                 Don't consider k-mer with count < lower-count
 -U, --upper-count=uint64                 Don't consider k-mer with count > upper-count
 -v, --verbose                            Verbose (false)
 -o, --output=c_string                    Output file
     --usage                              Usage
 -h, --help                               This message
     --full-help                          Detailed help
 -V, --version                            Version




HISTO
Usage: jellyfish histo [options] db:path

Create an histogram of k-mer occurrences

Create an histogram with the number of k-mers having a given
count. In bucket 'i' are tallied the k-mers which have a count 'c'
satisfying 'low+i*inc <= c < low+(i+1)*inc'. Buckets in the output are
labeled by the low end point (low+i*inc).

The last bucket in the output behaves as a catchall: it tallies all
k-mers with a count greater or equal to the low end point of this
bucket.

Options (default value in (), *required):
 -l, --low=uint64                         Low count value of histogram (1)
 -h, --high=uint64                        High count value of histogram (10000)
 -i, --increment=uint64                   Increment value for buckets (1)
 -t, --threads=uint32                     Number of threads (1)
 -f, --full                               Full histo. Don't skip count 0. (false)
 -o, --output=c_string                    Output file
 -v, --verbose                            Output information (false)
     --usage                              Usage
     --help                               This message
     --full-help                          Detailed help
 -V, --version                            Version




DUMP
Usage: jellyfish stats [options] db:path

Dump k-mer counts

By default, dump in a fasta format where the header is the count and
the sequence is the sequence of the k-mer. The column format is a 2
column output: k-mer count.

Options (default value in (), *required):
 -c, --column                             Column format (false)
 -t, --tab                                Tab separator (false)
 -L, --lower-count=uint64                 Don't output k-mer with count < lower-count
 -U, --upper-count=uint64                 Don't output k-mer with count > upper-count
 -o, --output=c_string                    Output file
     --usage                              Usage
 -h, --help                               This message
 -V, --version                            Version



MERGE
Usage: jellyfish merge [options] input:c_string+

Merge jellyfish databases

Options (default value in (), *required):
 -s, --buffer-size=Buffer length          Length in bytes of input buffer (10000000)
 -o, --output=string                      Output file (mer_counts_merged.jf)
     --out-counter-len=uint32             Length (in bytes) of counting field in output (4)
     --out-buffer-size=uint64             Size of output buffer per thread (10000000)
 -v, --verbose                            Be verbose (false)
     --usage                              Usage
 -h, --help                               This message
 -V, --version                            Version


"""
