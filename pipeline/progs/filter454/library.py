# import rquired modules
import os

def _invertDictionaryOfLists(dictionary, duplicate_values = True):

    # create an empty inverted dictionary
    inverted_dictionary = {}

    # loop over the identifiers in the dictionary
    for identifier in dictionary.keys():

        # loop over the values for this identifier
        for value in dictionary[identifier]:

            # if duplicate values are allowed
            if duplicate_values:

                # add this identifier to the list of values for this key
                try:
                    inverted_dictionary[value].append(identifier)
                except KeyError:
                    inverted_dictionary[value] = [identifier]

            # else duplicate values are not allowed
            else:

                # store the value:identifier pair
                inverted_dictionary[value] = identifier

    # return the result
    return inverted_dictionary

# end of _invertDictionaryOfLists


def _getCompressedFastaData(fasta_file_contents, sequence_data, max_polymer):

    # initialize identifier and sequence
    identifier = None
    sequence   = []

    # loop over lines
    for line in fasta_file_contents:

        # strip the line
        line = line.strip()

        # if this is an empty line
        if not line:

            # skip it
            continue

        # if this is a header line
        if line[0] == ">":

            # if there was a previous sequence
            if identifier and sequence:

                # convert the sequence list to a string
                sequence = "".join(sequence)

                # compress the sequence
                sequence_data[identifier] = _compressSequence(sequence,
                                                              max_polymer)

            # get the sequence identifier
            identifier = line[1:]

            # create an empty sequence list
            sequence = []

        # else this is a sequence line
        else:

            # add the sequence to the dictionary
            sequence.append(line)

    # if there was any sequence
    if identifier and sequence:

        # convert the sequence list to a string
        sequence = "".join(sequence)

        # compress the sequence
        sequence_data[identifier] = _compressSequence(sequence,
                                                      max_polymer)

# end of _getCompressedFastaData


def _createCompressedSequenceDictionary(fasta_file_or_dir, max_polymer = 1):

    # create a sequence dictionary with compressed sequences
    sequence_data = _createSequenceDictionary(fasta_file_or_dir,
                                              _getCompressedFastaData,
                                              parse_parameter = max_polymer)

    # return the result
    return sequence_data

# end of _createCompressedSequenceDictionary


def _groupSequences(sequence_data, seed_length, trim_length = 0):

    """
    Returns a dictionary with <identifier> : [identifiers] pairs
    where <identifier> is the id of the longest sequence of a group and
    [identifiers] is a list of all identifiers belonging to that group.
    Groups are determined based on exact sequence matches with the same start.
    The initial match is performed based on seed_length, do this parameter
    defines the tradeoff between speed and accuracy.
    """

    # group the sequence ids based on the sequence seeds
    seed_groups = _groupSequenceSeeds(sequence_data,
                                      seed_length)

    # create sequence groups from the seed groups
    sequence_groups = _createSequenceGroupsFromSeedGroups(sequence_data,
                                                          seed_groups,
                                                          trim_length)

    # return the result
    return sequence_groups

# end of _groupSequences


def _groupSequenceSeeds(sequence_data, seed_length):

    # create an empty seed groups dictionary
    seed_groups = {}

    # create a list of (sequence_seed, sequence_length, identifier) pairs
    sequence_seeds = [(sequence_data[seq_id][:seed_length],
                       len(sequence_data[seq_id]),
                       seq_id) \
                      for seq_id in sequence_data.keys()]

    # sort and reverse the sequence seeds
    # (so we have the longest sequence per seed first)
    sequence_seeds.sort()
    sequence_seeds.reverse()

    # store the previously seen sequence seed and identifiers
    previous_seed = None
    previous_ids  = []

    # loop over the sorted (sequence_seed, identifier) pairs
    for (sequence_seed, length, identifier) in sequence_seeds:

        # if the current seed is smaller than the seed length
        if previous_seed and len(sequence_seed) < seed_length:

            # this seed may still belong to the previous seed group
            previous_seed = previous_seed[:len(sequence_seed)]

        # if this is not the same seed as the previous one
        if sequence_seed != previous_seed:

            # if there was a previous seed
            if previous_seed:

                # get the group id from the longest sequence
                group_id = previous_ids[0]

                # store this seed group
                seed_groups[group_id] = previous_ids

            # reset the previous ids list
            previous_ids = []

        # add the current id to the previous ids list
        previous_ids.append(identifier)

        # store the seed for the next round
        previous_seed = sequence_seed

    # if there was any sequence seed
    if previous_seed:

        # get the group id from the longest sequence
        group_id = previous_ids[0]

        # store the final seed group
        seed_groups[group_id] = previous_ids

    # return the result
    return seed_groups

# end of _groupSequenceSeeds


def _createSequenceGroupsFromSeedGroups(sequence_data, seed_groups, trim_length = 0):

    """
    3 possibilities for trimming:
    trim_length = 0    : no trimming, use full slave length
    trim_length = 0..1 : percentage-based trimming
    trim_length >= 1   : length-based trimming
    """

    # create an empty sequence groups dictionary
    sequence_groups = {}

    # loop over the sequence seed groups
    for group_id in seed_groups.keys():

        # create a shortcut
        seed_group = seed_groups[group_id]

        # as long as there are sequences left in the seed group
        while seed_group:

            # create empty grouped and ungrouped sequences list
            grouped_ids   = []
            ungrouped_ids = []

            # determine the master id and corresponding sequence
            master_id       = seed_group[0]
            master_sequence = sequence_data[master_id].upper()

            # loop over the sequences in the seed group
            for slave_id in seed_group[1:]:

                # get the corresponding slave sequence and its length
                slave_sequence = sequence_data[slave_id].upper()
                slave_length   = len(slave_sequence)

                # determine the align length
                align_length = slave_length

                # if percentage based trimming is required
                if 0 < trim_length < 1:

                    # determine the new align legth
                    align_length = int((1 - trim_length) * align_length)

                # else if nucleotide based trimming is required
                elif trim_length:

                    # determine the new align legth
                    align_length -= int(trim_length)

                # if this slave sequence is a substring of the master sequence
                if slave_sequence[:align_length] == master_sequence[:align_length]:

                    # add the slave id to the grouped ids list
                    grouped_ids.append(slave_id)

                # else the slave sequence is part of a new group
                else:

                    # add the slave id to the ungrouped ids list
                    ungrouped_ids.append(slave_id)

            # store the sequence group
            sequence_groups[master_id] = [master_id] + grouped_ids

            # continue processing the ungrouped ids
            seed_group = ungrouped_ids

    # return the result
    return sequence_groups

# end of _createSequenceGroupsFromSeedGroups


def _createBaseCountDictionary(alphabet, wildcard = None):

    # create an empty base counts dictionary
    base_counts  = {}

    # loop over the alphabet
    for letter in alphabet:

        # create an entry in the base counts dictionary
        base_counts[letter] = 0

    # if a wildcard was supplied
    if wildcard:

        # add the wildcard to the base counts dictionary
        base_counts[wildcard] = 0

    # return the result
    return base_counts

# end of _createBaseCountDictionary


def _countBaseFrequenciesInSequence(sequence, base_counts, alphabet, wildcard = None):

    # initialize already counted to 0
    already_counted = 0

    # loop over all possible letters in the alphabet
    for letter in alphabet:

        # count the number of occurences of this letter in the sequence
        letter_count = sequence.upper().count(letter)

        # add the letter count to the base counts
        base_counts[letter] += letter_count

        # add the count of this letter to the already counted bases
        already_counted += letter_count

    # if a wildcard was specified
    if wildcard:

        # determine the number of wildcard characters
        wildcard_count = len(sequence) - already_counted

        # store the number of wildcard characters in the sequence
        base_counts[wildcard] += wildcard_count

# end of _countBaseFrequenciesInSequence


def _getReadAnalysisData(analysis_file):

    # create an empty analysis data dictionary
    analysis_data = {}

    # read the file contents
    lines = _readFileContents(analysis_file)

    # skip the header line
    _skipFileLines(lines, 1)

    # loop over the lines
    for line in lines:

        # split the line into attributes
        attributes = line.strip().split()

        # get the read id
        read_id = attributes[0]

        # get the data for this read
        analysis_data[read_id] = {"compressed_length" : int(attributes[1]),
                                  "a_count"           : int(attributes[2]),
                                  "t_count"           : int(attributes[3]),
                                  "c_count"           : int(attributes[4]),
                                  "g_count"           : int(attributes[5]),
                                  "n_count"           : int(attributes[6]),
                                  "duplicate_count"   : int(attributes[7]),
                                  "duplicate_master"  : attributes[8]}

    # return the result
    return analysis_data

# end of _getReadAnalysisData


def _writeTextFile(file_data, file_name):

    # open a handle to the specified file
    out_file = open(file_name, "w")

    # write the file data to the output file
    out_file.write(file_data)

    # close the output file
    out_file.close()

# end of _writeTextFile


def _getFastaData(fasta_file_contents, sequence_data, exact_headers = False):

    # create lists to store headers and sequences
    identifiers = []
    sequences   = []

    # loop over lines
    for line in fasta_file_contents:

        # strip the line
        line = line.strip()

        # if this is an empty line
        if not line:

            # skip it
            continue

        # if this is a header line
        if line[0] == ">":

            # get the sequence identifier
            identifier = line[1:]

            # add this to the identifiers list
            identifiers.append(identifier)

            # add a new entry to the sequences list
            sequences.append([])

        # else this is a sequence line
        else:

            # add this to the last entry in the sequences list
            sequences[-1].append(line)

    # store the sequences in the data dictionary
    _storeSequencesInDictionary(identifiers,
                                sequences,
                                sequence_data,
                                exact_headers)

# end of _getFastaData


def _createSequenceDictionary(fasta_file_or_dir, parse_function = _getFastaData, parse_parameter = None):

    # create an empty sequence data dictionary
    sequence_data = {}

    # create a list of fasta files
    fasta_files = _getFileList(fasta_file_or_dir)

    # loop over the fasta files
    for fasta_file in fasta_files:

        # read file contents
        lines = _readFileContents(fasta_file)

        # get the corresponding fasta records using the parse function
        parse_function(lines,
                       sequence_data,
                       parse_parameter)

    # return the result
    return sequence_data

# end of _createSequenceDictionary


def _getFileList(file_or_dir):

    # if the input is a dir
    if os.path.isdir(file_or_dir):

        # get lists of files from the fasta and quality directories
        files = _getFileListFromDir(file_or_dir)

    # else the input is a single file
    else:

        # create a list of the single file
        files = [file_or_dir]

    # return the result
    return files

# end of _getFileList


def _getFileListFromDir(directory, return_full_paths = True):

    # get the file list
    files = _getItemListFromDir(directory,
                                return_full_paths,
                                item_index = 2)

    # return the result
    return files

# end of _getFileListFromDir


def _getItemListFromDir(directory, return_full_paths = True, item_index = 2):

    # create a generator to walk over the directory
    generator = os.walk(directory)

    # get an item listing of the directory
    items = generator.next()[item_index]

    # if full path are requested
    if return_full_paths:

        # add path info the the items
        items = [os.path.join(directory, item) for item in items]

    # return the result
    return items

# end of _getItemListFromDir


def _readFileContents(file_name, read_as_lines = True):

    # if the content should be read as lines
    if read_as_lines:

        # read the file contents using a generator
        data = _readFileContentsAsGenerator(file_name)

    # else read the content as a single string
    else:

        # open the file
        in_file = open(file_name, "r")

        # read file contents
        data = in_file.read()

        # close the file
        in_file.close()

    # return the result
    return data

# end of _readFileContents


def _readFileContentsAsGenerator(file_name):

    # open the file
    in_file = open(file_name, "r")

    # read the first line from the file
    line = in_file.readline()

    # as long as there is any file data
    while line:

        # yield this line to where this generator is called
        yield line

        # read the next line
        line = in_file.readline()

    # close the input file
    in_file.close()

# end of _readFileContentsAsGenerator


def _compressSequence(sequence, max_polymer = 1):

    # if the max polymer size is 1
    if max_polymer == 1:

        # use the quicker function to compress the sequence
        compressed_sequence = _fullyCompressSequence(sequence)

    # else we need to use the slower version
    else:

        # (partially) compress the sequence
        compressed_sequence = _partiallyCompressSequence(sequence,
                                                         max_polymer)

    # return the result
    return compressed_sequence

# end of compressSequence


def _fullyCompressSequence(sequence, max_polymer = 1):

    # fully compress the sequence (except the first nucleotide)
    compressed_sequence = [sequence[index] \
                           for index in xrange(1, len(sequence)) \
                           if sequence[index] != sequence[index-1]]

    # check if the first nucleotide is unique
    if not compressed_sequence or compressed_sequence[0] != sequence[0]:

        # add the first nucleotide to the compressed sequence
        compressed_sequence = [sequence[0]] + compressed_sequence

    # convert the compressed sequence list to a string
    compressed_sequence = "".join(compressed_sequence)

    # return the result
    return compressed_sequence

# end of _fullyCompressSequence


def _partiallyCompressSequence(sequence, max_polymer = 1):

    # create an empty compressed sequence list
    compressed_sequence = []

    # store the last seen nucleotides
    last_nucleotides = []

    # loop over the nucleotides in the sequence
    for nucleotide in sequence:

        # convert the nucleotide to uppercase
        nucleotide = nucleotide.upper()

        # if this is not the same nucleotide as the last one
        if last_nucleotides and nucleotide != last_nucleotides[-1]:

            # compress and store the nucleotide(s)
            compressed_sequence += last_nucleotides[:max_polymer]

            # reset the last nucleotides list
            last_nucleotides = []

        # add it to the last nucleotides list
        last_nucleotides.append(nucleotide)

    # if there was a sequence
    if last_nucleotides:

        # compress and store the nucleotide(s)
        compressed_sequence += last_nucleotides[:max_polymer]

    # convert the compressed sequence list to a string
    compressed_sequence = "".join(compressed_sequence)

    # return the result
    return compressed_sequence

# end of _partiallyCompressSequence



def _skipFileLines(lines, to_skip = 1):

    # keep track of the number of skipped lines
    skipped_lines = 0

    # loop over the lines
    for line in lines:

        # skip this line
        skipped_lines += 1

        # if we have skipped enough lines
        if skipped_lines == to_skip:

            # stop the loop
            break

# end of _skipFileLines
