# import required modules
import sys

# import custom modules
from library import _getReadAnalysisData
from library import _writeTextFile
from library import _skipFileLines


def startAnalysis(analysis_file, good_output, bad_output, min_length, max_length, max_N_count, filter_clonality):

    # get the 454 read analysis data
    analysis_data = _getReadAnalysisData(analysis_file)

    # get the good and bad reads
    good_reads, \
    bad_reads   = _getGoodAndBadReads(analysis_data,
                                      min_length,
                                      max_length,
                                      max_N_count,
                                      filter_clonality)

    # generate the corresponding text output
    good_text = "\n".join(good_reads)
    bad_text  = "\n".join(bad_reads)

    # write the output files
    _writeTextFile(good_text,
                   good_output)
    _writeTextFile(bad_text,
                   bad_output)

# end of startAnalysis


def _getGoodAndBadReads(analysis_data, min_length, max_length, max_N_count, filter_clonality):

    # create empty good and bad reads lists
    good_reads = []
    bad_reads  = []

    # loop over the reads
    for read_id in analysis_data.keys():

        # create a shortcut
        read_info = analysis_data[read_id]

        # assume that this is a good read
        good_read = True

        # if the read is too small
        if min_length and read_info["compressed_length"] < min_length:

            # this is a bad read
            good_read = False

        # if the read is too large
        elif max_length and read_info["compressed_length"] > max_length:

            # this is a bad read
            good_read = False

        # if there are too many N's
        elif max_N_count and 0 < read_info["n_count"] > max_N_count:

            # this is a bad read
            good_read = False

        # if we want to filter on clonality
        elif filter_clonality and read_info["duplicate_count"] > 1 \
                              and read_info["duplicate_master"] != read_id:

            # this is a bad read
            good_read = False

        # if this read passed all checks
        if good_read:

            # add it to the list of good reads
            good_reads.append(read_id)

        # else this read failed at least one check
        else:

            # add it to the list of badd reads
            bad_reads.append(read_id)

    # return the result
    return good_reads, bad_reads

# end of _getGoodAndBadReads



# MAIN

# check argument length
if len(sys.argv[1:]) != 7:

    # print an error message and exit
    print "Usage: python filter454Reads.py <454 read analysis file> <good reads output file> <bad reads output file> <minimum compressed read length (0 disables)> <maximum compressed read length (0 disables)> <maximum number of Ns per read (0 disables, use -1 to accept no Ns)> <filter reads on clonaly (0 disables, 1 enables)>"
    sys.exit()


# get parameters
analysis_file    = sys.argv[1]
good_output      = sys.argv[2]
bad_output       = sys.argv[3]
min_length       = int(sys.argv[4])
max_length       = int(sys.argv[5])
max_N_count      = int(sys.argv[6])
filter_clonality = int(sys.argv[7])

# start the analysis
startAnalysis(analysis_file,
              good_output,
              bad_output,
              min_length,
              max_length,
              max_N_count,
              filter_clonality)
