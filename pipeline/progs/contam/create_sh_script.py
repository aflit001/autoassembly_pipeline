#!/usr/bin/python
import os
def exec_cmd(cmd):
    p = os.popen(cmd,"r")
    result = ''
    while 1:
        line = p.readline()
        if not line: break
        result += line
    return result

cmd = """cd /home/assembly/dev_150;PwD=$PWD; (for folder in sample_heinz sample_heinz_tiny sample_pennellii; do echo $folder; (cd $folder/_tmp ; for fastq in illumina*/seq.fastq; do echo "  ILL $PwD/$folder/_tmp/$fastq > $PwD/$folder/_tmp/assembly/$fastq.clean.fastq"; done; ); (cd $folder/raw; find . -name *.sff -exec bash -c 'fs={}; fs=${fs//\//_}; fs=${fs//\./_}; fs=${fs//__/_}; fs=${fs//\_4/4}; echo " 454 '$PwD/$folder'/raw/{} > '$PwD/$folder'/_tmp/assembly/$fs/seq.sff"' \;); done)"""


r = exec_cmd(cmd)
lines = r.split('\n')
ill_counter = 0
fw_in_file = ''
fw_out_file = ''

for line in lines:
    if line.startswith('  ILL'):
        ill_counter += 1
        
        in_file, out_file = line.replace('ILL', '', 1).replace(' ', '').split('>')
        out_dir = os.path.dirname(out_file)
        # fastq_filter.py <out_dir> <fw_reads.fastq> <rv_reads.fastq> <fw_reads_filtered.fastq> <rv_reads_filtered.fastq> <bwa_db> <max_percentage_contamination> <contamination_file>
        print "mkdir -p %s" %(out_dir)
        if ill_counter == 1:
            fw_in_file = in_file
            fw_out_file = out_file
        elif ill_counter == 2:
            ill_counter = 0
            
            print "[ -f %s ] && echo \"Skip job\" || /home/smits065/projects/saulo_filter_tomato/fastq_filter.py %s %s %s %s %s %d %s\n" %(out_file, fw_in_file, in_file, fw_out_file, out_file, '/home/smits065/projects/saulo_filter_tomato/db/contamination_without_ecolidb_v0.2.fa', 0, fw_out_file+'.contamination.txt')
    else:
        if ill_counter != 0:
           # print ill_counter
            raise Exception, "no rv file found for previous ill file, so crash!"

    if line.startswith(' 454'):
        #  ./sff_filter.py check/check_ids.sff real_data_test_output/myfiltered.sff db/contamination_without_ecolidb.fa 97 real_data_test_output/mycontamination.txt
        in_file, out_file = line.replace('454', '', 1).replace(' ', '').split('>')
        out_dir = os.path.dirname(out_file)
  
        print "mkdir -p %s" %(out_dir)
        print "[ -f %s ] && echo \"Skip job\" || /home/smits065/projects/saulo_filter_tomato/sff_filter.py %s %s %s %d %s\n" %(out_file,in_file, out_file, '/home/smits065/projects/saulo_filter_tomato/db/contamination_without_ecolidb_v0.2.fa', 97, out_file+'.contamination.txt')
