This system consists of:
    SETUP:
        ROOT/scripts/behavior.py
            Setup script where all the behavior and parameters for the programs
            can be set.
            It imports ROOT/scripts/ftp/folderStruct.py.
            It is a symlink to ftp/behavior and will create
            ROOT/scripts/setup.json which will be used by the rest of the system
            (including pipeline).

        ROOT/scripts/ftp/folderStruct.py
            Setup script containing the description of the folders and projects.

        ROOT/scripts/mktree.sh
            Script to create the files containing the tree structure of the
            parents folder: FILETREE  FILETREETMP


    LIBS:
        ROOT/scripts/pipeline/lib/
            Link to shared libraries in ROOT/scripts/ftp folder.

        ROOT/scripts/pipeline/lib/config.py
            Setup python script containing most of the pertinent constants of
            the pipeline.

        ROOT/scripts/pipeline/src/
            Folder containing Scons portable code

        ROOT/scripts/pipeline/techs/
            Folder containing the library to encapsulate sequencing structure.

        ROOT/scripts/pipeline/wrappers/
            Folder containing the wrappers to the programs to be called and
            taking care of problems and execution.

        ROOT/scripts/pipeline/progs/
            Folder containing the programs needed to run the pipeline. They
            system wide version can also be used. This directory is added
            to the PATH variable by the run and runReal scripts.

        ROOT/scripts/pipeline/tree
            Creates ROOT/scripts/pipeline/tree.dat and
            ROOT/scripts/pipeline/tree.png, the first the textual description
            of the pipeline steps and the second a graphical representation
            (which, often, will be too big to visualize).


    RUN:
        ROOT/scripts/pipeline/run
            Runs the makefile script in dry-run mode

        ROOT/scripts/pipeline/runReal
            Runs the makefile script for real

        ROOT/scripts/pipeline/clean
            Cleans (deletes) most of the files generated

        ROOT/scripts/pipeline/SConstruct
            Scons makefile. Python script with Scons framework defining what to
            do with the data and, effectively, creating the pipeline.
            SConstruc will run:
                - Jellyfish
                - Quake
                - progs/contam *
                - solexaQA DynamicTrim
                - fastqCount *


    * progs/contam
        Home brew scrpts to check the files against contaminatio database using bwa.
    * fastqCount
        Home brew program which reads a fastq file and generates a complete report
        of number of reads, qualities and kmers


Last updated: Thu Sep 13 12:33:27 CEST 2012
Steps:
    for each project:
        for each sample:
            for each technology:
                if illumina:
                    for each library:
                        for each pair:
                            * trim pairs                         | (01)
                            * clean contamination                | (01)        > (02)
                            * create jellyfish db                | (02)        > (03)
                        * merge jellyfish dbs                    | (03)        > (04)
                    * merge jellyfish dbs                        | (04)        > (05)
                    * run quake on (05) for each clean pair or   | (02) + (05) > (06)
                      clean non-paired (single) file (02)

                if 454:
                    for each library:
                        for each run:
                            * clean contamination (07)

    (01) = trimmed fastq files                   (.trimmed.single.fastq and .trimmed.paired.fastq)
    (02) = fastq files cleaned from contaminants (.contamCleaned.fastq)
    (06) = kmer correct files                    (.quake.cor.fastq and quake.cor_single.fastq)
    (07) = sff files cleaned                     (.contamCleaned.sff)

    .fastq --> .fastq.trimmed.single.fastq -> .fastq.trimmed.single.fastq.contamCleaned.fastq --> .fastq.trimmed.single.fastq.contamCleaned.quake.cor_single.fastq
           '-> .fastq.trimmed.paired.fastq -> .fastq.trimmed.paired.fastq.contamCleaned.fastq --> .fastq.trimmed.paired.fastq.contamCleaned.quake.cor.fastq
                                                                                              '-> .fastq.trimmed.paired.fastq.contamCleaned.quake.cor_single.fastq
    .sff   -> .sff.contamCleaned.sff

    (01) (02) (06) (07) can be found in <PROJECT NAME>/_tmp/<FILE NAME>/
    (03) (04) (05)      can be found in <PROJECT NAME>/_tmp/assembly/
