import os,sys
import roche, illumina
import types

#class dataset(object):
#    """Species dataset"""
#    def __init__(self, name):
#        self.name         = name
#        self.dstFolder    = None
#        self.technologies = {}
#
#
#    def getName(self):
#        return self.name
#
#
#    def add(self, name, data):
#        self.technologies[name] = data
#        if self.dstFolder is None:
#            self.dstFolder = os.path.abspath(os.path.join(data.dstFolder, '..'))
#
#    def get(self):
#        return self.technologies
#
#    def getNames(self):
#        for key in self.technologies.keys():
#            yield key
#
#    def getData(self):
#        for key in self.getNames:
#            yield [key, self.technologies[key]]
#
#    def get(self, name):
#        if name in self.technologies:
#            return self.technologies[name]
#        else:
#            print "DATASET: KEY DOES NOT EXISTS:",key
#            print "  OPTIONS ARE:",self.getNames()
#            return None
#
#    def __iter__(self):
#        for el in self.technologies.values():
#            yield el
#
#    def getTech(self, techName ):
#        for value in self.technologies[ techName ]:
#            yield value
#
#    def __str__(self):
#        lns  = "DATASET %s\n" % self.name
#        lns += "DATASET %s = Dst Folder %s\n" % (self.name, self.dstFolder)
#        for techName in self.getNames():
#            #lns += "DATASET %s  TECH %s  SAMPLE %s\n" % ( self.name, techName, sampleName )
#            tech = self.getTech( techName )
#            for ln in str(tech).split('\n'):
#                lns += "DATASET %s  %s\n" % ( self.name, ln )
#                #lns += "  " + techName + "  " + libName + "  " + lib.name + "\n"
#        return lns



class datasets(object):
    """All species"""
    def __init__(self, name, setup):
        self.name      = name
        self.setup     = setup
        self.datasets  = {}
        self.index     = 0
        self.dstFolder = setup.base

    def getName(self):
        return self.name

    def getCount(self):
        return len(self.datasets.keys())


    def keys(self):
        return self.datasets.keys()

    def values(self):
        return self.datasets.values()

    def contains(self, key):
        return key in self.datasets.keys()


    def items(self):
        return self.datasets.items()

    def iteritems(self):
        return self.datasets.iteritems()

    def iterkeys(self):
        return self.datasets.iterkeys()

    def itervalues(self):
        return self.datasets.itervalues()

    def add(self, dataset):
        name = dataset.getName()
        if name is None:
            print "DATASETS: input to add has to have a getName"
            sys.exit(1)
        self.datasets[name] = dataset

    def get(self, key):
        if self.datasets.has_key(key):
            return self.datasets.get(key)
        else:
            print "DATASETS: COULD NOT FIND DATASET ",key
            print "          AVAILABLE: ", self.keys()
            return None

    def __iter__(self):
        for value in self.datasets.values():
            yield value

    def next(self):
        if self.index == len(self.datasets.values) - 1:
            self.index = 0
            raise StopIteration

        self.index = self.index + 1

        return self.datasets.values[self.index]

    def __str__(self):
        lns  = "DATASETS %s\n" % self.name
        lns += "DATASETS %s = Dst Folder %s\n" % (self.name, self.dstFolder)
        for dataset in self.datasets:
            for ln in str(self.datasets[dataset]).split('\n'):
                lns += "DATASETS %s  %s\n" % ( self.name, ln )

        return lns
