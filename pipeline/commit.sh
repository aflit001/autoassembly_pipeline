#!/bin/bash

DATESTR=`date --rfc-3339=seconds | tr " |,|:|\+" _`

git config pack.window 25M           2>&1 | tee -a $LOG
git config pack.windowMemory 25M     2>&1 | tee -a $LOG
git config pack.compression 1        2>&1 | tee -a $LOG
git config pack.packSizeLimit 25M    2>&1 | tee -a $LOG
git config pack.deltaCacheSize 25M   2>&1 | tee -a $LOG
git config pack.deltaCacheLimit 25M  2>&1 | tee -a $LOG
git config core.bigFileThreshold 25M 2>&1 | tee -a $LOG


git add -v .     2>&1 | tee -a $LOG

git status                        2>&1 | tee -a $LOG

STATUSN=`git status -s | grep -v \?\? | wc -l`
STATUSN=$((STATUSN-1))
STATUST=`git status -s | grep -v \?\? | perl -MFile::Basename -ane 'BEGIN { %p } END { map { my $k = $_; print $k, " => ", (scalar keys %{$p{$k}}), "\n"; } sort keys %p; } $d=dirname $F[1]; $p{$F[0]}{$d}++;'`
STATUSL=`git status -s | grep -v \?\? | perl -MFile::Basename -ane 'BEGIN { %p } END { map { my $k = $_; print $k, " => ", (scalar keys %{$p{$k}}), "\n"; } sort keys %p; foreach my $k (sort keys %p) { foreach my $kk ( sort keys %{$p{$k}} ) { print "\t$kk => $p{$k}{$kk}\n"; } } } $d=dirname $F[1]; $p{$F[0]}{$d}++;'`

if [[ ! -z $(git ls-files --deleted) ]]; then
    git rm $(git ls-files --deleted)  2>&1 | tee -a $LOG
fi


echo "STATUS $STATUS ($STATUSN)"
echo "STATUS $STATUS ($STATUST)"
git commit -am "$DATESTR :: $STATUSN :: $STATUST"  2>&1 | tee -a $LOG
