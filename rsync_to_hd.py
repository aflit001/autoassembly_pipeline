#!/usr/bin/python
import os,sys
import subprocess
import shutil
from operator import itemgetter

dry_run  = True
symbolic = True

if len(sys.argv) == 1:
    print "no config file given"
    sys.exit(1)

infile = sys.argv[1]

if not os.path.exists(infile):
    print "config file %s does not exists" % infile
    sys.exit(1)

if not infile.endswith('.py'):
    print "config file %s is not a python script" % infile
    sys.exit(1)

execfile(infile)

try:
    folders.sort()
except:
    print "no folder given"
    sys.exit(1)

try:
    excludes.sort()
except:
    print "no folder given"
    sys.exit(1)

if len(folders) == 0:
    print "no folder given"
    sys.exit(1)


#reset
#16777216

#folders = [
#        "/home/assembly/tomato150/denovo",
#        "/home/assembly/tomato150/reseq",
#        "/home/assembly/tomato150/ril",
#]
#
#
#excludes = [
#        "--exclude _tmp_nobackup",
#        "--exclude _prelim_nobackup",
#        "--exclude _prefiltered_nobackup",
#        "--exclude 'reseq/mapped*' ",
#        "--exclude 'FASTQC_checked*' ",
#        "--exclude 'report_checked*' ",
#        "--exclude '*.checked.*csv' ",
#        #"--exclude filtered",
#        #"--exclude FASTQC_filtered",
#        #"--exclude report_filtered",
#        #"--exclude '*.filtered.*csv'",
#        "--exclude 'raw/*'",
#        "--exclude '.~*'",
#        #"--exclude docs/*",
#        #"--exclude *.py"
#]

print "folders:\n\t",
print "\n\t".join( folders  ), "\n"
print "excludes:\n\t",
print "\n\t".join( excludes ), "\n"
print "dry run :",dry_run
print "symbolic:",symbolic

#CMD1="rsync --dry-run --archive --prune-empty-dirs --human-readable --hard-links --delete --delete-excluded --list-only --block-size=67108864
CMD1="""rsync  --dry-run --archive --prune-empty-dirs                  --hard-links --delete --delete-excluded             --block-size=67108864 --out-format="%f|%n" """


for exclude in excludes:
    CMD1 += ' ' + exclude

print CMD1

tolink     = []
todel      = []
total_size =  0

for folder in folders:
    CMD2     = "%(cmd1)s %(folder)s ." % { 'cmd1': CMD1, 'folder': folder }
    print CMD2, "\n"

    CMD2_RES = subprocess.Popen(CMD2, stdout=subprocess.PIPE, shell=True)

    stdout   = CMD2_RES.communicate()[0]

    ret      = CMD2_RES.returncode

    if ret != 0:
        if ret < 0:
            print "error running rsync %s. killed by signal %d" % ( folder, ret )
            sys.exit(ret)
        else:
            print "error running rsync %s. error #%d" % ( folder, ret )
            sys.exit(ret)

    else:
        print "  running rsync %s finished successfully" % folder

    for line in stdout.split('\n'):
        #print line
        line = line.strip()

        if len(line) == 0:
            continue

        vals = line.split('|')
        #print vals,"\n"
        if len(vals) != 2:
            if line.startswith('deleting '):
                todel.append(line[9:])
                continue

            else:
                print "INVALID", vals
                continue

        vals[0] = '/' + vals[0]

        if not os.path.exists(vals[1]):
            tolink.append(vals)
            if os.path.isfile(vals[0]):
                total_size += os.path.getsize(vals[0])

        else:
            if os.path.islink(vals[1]) and not symbolic:
                todel.append(vals[1])
                tolink.append(vals)
                if os.path.isfile(vals[0]):
                    total_size += os.path.getsize(vals[0])


tolink = sorted(tolink, key=itemgetter(1))
todel.sort(reverse=True)

for fln in todel:
    print "    deleting %s" % fln

    if dry_run:
        continue

    if os.path.exists(fln):
        if os.path.isdir(fln):
            os.rmdir(fln)
        else:
            os.remove(fln)


for pair in tolink:
    files = { 'src': pair[0], 'dst': pair[1] }
    print "    copying %(src)s -> %(dst)s" % files

    if dry_run:
        continue

    if os.path.isdir( files['src'] ):
        print "      src is dir. skipping"
        continue

    dst_path = os.path.dirname( files['dst'] )

    if not os.path.exists( dst_path ):
        print "      dst dir %s does not exists. creating" % dst_path
        os.makedirs( dst_path )

    if os.path.exists( files['dst'] ):
        print "      dst file %s exists. deleting" % files['dst']
        continue
        #os.remove( files['dst'] )

    if not os.path.exists( files['dst'] ):
        print "      linking"
        if symbolic:
            os.symlink(files['src'], files['dst'])
        else:
            try:
                os.link(files['src'], files['dst'])
            except OSError:
                print "        linking failed. copying"
                shutil.copy2(files['src'], files['dst'])
    else:
        print "      skipping"

unity = 'b'
if total_size >= 1024:
    total_size /= 1024.0
    unity       = 'Kb'
if total_size >= 1024:
    total_size /= 1024.0
    unity       = 'Mb'
if total_size >= 1024:
    total_size /= 1024.0
    unity       = 'Gb'
if total_size >= 1024:
    total_size /= 1024.0
    unity       = 'Tb'

print "total size: %.3f%s" % (total_size, unity)

if dry_run:
    print "dry run. bye"
    sys.exit(0)

CMD_TREE = 'tree -h | tee DISKTREE.txt'
CMD_DU   = 'du -h . | sort -k 2 | tee FOLDERSIZE.txt'

ret = subprocess.call(CMD_TREE, shell=True)
if ret != 0:
    if ret < 0:
        print "error running tree. killed by signal %d" % ( ret )
        sys.exit(ret)
    else:
        print "error running tree. error #%d" % ( ret )
        sys.exit(ret)
else:
    print "  running tree finished successfully"



ret = subprocess.call(CMD_DU, shell=True)
if ret != 0:
    if ret < 0:
        print "error running du. killed by signal %d" % ( ret )
        sys.exit(ret)
    else:
        print "error running du. error #%d" % ( ret )
        sys.exit(ret)
else:
    print "  running du finished successfully"
