#!/usr/bin/python
import os
import sys
import inspect

currAbsPath     = os.path.dirname(inspect.getfile(inspect.currentframe()))
currFtpPath     = os.path.abspath(os.path.join(currAbsPath, "ftp"))
sys.path.insert(0, currFtpPath)

import lib.constants as constants
import lib.dbdump    as dbdump
from folderStruct import *
######################
## PERSONAL VARIABLES
######################
maxThreads              = 1
sleepWhileWaiting       = 10
Qborder                 = 30
DoAdaptamers            = True

ignoreDb                = False # ignore db file and re-do the work
loadLocalDbs            = True

skipIllumina            = False  # skip illumina dataset
skip454                 = False # skip 454 dataset

exportToFile            = True  # export to files or print in the screen
replaceFiles            = True  # when exporting to file replace files or leave output as tmp

redoReport              = False
mergePdfs               = True

debug                   = False # print extra information

runGenData              = True
#redo if variable is true
redo                    =   {
                                'compression'  : False,
                                'contamination': False,
                                'fastqc'       : False,
                                'hash'         : False,
                                'info'         : False,
                                'quals'        : False,
                            }

#run if not defined of true. does not run if false
does                    =   {
                                'contamination': True,
                                'fastqc'       : True,
                                'hash'         : True,
                                'quals'        : True,
                            }


## PIPELINE :: BEHAVIOR
runFastqc       = False
runSolexaqa     = False
runJellyfish    = True
runQuake        = True


contamClean = {
                'illumina': {   'threshold': 95,
                                'db'       : '/home/assembly/tomato150/scripts/pipeline/progs/contam/db/contamination_without_ecolidb_v0.2.fa'
                            },
                '454'     : {   'threshold': 85,
                                'db'       : '/home/assembly/tomato150/scripts/pipeline/progs/contam/db/contamination_without_ecolidb_v0.2.fa'
                            }
}

trimFastq = {
                '-h': 20,
                '-l': 30
}

#-p|probcutoff	probability value (between 0 and 1) at which base-calling error is considered too high (default; p = 0.05) *or*
#-h|phredcutoff  Phred score (between 0 and 40) at which base-calling error is considered too high
#-b|bwa          use BWA trimming algorithm
#-d|directory    path to directory where output files are saved
#-sanger         Sanger format (bypasses automatic format detection)
#-solexa         Solexa format (bypasses automatic format detection)
#-illumina       Illumina format (bypasses automatic format detection)
#-454            set flag if trimming Roche 454 data (experimental feature)
#LengthSort.pl one single-end or two paired-end FASTQ files [-l|length 25] [-d|directory path]
#-l|length       length cutoff [defaults to 25 nucleotides]


jellyfishParams = {
    'mer-len'        :  19,
    'threads'        :   8,
    'counter-len'    :   7,
    'out-counter-len':   4,
    'low'            :   1,
    'high'           : 300,
    'increment'      :   1,
    'size'           : 800000000, #hash size
    'out-buffer-size': 800000000,
    '_extra'         : '--both-strands', # --quake
    'lower-count'    : 1 #jelly dump
}

rmdupParams = {
    'compress': False,
    'homo'    : False,
    'zip'     : False,
    'nsize'   : None
}

quakeParams = {
    '-p'      : 10,     # number of processes
    '--no_cut': False,  # coverage model is optimized and cutoff was printed to
                        #   expected file cutoff.txt (default false)
    '--ratio' : 800,    # Likelihood ratio to set trusted/untrusted cutoff.
                        #   Generally set between 10-1000 with lower numbers
                        #   suggesting a lower threshold. [default: 200]

    #-p PROC               Number of processes [default: 4]
    #-q QUALITY_SCALE      Quality value ascii scale, generally 64 or 33. If not
    #                      specified, it will guess.
    #'--nocut': True,    # coverage model is optimized and cutoff was printed to
    #                    #   expected file cutoff.txt (default false)
    #'--ratio': 500,     # Likelihood ratio to set trusted/untrusted cutoff.
    #                    #   Generally set between 10-1000 with lower numbers
    #                    #   suggesting a lower threshold. [default: 200]
    #'-l'     :         # MIN_READ         Return only reads corrected and/or
                        #   trimmed to <min_read> bp
    #-u                 # Output error reads even if they can't be corrected,
                        #   maintaing paired end reads
    #-t TRIM_PAR        # Use BWA-like trim parameter <trim_par>
                        #   Parameter for read trimming. BWA trims a read down to
                        #   argmax_x{\sum_{i=x+1}^l(INT-q_i)}
                        #   if q_l<INT where l is the original read length. [0]
    #--headers          # Output only the original read headers without
                        #   correction messages
    #--log              # Output a log of all corrections into *.log as "quality
                        #   position new_nt old_nt"
}

filter454 = {
    'compressHomopolymerSize': 1,
    'seedLength'             : 50,
    'trim5'                  : 0.1,
    'minCompressedSize'      : 50,
    'maxCompressedSize'      : 850, # why?
    'maxNs'                  : 1,
    'filterDuplicates'       : 1


    #- The first argument (required) should be the path to the fasta file that
    #  contains the 454 reads (or alternatively, a directory containing ONLY
    #  fasta files).

    #- The second argument (optional) controls the homopolymer compression.
    #  It is recommended to leave this value at 1.

    #- The third argument (optional) controls the seed length used in clustering.
    #  Depending on the clonality of your data set, changing this may speed up
    #  or slow down the analysis. I can't give any clear guidelines on this, but
    #  0 (default) seems to be reasonably fast.

    #- The fourth (optional) argument controls the end trimming length. With this
    #  parameter it is possible to ignore the last N % or N bases of each read
    #  during the substring identity search. Emperically, a good value is 0.1 (this
    #  will ignore the last 10% of each sequence). Leaving this off may result in
    #  slightly more duplicated sequences being retained in the next step.

    #- Remember to pipe the output to an output file!


    #- The first argument should be the output file produced by analyze454Reads.py
    #- The second argument controls the name of the 'good' reads identifier file.
    #- The third argument controls the name of the 'bad' reads identifier file.
    #- The fourth argument controls the minimum compressed read length, all reads
    #  shorter than this will end up in the 'bad' file.
    #- The fifth argument controls the maximum compressed read length, all reads
    #  longer than this will end up in the 'bad' file.
    #- The sixth argument controls the maximum number of N's allowed per read, any
    #  read with more Ns than this number will end up in the 'bad' file.
    #- The seventh argument toggles the duplicate filtering, when switched on all
    #  duplicate reads will end up in the 'bad' file.



    #/path/to/python analyze454Reads.py 20kb_inserts.fasta 1 50 0.1 > 20kb_table.out
    #
    #This will first compress all the homopolymers in the sequence to 1. Next, it
    #will perform substring searches on the first 90% of each sequence to identify
    #duplicates. The results are reported in a human-readable table 20kb_table.out .
    #
    #After this has finished we can perform the actual filtering:
    #/path/to/python filter454Reads.py 20kb_table.out good.ids bad.ids 50 450 1 1
    #
    #This will evaluate the table produced in the previous step and divide all reads
    #into two categories: 'good' and 'bad'. The 'bad' reads file will contain the
    #identifiers of all reads that are shorter than 50 nt after compression (i.e.,
    #reads that consist of less than 50 flows), all reads longer than 450 nt after
    #compression (i.e., reads that consist of more than 450 flows), all reads with
    #more than 1 N (ambiguous basecall), and all duplicated reads. The 'good' file
    #will then contain all the read idenfiers that were not discarded based on any
    #of these criteria.
}







globalVariables = globals()
resVariables    = {}
for key in globalVariables.keys():
    forbidden = ['forbidden', 'sys', 'os', 'inspect', 'fullPath', 'currPath', 'pprint', 'copy', 'lib', 'globalVariables', 'constants', 'dbdump', 'resVariables', 'setup_forbidden']
     #'__builtins__', "__doc__", "__file__", "__name__", "__package__", , '__warningregistry__'
    if key not in forbidden:
        if key[0:2] != '__':
            resVariables[key] = globalVariables[key]

#import pprint
#pprint.pprint(resVariables)
dbdump.saveDump('setup', resVariables)
