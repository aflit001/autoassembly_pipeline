cd ..
tree -h -F -l -I '*.ok|*.py|Disk0*|_*|_prelim|_prefiltered|_tmp|_shotgun|Disk1|Disk2|scripts*|_bgi_data*|assemblies|project_description.csv|*.csv|*.sha256|*fastQC.zip|*.pdf|FILETREE|FILETREETMP'                         > FILETREE
tree -h -F -l -I 'raw|docs|filtered|assembled|_shotgun|_tmp|scripts*|mapped|assemblies|FASTQC_preliminary|scripts|_bgi_data|project_description.csv|*.csv|*.sha256|*fastQC.zip|*.pdf|FILETREETMP' > FILETREETMP
cd -
